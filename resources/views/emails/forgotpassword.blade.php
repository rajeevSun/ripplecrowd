Hello {{ $user->first_name }} {{ $user->last_name }},

We received a request to reset the password associated with this email address. If you made this request, please follow the instruction below.

If you did not request to have your password reset, you can safely ignore this email. We assure you that your customer account is safe.

<strong>Click the link below to reset your password:</strong>
You can reset your password here: <a href="{{ URL::to('#') }}">Click here to reset your password.</a>
Thanks for using RippleCrowd!
RippleCrowd Team.