<?php
return [
    '001' => 'Wrong params',
    '002' => 'Created success',
    '003' => 'Server error',
    '004' => 'Success',
    '005' => 'Not authenticated',
    '006' => 'Empty',
    '007' => 'Not found',
    '008' => 'Not Acceptable',
    '009' => 'Not Authorized',
];