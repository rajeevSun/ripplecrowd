<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'v1'], function () {
    // get follow campaigns
    Route::get('campaigns/follow', 'CampaignController@followingCampaigns');
    // get discover campaigns
    Route::get('campaigns/discover', 'CampaignController@discoverCampaigns');
    // get recommend campaigns
    Route::get('campaigns/recommend', 'CampaignController@recommendCampaigns');
    // get trending campaigns
    Route::get('campaigns/trending', 'CampaignController@trendingCampaigns');
    // create new campaigns
    Route::post('campaigns', 'CampaignController@store');
    // get specific campaign
    Route::get('campaigns/{campaignId}', 'CampaignController@show');
    // update a campaign
    Route::post('campaigns/{campaignId}', 'CampaignController@edit');
    // post new post
    Route::post('posts', 'PostController@store');
    // edit a post
    Route::post('posts/{postId}', 'PostController@edit');
    // delete a post
    Route::delete('posts/{postId}', 'PostController@delete');
    // new post report.
    Route::post('posts/{postId}/reports', 'ReportController@store');
    // get comments list of a post
    Route::get('posts/{postId}/comments', 'CommentController@getCommentsOfPost');
    // follow project/user
    Route::post('follows', 'FollowController@store');
    // share project/post
    Route::post('shares', 'ShareController@store');
    // edit a share
    Route::post('shares/{shareId}', 'ShareController@edit');
    // delete a share
    Route::delete('shares/{shareId}', 'ShareController@delete');
    // get comments list of a share
    Route::get('shares/{shareId}/comments', 'CommentController@getCommentsOfShare');
    // get activities
    Route::get('activities', 'ActivityController@getActivitiesStreamByUser');
    // get activity by id
    Route::get('activities/{activityId}', 'ActivityController@getActivityById');
    // get comments list by activity.
    Route::get('activities/{activityId}/comments', 'CommentController@getCommentsByActivity');
    // new comment by activity
    Route::post('activities/{activityId}/comments', 'CommentController@storeCommentsByActivity');
    // new like by activity
    Route::post('activities/{activityId}/likes', 'LikeController@storeLikesByActivity');
    // unlike by activity
    Route::delete('activities/{activityId}/likes', 'LikeController@deleteLikesByActivity');
    // new share by activity
    Route::post('activities/{activityId}/shares', 'ShareController@storeSharesByActivity');
    // delete an acitvity
    Route::delete('activities/{activityId}', 'ActivityController@delete');
    // get followed user
    Route::get('follows/user/followers', 'FollowController@getFollowerUsers');
    // get following user
    Route::get('follows/user/followings', 'FollowController@getFollowingUsers');
    // discover groups.
    Route::get('groups', 'GroupController@getGroups');
    // get joined groups of current user.
    Route::get('groups/joined', 'GroupController@getJoinedGroups');
    // join a group
    Route::post('groups/{groupId}/join', 'GroupController@joinGroup');
    // leave a group
    Route::post('groups/{groupId}/leave', 'GroupController@leaveGroup');
    // create new groups
    Route::post('groups', 'GroupController@store');
    // edit group
    Route::patch('groups/{groupId}', 'GroupController@edit');
    // get specific group detail
    Route::get('groups/{groupId}', 'GroupController@getGroupById');
    // get members of group
    Route::get('groups/{groupId}/members', 'GroupController@getMembersById');
    // delete group
    Route::delete('groups/{groupId}', 'GroupController@delete');
    // achievements list
    Route::get('profile/achievements', 'AchievementController@getAllAchievements');
    // achievements summary
    Route::get('profile/achievements/summary', 'AchievementController@getAchievementsSummary');
    // get profile activities
    Route::get('profile/activities', 'UserController@getActivities');
    // get profilings
    Route::get('profilings/{profilingId?}', 'ProfilingController@all');
    // update profiling
    Route::put('profilings/{profilingId}', 'ProfilingController@edit');
    // add profiling
    Route::post('profilings', 'ProfilingController@add');
    // remove profiling
    Route::delete('profilings/{profilingId}', 'ProfilingController@remove');
    // get profile statistics
    Route::get('profile/statistics', 'UserController@getStatistics');
    // get profile raised backings
    Route::get('profile/statistics/raised', 'UserController@getRaisedStatistics');
    // get profile invested backings
    Route::get('profile/statistics/invested', 'UserController@getInvestedStatistics');
    // update profile
    Route::post('profile', 'UserController@edit');
    // get inviting users
    Route::get('users/inviting', 'UserController@getInvitedUsersList');
    // find all user
    Route::get('users', 'UserController@getAllUsers');
    // create invitation
    Route::post('invitations', 'InvitationController@store');
    // delete invitation
    Route::delete('invitations/{invitationId}', 'InvitationController@delete');
    // delete follow
    Route::delete('follows', 'FollowController@delete');
    // get trending users
    Route::get('users/trending', 'UserController@getTrending');
    // get recommend campaigns
    Route::get('users/recommend', 'UserController@recommendUsers');
    // funding a campaign
    Route::post('backings', 'BackingController@store');
    // confirm a pending backing
    Route::patch('backings/{backingId}', 'BackingController@edit');
    // remove a pending backing
    Route::delete('backings/{backingId}', 'BackingController@delete');
    // get comments list of campaign
    Route::get('campaigns/{campaignId}/comments', 'CommentController@getCommentsOfCampaign');
    // get backings list campaign
    Route::get('campaigns/{campaignId}/backings', 'BackingController@getBackingsOfCampaign');
    // get milestones of campaign
    Route::get('campaigns/{campaignId}/milestones', 'MilestoneController@getMilestonesOfCampaign');
    // add milestone for campaign
    Route::post('milestones', 'MilestoneController@store');
    // get milestone detail
    Route::get('milestones/{milestoneId}', 'MilestoneController@show');
    // edit milestone
    Route::post('milestones/{milestoneId}', 'MilestoneController@edit');
    // delete milestone
    Route::delete('milestones/{milestoneId}', 'MilestoneController@delete');
    // get comments list of milestone.
    Route::get('milestones/{milestoneId}/comments', 'CommentController@getCommentsOfMilestone');
    // get meetup list
    Route::get('meetups', 'MeetupController@getMeetups');
    // edit meetup
    Route::patch('meetups/{meetupId}', 'MeetupController@edit');
    // get a specific meetup
    Route::get('meetups/{meetupId}', 'MeetupController@getMeetupById');
    // delete a meetup
    Route::delete('meetups/{meetupId}', 'MeetupController@delete');
    // create new meetup
    Route::post('meetups', 'MeetupController@store');
    // get comment list of a meetup
    Route::get('meetups/{meetupId}/comments', 'CommentController@getCommentsOfMeetup');
    // get attendees of a meetup
    Route::get('meetups/{meetupId}/attendees', 'MeetupController@getAttendeesOfMeetup');
    // join a meetup
    Route::post('meetups/{meetupId}/join', 'MeetupController@joinMeetup');
    // leave a group
    Route::post('meetups/{meetupId}/leave', 'MeetupController@leaveMeetup');
    // create new like (like)
    Route::post('likes', 'LikeController@store');
    // delete a like (unlike)
    Route::delete('likes', 'LikeController@delete');
    // post new comment
    Route::post('comments', 'CommentController@store');
    // edit comment
    Route::post('comments/{commentId}', 'CommentController@edit');
    // delete a comment
    Route::delete('comments/{commentId}', 'CommentController@delete');
    // full text search
    Route::get('search', 'SearchController@fullTextSearch');
    // logout
    Route::post('logout', 'UserController@logout');
    // regiter device token
    Route::post('device_tokens', 'UserController@registerDeviceToken');
    // get notification
    Route::get('notifications', 'NotificationController@getNotifications');
});

// api for get token when exchange email/password
Route::post('v1/token', 'UserController@getToken');
Route::post('v1/register', 'UserController@register');
Route::post('v1/{provider?}/token', 'UserController@getProviderToken');
Route::post('v1/forgot_password', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// get categories
Route::get('v1/categories', 'CategoryController@index');