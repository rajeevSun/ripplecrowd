<?php

use Illuminate\Database\Seeder;
use App\Meetup;

class MeetupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 1000000; $i++) {
            Meetup::create([
                'title' => $faker->title,
                'description' => $faker->text(),
                'image' => $faker->imageUrl()
            ]);
        }
    }
}
