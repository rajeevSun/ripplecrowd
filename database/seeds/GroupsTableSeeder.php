<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 1000000; $i++) {
            Group::create([
                'name' => $faker->name,
                'image' => $faker->imageUrl()
            ]);
        }
    }
}
