<?php

use Illuminate\Database\Seeder;
use App\Campaign;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 1000000; $i++) {
            Campaign::create([
                'name' => $faker->name,
                'short_blurb' => $faker->text,
                'description' => $faker->text,
                'image' => $faker->imageUrl()
            ]);
        }
    }
}
