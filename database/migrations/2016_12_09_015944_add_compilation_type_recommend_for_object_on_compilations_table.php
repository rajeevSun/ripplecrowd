<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompilationTypeRecommendForObjectOnCompilationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compilations', function (Blueprint $table) {
            DB::statement("ALTER TABLE `compilations` CHANGE `object` `object` ENUM('follow','join', 'suggest') DEFAULT 'follow'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compilations', function (Blueprint $table) {
            //
        });
    }
}
