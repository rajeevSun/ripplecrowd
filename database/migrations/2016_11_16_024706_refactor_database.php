<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            DB::statement("ALTER TABLE `subscriptions` CHANGE `verificationcode` `verification_code` VARCHAR(45)");
            DB::statement("ALTER TABLE `subscriptions` CHANGE `isverified` `is_verified` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `subscriptions` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->dropColumn('createdon');
            $table->timestamps();
        });

        Schema::table('activities', function (Blueprint $table) {
            DB::statement("ALTER TABLE `activities` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `activities` CHANGE `likes` `likes_count` INT");
            $table->dropColumn('createdon');
        });

        Schema::table('admins', function(Blueprint $table) {
            DB::statement("ALTER TABLE `admins` CHANGE `admintype` `admin_type` ENUM('super', 'sub') DEFAULT 'sub'");
            DB::statement("ALTER TABLE `admins` CHANGE `lastlogindate` `last_login_date` DATETIME");
            DB::statement("ALTER TABLE `admins` CHANGE `lastlogoutdate` `last_logout_date` DATETIME");
            DB::statement("ALTER TABLE `admins` CHANGE `lastloginip` `last_login_ip` VARCHAR(16)");
            DB::statement("ALTER TABLE `admins` CHANGE `isverified` `is_verified` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `admins` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->dropColumn('modifiedon');
            $table->dropColumn('createdon');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('backings', function (Blueprint $table) {
            DB::statement("ALTER TABLE `backings` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `backings` CHANGE `pledgeamount` `pledges_amount` DECIMAL(8,2)");
            DB::statement("ALTER TABLE `backings` CHANGE `projectid` `campaign_id` INT");
            DB::statement("ALTER TABLE `backings` CHANGE `rewardid` `reward_id` INT");
            DB::statement("ALTER TABLE `backings` CHANGE `categoryid` `category_id` INT");
            DB::statement("ALTER TABLE `backings` CHANGE `cardname` `card_name` VARCHAR(100)");
            DB::statement("ALTER TABLE `backings` CHANGE `cardnumber` `card_number` BIGINT");
            DB::statement("ALTER TABLE `backings` CHANGE `expirydate` `expiry_date` VARCHAR(125)");
            DB::statement("ALTER TABLE `backings` CHANGE `isremember` `is_remember` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `backings` CHANGE `billingaddress1` `primary_billing_address` VARCHAR(255)");
            DB::statement("ALTER TABLE `backings` CHANGE `billingaddress2` `secondary_billing_address` VARCHAR(255)");
            DB::statement("ALTER TABLE `backings` CHANGE `postalcode` `postal_code` INT");
            DB::statement("ALTER TABLE `backings` CHANGE `stripecustomerid` `stripe_customer_id` VARCHAR(255)");
            DB::statement("ALTER TABLE `backings` CHANGE `stripetoken` `stripe_token` VARCHAR(255)");
            DB::statement("ALTER TABLE `backings` CHANGE `stripechargeid` `stripe_charge_id` VARCHAR(255)");
            DB::statement("ALTER TABLE `backings` CHANGE `bankname` `bank_name` VARCHAR(255)");
            DB::statement("ALTER TABLE `backings` CHANGE `bankaccount` `bank_account` VARCHAR(255)");
            $table->timestamps();
            $table->softDeletes();
            $table->dropColumn('createdon');
        });

        Schema::table('categories', function (Blueprint $table) {
            DB::statement("ALTER TABLE `categories` CHANGE `categoryname` `name` VARCHAR(100)");
            DB::statement("ALTER TABLE `categories` CHANGE `metatitle` `meta_title` VARCHAR(255)");
            DB::statement("ALTER TABLE `categories` CHANGE `metakeyword` `meta_keyword` MEDIUMTEXT");
            DB::statement("ALTER TABLE `categories` CHANGE `metadescription` `meta_description` MEDIUMTEXT");
            DB::statement("ALTER TABLE `categories` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->dropColumn('createdon');
        });

        Schema::table('cities', function (Blueprint $table) {
            DB::statement("ALTER TABLE `cities` CHANGE `stateid` `state_id` INT");
            DB::statement("ALTER TABLE `cities` CHANGE `cityname` `name` VARCHAR(255)");
            $table->timestamps();
            $table->softDeletes();
            $table->dropColumn('createdon');
        });

        Schema::table('comments', function (Blueprint $table) {
            DB::statement("ALTER TABLE `comments` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `comments` CHANGE `likes` `likes_count` INT");
            $table->softDeletes();
            $table->dropColumn('postedon');
        });

        Schema::table('contacts', function (Blueprint $table) {
            DB::statement("ALTER TABLE `contacts` CHANGE `firstname` `first_name` VARCHAR(100)");
            DB::statement("ALTER TABLE `contacts` CHANGE `lastname` `last_name` VARCHAR(100)");
            DB::statement("ALTER TABLE `contacts` CHANGE `mobile` `mobile_number` VARCHAR(100)");
            $table->softDeletes();
            $table->timestamps();
            $table->dropColumn('createdon');
        });

        Schema::table('countries', function (Blueprint $table) {
            DB::statement("ALTER TABLE `countries` CHANGE `countryid` `country_id` VARCHAR(100)");
            DB::statement("ALTER TABLE `countries` CHANGE `countrycode` `country_code` VARCHAR(2)");
            DB::statement("ALTER TABLE `countries` CHANGE `countrymobilecode` `country_mobile_code` VARCHAR(200)");
            DB::statement("ALTER TABLE `countries` CHANGE `countryname` `name` VARCHAR(255)");
            DB::statement("ALTER TABLE `countries` CHANGE `currencytype` `currency_type` CHAR(3)");
            DB::statement("ALTER TABLE `countries` CHANGE `currencysymbol` `currency_symbol` TEXT");
            DB::statement("ALTER TABLE `countries` CHANGE `paypalsupport` `paypal_support` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `countries` CHANGE `status` `status` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `countries` CHANGE `defaultcurrency` `default_currency` TINYINT DEFAULT 0");
            $table->softDeletes();
            $table->timestamps();
            $table->dropColumn('createdon');
        });

        Schema::table('currencies', function (Blueprint $table) {
            DB::statement("ALTER TABLE `currencies` CHANGE `countryname` `country_name` VARCHAR(100)");
            DB::statement("ALTER TABLE `currencies` CHANGE `currencytype` `currency_type` VARCHAR(100)");
            DB::statement("ALTER TABLE `currencies` CHANGE `currencysymbol` `currency_symbol` VARCHAR(45)");
            DB::statement("ALTER TABLE `currencies` CHANGE `currencyrate` `currency_rate` DECIMAL (10,2)");
            DB::statement("ALTER TABLE `currencies` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->softDeletes();
            $table->timestamps();
            $table->dropColumn('createdon');
        });

        Schema::table('faqs', function (Blueprint $table) {
            DB::statement("ALTER TABLE `faqs` CHANGE `projectid` `campaign_id` INT");
            $table->softDeletes();
            $table->timestamps();
            $table->dropColumn('createdon');
        });

        Schema::table('files', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::drop('followprojects');

        Schema::table('follows', function(Blueprint $table) {
            DB::statement("ALTER TABLE `follows` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `follows` CHANGE `object` `object` ENUM('campaign', 'user') DEFAULT 'user'");
            $table->dropColumn('createdon');
        });

        Schema::table('groups', function(Blueprint $table) {
            DB::statement("ALTER TABLE `groups` CHANGE `group_name` `name` VARCHAR(255)");
        });

        Schema::table('histories', function(Blueprint $table) {
            DB::statement("ALTER TABLE `histories` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `histories` CHANGE `ipaddress` `ip_address` VARCHAR(50)");
            DB::statement("ALTER TABLE `histories` CHANGE `logindatetime` `login_datetime` DATETIME");
            DB::statement("ALTER TABLE `histories` CHANGE `logoutdatetime` `logout_datetime` DATETIME");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::drop('inboxmsgs');

        Schema::table('interests', function(Blueprint $table) {
            DB::statement("ALTER TABLE `interests` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `interests` CHANGE `categoryid` `category_id` INT");
            $table->dropColumn('createdon');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('languages', function(Blueprint $table) {
            DB::statement("ALTER TABLE `languages` CHANGE `languagename` `name` VARCHAR(125)");
            DB::statement("ALTER TABLE `languages` CHANGE `languagecode` `code` VARCHAR(45)");
            DB::statement("ALTER TABLE `languages` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('likes', function(Blueprint $table) {
            DB::statement("ALTER TABLE `likes` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `likes` CHANGE `object` `object` ENUM('post', 'campaign', 'comment', 'activity') DEFAULT 'post'");
            $table->dropColumn('createdon');
            $table->softDeletes();
        });

        Schema::table('memberships', function(Blueprint $table) {
            DB::statement("ALTER TABLE `memberships` CHANGE `packagename` `package_name` VARCHAR(125)");
            DB::statement("ALTER TABLE `memberships` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->dropColumn('createdon');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('newsletters', function(Blueprint $table) {
            DB::statement("ALTER TABLE `newsletters` CHANGE `templatename` `template_name` VARCHAR(125)");
            DB::statement("ALTER TABLE `newsletters` CHANGE `senderemail` `sender_email` VARCHAR(125)");
            DB::statement("ALTER TABLE `newsletters` CHANGE `sendername` `sender_name` VARCHAR(45)");
            DB::statement("ALTER TABLE `newsletters` CHANGE `emailcontent` `email_content` BLOB");
            DB::statement("ALTER TABLE `newsletters` CHANGE `subscription` `subscription` TINYINT DEFAULT 0");
            $table->dropColumn('createdon');
        });

        Schema::table('notification_objects', function(Blueprint $table) {
            DB::statement("ALTER TABLE `notification_objects` CHANGE `object` `object` ENUM('campaign', 'user') DEFAULT 'campaign'");
        });

        Schema::table('notification_objects', function(Blueprint $table) {
            DB::statement("ALTER TABLE `notification_objects` CHANGE `object` `object` ENUM('campaign', 'user') DEFAULT 'campaign'");
        });

        Schema::rename('paymentgateways', 'payment_gateways');
        Schema::rename('paymenthosts', 'payment_hosts');

        Schema::table('payment_gateways', function(Blueprint $table) {
            DB::statement("ALTER TABLE `payment_gateways` CHANGE `gatewayname` `gateway_name` VARCHAR(100)");
            DB::statement("ALTER TABLE `payment_gateways` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('payment_hosts', function(Blueprint $table) {
            DB::statement("ALTER TABLE `payment_hosts` CHANGE `projectid` `campaign_id` INT");
            DB::statement("ALTER TABLE `payment_hosts` CHANGE `hostid` `host_id` INT");
            DB::statement("ALTER TABLE `payment_hosts` CHANGE `transactionid` `transaction_id` INT");
            DB::statement("ALTER TABLE `payment_hosts` CHANGE `transactiondate` `transaction_date` DATETIME");
            DB::statement("ALTER TABLE `payment_hosts` CHANGE `transactiontype` `transaction_type` VARCHAR(100)");
            DB::statement("ALTER TABLE `payment_hosts` CHANGE `paymentstatus` `payment_status` TINYINT DEFAULT 0");
            $table->timestamps();
            $table->softDeletes();
            $table->dropColumn('createdon');
        });

        Schema::table('payments', function(Blueprint $table) {
            DB::statement("ALTER TABLE `payments` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `payments` CHANGE `backingid` `backing_id` INT");
            DB::statement("ALTER TABLE `payments` CHANGE `projectid` `campaign_id` INT");
            DB::statement("ALTER TABLE `payments` CHANGE `paykey` `pay_key` VARCHAR(125)");
            DB::statement("ALTER TABLE `payments` CHANGE `transactionid` `transaction_id` VARCHAR(125)");
            DB::statement("ALTER TABLE `payments` CHANGE `shippingstatus` `shipping_status` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `payments` CHANGE `paymenttype` `payment_type` INT");
            DB::statement("ALTER TABLE `payments` CHANGE `payeremail` `payer_email` VARCHAR(125)");
            DB::statement("ALTER TABLE `payments` CHANGE `recievedstatus` `received_status` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `payments` CHANGE `paidon` `paid_at` DATETIME");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('posts', function(Blueprint $table) {
            DB::statement("ALTER TABLE `posts` CHANGE `userid` `user_id` INT");
            $table->dropColumn('createdon');
            $table->softDeletes();
        });

        Schema::table('prefooters', function(Blueprint $table) {
            DB::statement("ALTER TABLE `prefooters` CHANGE `footerlink` `footer_link` VARCHAR(100)");
            DB::statement("ALTER TABLE `prefooters` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->dropColumn('createdon');
            $table->timestamps();
        });

        Schema::table('profilings', function(Blueprint $table) {
            DB::statement("ALTER TABLE `profilings` CHANGE `userid` `user_id` INT");
            $table->timestamps();
        });

        Schema::rename('projects', 'campaigns');
        Schema::table('campaigns', function(Blueprint $table) {
            DB::statement("ALTER TABLE `campaigns` CHANGE `projectname` `name` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `categoryid` `category_id` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `shortblurb` `short_blurb` MEDIUMTEXT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `fundingduration` `funding_duration` VARCHAR(100)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `fundinggoal` `funding_goal` DECIMAL(10,2)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `totalpledgeamount` `pledges_amount` DECIMAL(8,2)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `totalbacking` `backings_count` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `currencyid` `currency_id` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `projectlogo` `logo` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `projectimage` `image` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `projectvideo` `video` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `likes` `likes_count` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `newandnoteworthy` `new_and_note_worthy` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `projectoftheday` `campaign_of_the_day` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `isfunded` `is_funded` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `endingon` `ending_at` DATETIME");
            DB::statement("ALTER TABLE `campaigns` CHANGE `address1` `primary_address` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `address2` `secondary_address` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `businessname` `business_name` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `businesstype` `business_type` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `idverified` `id_verified` TINYINT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `identityproof` `identity_proof` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `prooftype` `proof_type` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `proofverified` `proof_verified` TINYINT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `verifiedon` `verified_at` DATETIME");
            DB::statement("ALTER TABLE `campaigns` CHANGE `feerecieved` `fee_recieved` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `paypalemail` `paypal_email` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `metatitle` `meta_title` VARCHAR(255)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `metakeyword` `meta_keyword` MEDIUMTEXT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `metadescription` `meta_description` MEDIUMTEXT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `projectverified` `campaign_verified` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `fundingstrategy` `funding_strategy` ENUM('allornothing', 'keepitall') DEFAULT 'allornothing'");
            DB::statement("ALTER TABLE `campaigns` CHANGE `fundingkind` `funding_kind` ENUM('equity', 'debt', 'revenue') DEFAULT 'equity'");
            DB::statement("ALTER TABLE `campaigns` CHANGE `debtduration` `debt_duration` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `interestrateamount` `interest_rate_amount` DECIMAL(8,2)");
            DB::statement("ALTER TABLE `campaigns` CHANGE `interestpaymentperiod` `interest_payment_period` ENUM('annual', 'semiannual', 'none') DEFAULT 'none'");
            DB::statement("ALTER TABLE `campaigns` CHANGE `revenueshare` `revenue_share` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `returnmultipliers` `return_multipliers` INT");
            DB::statement("ALTER TABLE `campaigns` CHANGE `shares` `shares_count` INT");
            $table->dropColumn('title');
            $table->dropColumn('subcategoryid');
            $table->dropColumn('createdon');
            $table->dropColumn('updatedon');
            $table->dropColumn('fundingtype');
        });

        Schema::drop('projectviews');

        Schema::table('requests', function(Blueprint $table) {
            DB::statement("ALTER TABLE `requests` CHANGE `projecttitle` `campaign_title` VARCHAR(255)");
            DB::statement("ALTER TABLE `requests` CHANGE `projectid` `campaign_id` INT");
            DB::statement("ALTER TABLE `requests` CHANGE `fundinggoal` `funding_goal` DECIMAL(8,2)");
            DB::statement("ALTER TABLE `requests` CHANGE `fundingduration` `funding_duration` INT");
            DB::statement("ALTER TABLE `requests` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->dropColumn('createdon');
            $table->timestamps();
        });

        Schema::table('rewards', function(Blueprint $table) {
            DB::statement("ALTER TABLE `rewards` CHANGE `projectid` `campaign_id` INT");
            DB::statement("ALTER TABLE `rewards` CHANGE `pledgeamount` `pledges_amount` DECIMAL(8,2)");
            DB::statement("ALTER TABLE `rewards` CHANGE `shippinginvolved` `shipping_involved` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `rewards` CHANGE `shippingdetails` `shipping_details` MEDIUMTEXT");
            DB::statement("ALTER TABLE `rewards` CHANGE `estimateddelivery` `estimated_delivery` VARCHAR(45)");
            DB::statement("ALTER TABLE `rewards` CHANGE `islimited` `is_limited` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `rewards` CHANGE `countrylist` `country_list` MEDIUMTEXT");
            DB::statement("ALTER TABLE `rewards` CHANGE `backerscount` `backers_count` INT");
            DB::statement("ALTER TABLE `rewards` CHANGE `checkbox` `checkbox` TINYINT DEFAULT 0");
            $table->dropColumn('createdon');
            $table->dropColumn('updatedon');
            $table->timestamps();
        });

        Schema::drop('sendmsgs');

        Schema::table('shares', function(Blueprint $table) {
            DB::statement("ALTER TABLE `shares` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `shares` CHANGE `object` `object` ENUM('post', 'campaign') DEFAULT 'post'");
            $table->dropColumn('createdon');
        });

        Schema::table('sliders', function(Blueprint $table) {
            DB::statement("ALTER TABLE `sliders` CHANGE `slidername` `name` VARCHAR(125)");
            DB::statement("ALTER TABLE `sliders` CHANGE `slidertitle` `title` VARCHAR(100)");
            DB::statement("ALTER TABLE `sliders` CHANGE `sliderimage` `image` VARCHAR(255)");
            DB::statement("ALTER TABLE `sliders` CHANGE `sliderdescription` `description` VARCHAR(255)");
            DB::statement("ALTER TABLE `sliders` CHANGE `status` `status` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `sliders` CHANGE `sliderurl` `url` VARCHAR(255)");
            $table->timestamps();
        });

        Schema::rename('starredprojects', 'starred_campaigns');
        Schema::table('starred_campaigns', function(Blueprint $table) {
            DB::statement("ALTER TABLE `starred_campaigns` CHANGE `userid` `user_id` INT");
            DB::statement("ALTER TABLE `starred_campaigns` CHANGE `projectid` `campaign_id` INT");
            DB::statement("ALTER TABLE `starred_campaigns` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->dropColumn('createdon');
            $table->timestamps();
        });

        Schema::table('states', function(Blueprint $table) {
            DB::statement("ALTER TABLE `states` CHANGE `countryid` `country_id` INT");
            DB::statement("ALTER TABLE `states` CHANGE `statecode` `code` VARCHAR(32)");
            DB::statement("ALTER TABLE `states` CHANGE `statename` `name` VARCHAR(32)");
            DB::statement("ALTER TABLE `states` CHANGE `status` `status` TINYINT DEFAULT 0");
            $table->timestamps();
        });

        Schema::rename('staticpages', 'static_pages');
        Schema::table('static_pages', function(Blueprint $table) {
            DB::statement("ALTER TABLE `static_pages` CHANGE `pagename` `name` VARCHAR(100)");
            DB::statement("ALTER TABLE `static_pages` CHANGE `pagetitle` `title` VARCHAR(100)");
            DB::statement("ALTER TABLE `static_pages` CHANGE `seourl` `url` VARCHAR(255)");
            DB::statement("ALTER TABLE `static_pages` CHANGE `metaname` `meta_name` VARCHAR(255)");
            DB::statement("ALTER TABLE `static_pages` CHANGE `metakeyword` `meta_keyword` MEDIUMTEXT");
            DB::statement("ALTER TABLE `static_pages` CHANGE `metadescription` `meta_description` BLOB");
            DB::statement("ALTER TABLE `static_pages` CHANGE `description` `description` BLOB");
            DB::statement("ALTER TABLE `static_pages` CHANGE `status` `status` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `static_pages` CHANGE `header` `header` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `static_pages` CHANGE `footer` `footer` TINYINT DEFAULT 0");
            $table->timestamps();
        });

        Schema::drop('subcategories');

        Schema::table('updates', function(Blueprint $table) {
            DB::statement("ALTER TABLE `updates` CHANGE `projectid` `campaign_id` INT");
            DB::statement("ALTER TABLE `updates` CHANGE `userid` `user_id` INT");
            $table->dropColumn('postedon');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('users', function(Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE `contactnumber` `mobile_number` VARCHAR(20)");
            DB::statement("ALTER TABLE `users` CHANGE `status` `status` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `mobileveificationcode` `mobile_verification_code` VARCHAR(45)");
            DB::statement("ALTER TABLE `users` CHANGE `emailverificationcode` `email_verification_code` VARCHAR(45)");
            DB::statement("ALTER TABLE `users` CHANGE `mobilestatus` `mobile_status` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `emailstatus` `email_status` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `lastlogindate` `last_login_date` DATETIME");
            DB::statement("ALTER TABLE `users` CHANGE `lastlogoutdate` `last_logout_date` DATETIME");
            DB::statement("ALTER TABLE `users` CHANGE `lastloginip` `last_login_ip` VARCHAR(50)");
            DB::statement("ALTER TABLE `users` CHANGE `postalcode` `postal_code` INT");
            DB::statement("ALTER TABLE `users` CHANGE `vanityurl` `vanity_url` VARCHAR(125)");
            DB::statement("ALTER TABLE `users` CHANGE `weburl` `web_url` VARCHAR(225)");
            DB::statement("ALTER TABLE `users` CHANGE `followerscount` `followers_count` INT");
            DB::statement("ALTER TABLE `users` CHANGE `followingcount` `followings_count` INT");
            DB::statement("ALTER TABLE `users` CHANGE `loginhit` `login_hit` INT");
            DB::statement("ALTER TABLE `users` CHANGE `createdcount` `created_count` INT");
            DB::statement("ALTER TABLE `users` CHANGE `backedcount` `backed_count` INT");
            DB::statement("ALTER TABLE `users` CHANGE `packageid` `package_id` INT");
            DB::statement("ALTER TABLE `users` CHANGE `packageenddate` `package_enddate` DATETIME");
            DB::statement("ALTER TABLE `users` CHANGE `newsandevents` `news_and_events` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `backerupdates` `backer_updates` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `creatorpledges` `creator_pledges` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `creatorcomments` `creator_comments` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `friendactivity` `friend_activity` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `newfollowers` `new_followers` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `newlikes` `new_likes` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `referrerid` `referrer_id` INT");
            DB::statement("ALTER TABLE `users` CHANGE `referrercredit` `referrer_credit` INT");
            DB::statement("ALTER TABLE `users` CHANGE `adminremarks` `admin_remarks` MEDIUMTEXT");
            DB::statement("ALTER TABLE `users` CHANGE `paypalemail` `paypal_email` VARCHAR(255)");
            DB::statement("ALTER TABLE `users` CHANGE `prooftype` `proof_type` VARCHAR(125)");
            DB::statement("ALTER TABLE `users` CHANGE `idproof` `id_proof` VARCHAR(125)");
            DB::statement("ALTER TABLE `users` CHANGE `accountverified` `account_verified` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `users` CHANGE `bankname` `bank_name` VARCHAR(255)");
            DB::statement("ALTER TABLE `users` CHANGE `bankaccount` `bank_account` VARCHAR(255)");
            $table->dropColumn('mobile');
            $table->dropColumn('createdon');
            $table->dropColumn('modifiedon');
            $table->dropColumn('logintype');
            $table->dropColumn('twitterid');
            $table->dropColumn('facebook');
            $table->dropColumn('followers');
            $table->dropColumn('following');
            $table->dropColumn('twitter');
            $table->dropColumn('google');
            $table->dropColumn('sandbox_stripe_access_token');
            $table->dropColumn('sandbox_stripe_refresh_token');
            $table->dropColumn('sandbox_stripe_publishable_key');
            $table->dropColumn('sandbox_stripe_user_id');
            $table->dropColumn('sandbox_stripe_token_type');
            $table->dropColumn('live_stripe_access_token');
            $table->dropColumn('live_stripe_publishable_key');
            $table->dropColumn('live_stripe_user_id');
            $table->dropColumn('live_stripe_token_type');
            $table->dropColumn('stripe_customerid');
            $table->dropColumn('usertype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
