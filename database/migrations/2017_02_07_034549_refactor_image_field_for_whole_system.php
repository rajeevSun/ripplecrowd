<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorImageFieldForWholeSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'image_id')) {
                $table->integer('image_id')->nullable()->unsigned();
            }
        });
        Schema::table('milestones', function(Blueprint $table) {
            if (!Schema::hasColumn('milestones', 'image_id')) {
                $table->integer('image_id')->nullable()->unsigned();
            }
        });
        Schema::table('campaigns', function(Blueprint $table) {
            if (!Schema::hasColumn('campaigns', 'image_id')) {
                $table->integer('image_id')->nullable()->unsigned();
            }
        });
        Schema::table('groups', function(Blueprint $table) {
            if (!Schema::hasColumn('groups', 'image_id')) {
                $table->integer('image_id')->nullable()->unsigned();
            }
        });
        Schema::table('meetups', function(Blueprint $table) {
            if (!Schema::hasColumn('meetups', 'image_id')) {
                $table->integer('image_id')->nullable()->unsigned();
            }
        });
        Schema::table('achievements', function(Blueprint $table) {
            if (!Schema::hasColumn('achievements', 'image_id')) {
                $table->integer('image_id')->nullable()->unsigned();
            }
        });
        Schema::table('posts', function(Blueprint $table) {
            if (!Schema::hasColumn('posts', 'image_id')) {
                $table->integer('image_id')->nullable()->unsigned();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
