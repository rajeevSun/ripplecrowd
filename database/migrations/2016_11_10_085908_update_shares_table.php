<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shares', function (Blueprint $table) {
            DB::statement("ALTER TABLE `shares` CHANGE `projectid` `object_id` INTEGER");
            $table->enum('object', ['post', 'project'])->default('post');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shares', function (Blueprint $table) {
            DB::statement("ALTER TABLE `shares` CHANGE `object_id` `projectid` INTEGER");
            $table->dropColumn('object');
        });
    }
}
