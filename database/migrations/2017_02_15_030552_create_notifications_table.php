<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('notification_receivers');
        Schema::dropIfExists('notification_objects');
        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('user_id');
            $table->string('type');
            $table->string('notification_type');
            $table->morphs('notifiable');
            $table->morphs('object');
            $table->text('data');
            $table->timestamp('read_at')->nullable();
            $table->integer('target_id');
            $table->string('target_name');
            $table->string('target_object');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
