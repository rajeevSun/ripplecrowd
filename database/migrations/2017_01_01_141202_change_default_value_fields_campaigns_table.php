<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultValueFieldsCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            DB::statement("ALTER TABLE `campaigns` CHANGE `backings_count` `backings_count` INTEGER DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `video` `video` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `likes_count` `likes_count` INTEGER DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `new_and_note_worthy` `new_and_note_worthy` INTEGER DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `primary_address` `primary_address` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `secondary_address` `secondary_address` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `business_name` `business_name` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `business_type` `business_type` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `id_verified` `id_verified` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `identity_proof` `identity_proof` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `proof_type` `proof_type` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `proof_verified` `proof_verified` TINYINT DEFAULT 0");
            DB::statement("ALTER TABLE `campaigns` CHANGE `paypal_email` `paypal_email` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `meta_title` `meta_title` VARCHAR(255) DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `meta_keyword` `meta_keyword` TEXT DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `meta_description` `meta_description` TEXT DEFAULT ''");
            DB::statement("ALTER TABLE `campaigns` CHANGE `shares_count` `shares_count` INTEGER DEFAULT 0");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            //
        });
    }
}
