<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorBackingsCampaignsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('campaigns', 'pledges_amount')) {
            DB::statement("ALTER TABLE `campaigns` CHANGE `pledges_amount` `funding_current` DECIMAL(8, 2)");
        }

        if (Schema::hasColumn('campaigns', 'return_multipliers')) {
            DB::statement("ALTER TABLE `campaigns` CHANGE `return_multipliers` `repayment_amount` INTEGER");
        }

        if (Schema::hasColumn('backings', 'pledges_amount')) {
            DB::statement("ALTER TABLE `backings` CHANGE `pledges_amount` `fund_amount` DECIMAL(8, 2)");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
