<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateObjectFieldFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            DB::statement("ALTER TABLE `files` CHANGE `object` `object` ENUM('project', 'user', 'post', 'group') DEFAULT 'user'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            DB::statement("ALTER TABLE `files` CHANGE `object` `object` ENUM('project', 'user') DEFAULT 'user'");
        });
    }
}
