<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfilingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profilings', function (Blueprint $table) {
            DB::statement("ALTER TABLE `profilings` CHANGE `type` `type` ENUM('campaigns', 'experiences', 'certifications', 'organizations', 'education') DEFAULT 'campaigns'");
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->dropColumn('period');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profilings', function (Blueprint $table) {
            //
        });
    }
}
