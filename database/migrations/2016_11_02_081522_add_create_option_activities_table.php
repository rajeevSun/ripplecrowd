<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreateOptionActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            DB::statement("ALTER TABLE `activities` CHANGE `action` `action` ENUM('post', 'share', 'like', 'follow', 'create') NOT NULL DEFAULT 'like'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            DB::statement("ALTER TABLE `activities` CHANGE `action` `action` ENUM('post', 'share', 'like', 'follow') NOT NULL DEFAULT 'like'");
        });
    }
}
