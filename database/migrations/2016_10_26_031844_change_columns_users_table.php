<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE `firstname` `first_name` VARCHAR(255)");
            DB::statement("ALTER TABLE `users` CHANGE `lastname` `last_name` VARCHAR(255)");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            DB::statement("ALTER TABLE `users` CHANGE `first_name` `firstname` VARCHAR(255)");
            DB::statement("ALTER TABLE `users` CHANGE `last_name` `lastname` VARCHAR(255)");
        });
    }
}
