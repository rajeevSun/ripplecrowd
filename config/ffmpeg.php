<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 11-Jan-17
 * Time: 14:02
 */
return [
    'ffmpeg.binaries' => 'C:\Users\USER\Desktop\ffmpeg-20170110-f48b6b8-win64-static\bin\ffmpeg.exe',
    'ffprobe.binaries' => 'C:\Users\USER\Desktop\ffmpeg-20170110-f48b6b8-win64-static\bin\ffprobe.exe',
    'timeout' => 3600, // The timeout for the underlying process
    'ffmpeg.threads' => 12,   // The number of threads that FFMpeg should use
];