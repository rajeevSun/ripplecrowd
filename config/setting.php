<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 03-Nov-16
 * Time: 14:27
 */
return [
    'activity' => [
        'default_per_page' => 20,
        'default_page' => 1
    ],
    'comment' => [
        'default_per_page' => 2,
        'default_page' => 1
    ],
    'group' => [
        'default_per_page' => 10,
        'default_page' => 1,
        'default_per_page_on_category' => 3
    ],
    'recommend_campaign' =>  [
        'default_per_page' => 2,
        'default_page' => 1
    ],
    'campaign' => [
        'default_per_page' => 15,
        'default_page' => 1,
        'default_sort_by' => 'updated_at',
        'default_order' => 'desc',
        'default_category' => 'all',
        'comment' => [
            'default_per_page' => 15,
            'default_page' => 1
        ]
    ],
    'statistic' => [
        'default_per_page' => 4,
        'default_page' => 1
    ],
    'user' => [
        'default_per_page' => 15,
        'default_page' => 1,
        'default_sort_by' => 'first_name',
        'default_order' => 'desc'
    ],
    'milestone' => [
        'default_per_page' => 15,
        'default_page' => 1,
        'default_sort_by' => 'created_at',
        'default_order' => 'desc'
    ],
    'meetup' => [
        'default_per_page' => 15,
        'default_page' => 1,
        'default_sort_by' => 'created_at',
        'default_order' => 'desc'
    ],
    'notification' => [
        'default_per_page' => 20,
        'default_page' => 1,
        'default_sort_by' => 'created_at',
        'default_order' => 'desc'
    ],
    'default_avatar' => 'uploads/images/users/sampleuser@gmail.com.png',
    'default_position' => 'New Joiner',
    'default_organization' => 'RippleCrowd'
];