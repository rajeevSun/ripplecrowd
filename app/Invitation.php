<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = 'invitations';

    protected $fillable = ['user_id', 'campaign_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;
}