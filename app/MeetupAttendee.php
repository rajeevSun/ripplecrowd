<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetupAttendee extends Model
{
    protected $table = 'meetup_attendees';

    protected $fillable = ['meetup_id', 'user_id', 'role'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;
}