<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 28-Oct-16
 * Time: 09:44
 */

namespace App\Helpers;

use Input;
use Carbon\Carbon;
use App\File as FileModel;
use Validator;

class File
{

    const UPLOAD_PATH = 'uploads/';
    const FULL_URL = 'https://crowd.tihub.net/';

    public static function upload($fieldName, $object, $objectId, $prefix = NULL, $uploadPath = NULL) {
        $file = Input::file($fieldName);
        if ($uploadPath) {
            $destinationPath = $uploadPath;
        } else {
            $destinationPath = self::UPLOAD_PATH;
        }
        if (!$file) {
            return '';
        }
        $fileMimeType = $file->getClientOriginalExtension();
        $fileName =  ($prefix ? $prefix . '_' : '') . md5(Carbon::now().rand(1, 100)) . '.'. $fileMimeType;
        $file->move('../../'.$destinationPath, $fileName);
        $fileLink = $destinationPath . $fileName;

        // save to file model
        $file = new FileModel();
        $file->object = $object;
        $file->object_id = $objectId;
        $file->path = $destinationPath;
        $file->file_name = $fileName;
        $file->link = $fileLink;
        $file->save();

        return $fileLink;
    }
    // upload image for user.
    public static function uploadFile($fieldName, $object, $objectId, $prefix = NULL) {
        $uploadPath = self::UPLOAD_PATH;
        return self::upload($fieldName, $object, $objectId, $prefix, $uploadPath);
    }
    // get full image link
    public static function getFullURL($url) {
        $rules = [
            'url' => 'required|url'
        ];

        $validator = Validator::make(['url' => $url], $rules);
        if ($validator->fails()) {
            if ($url) {
                return self::FULL_URL . $url;
            } else {
                return '';
            }
        } else {
            return $url;
        }
    }
}