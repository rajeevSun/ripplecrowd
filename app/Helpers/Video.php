<?php
namespace App\Helpers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 12-Jan-17
 * Time: 09:34
 */
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Format\Video\WebM;
use FFMpeg\Coordinate\TimeCode;
use App\File as FileModel;
use Carbon\Carbon;

class Video extends File {
    const UPLOAD_PATH = 'uploads/videos/';
    public static function uploadVideo($fieldName, $object, $objectId, $prefix = NULL) {
        $videoPath = self::uploadFile($fieldName, $object, $objectId, $prefix);
        if (!$videoPath) {
            return [];
        }
        $_videoPath = 'C:\xampp\htdocs\\' . str_replace("/", "\\", $videoPath);
        $videoPath = self::convertToMp4($_videoPath);

        //$videoPath = 'C:\xampp\htdocs\\' . str_replace("/", "\\", $videoPath);

        $videoDuration = self::getDuration($_videoPath);

        $thumbnailPath = self::getThumbnail($_videoPath, $object, $objectId);

        @unlink($videoPath);
        return [
            'video' => $videoPath,
            'duration' => $videoDuration,
            'image' => $thumbnailPath
        ];
    }
    public static function getDuration($videoPath) {
        $ffmpeg = resolve(FFMpeg::class);
        $video = $ffmpeg->open($videoPath);
        return $video->getFFProbe()->format($videoPath)->get('duration');
    }
    public static function getThumbnail($videoPath, $object, $objectId, $savePath = NULL) {
        $ffmpeg = resolve(FFMpeg::class);
        $video = $ffmpeg->open($videoPath);
        $frame = $video->frame(TimeCode::fromSeconds(0));

        if ($savePath) {
            $destinationPath = $savePath;
        } else {
            $destinationPath = self::UPLOAD_PATH;
        }
        $thumbnailName = md5(Carbon::now().rand(1, 100)) . '.jpg';

        //$saveImagePath = '../../' . $destinationPath . $thumbnailName;
        $saveImagePath = 'C:\xampp\htdocs\uploads\\' . $thumbnailName;

        $frame->save($saveImagePath);

        $fileLink = $destinationPath . $thumbnailName;

        // save to file model
        $file = new FileModel();
        $file->object = $object;
        $file->object_id = $objectId;
        $file->path = $destinationPath;
        $file->file_name = $thumbnailName;
        $file->link = $fileLink;
        $file->save();

        return $fileLink;
    }

    public static function convertToMp4($videoPath, $savePath = NULL) {
        $format = new WebM();
        $ffmpeg = resolve(FFMpeg::class);
        $video = $ffmpeg->open($videoPath);

        $videoName = md5(Carbon::now().rand(1, 100)) . '.mp4';

        if ($savePath) {
            $destinationPath = $savePath;
        } else {
            $destinationPath = self::UPLOAD_PATH;
        }

        //$videoName = '../../' . $destinationPath . $videoName;
        $saveVideoPath = 'C:\xampp\htdocs\uploads\\' . $videoName;

        $video->save($format, $saveVideoPath);

        $fileLink = $destinationPath . $videoName;
        return $fileLink;
    }

    public static function upload($fieldName, $object, $objectId, $prefix = NULL, $uploadPath = NULL) {
        $file = Input::file($fieldName);
        if ($uploadPath) {
            $destinationPath = $uploadPath;
        } else {
            $destinationPath = self::UPLOAD_PATH;
        }
        if (!$file) {
            return '';
        }
        $fileMimeType = $file->getClientOriginalExtension();
        $fileName =  ($prefix ? $prefix . '_' : '') . md5(Carbon::now().rand(1, 100)) . '.'. $fileMimeType;
        $file->move('../../'.$destinationPath, $fileName);
        $fileLink = $destinationPath . $fileName;

        return $fileLink;
    }
}