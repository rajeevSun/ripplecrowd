<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 28-Oct-16
 * Time: 09:44
 */

namespace App\Helpers;

use Input;
use Carbon\Carbon;
use App\File as FileModel;
use App\Campaign;
use Validator;

class Image
{

    const UPLOAD_PATH = 'uploads/images/';
    const FULL_URL = 'https://crowd.tihub.net/';

    public static function upload($fieldName, $object, $objectId, $prefix = NULL, $uploadPath = NULL) {
        $file = Input::file($fieldName);
        if ($uploadPath) {
            $destinationPath = $uploadPath;
        } else {
            $destinationPath = self::UPLOAD_PATH;
        }
        if (!$file) {
            return '';
        }
        $imagemimetype = $file->getClientOriginalExtension();
        $fileName =  ($prefix ? $prefix . '_' : '') . md5(Carbon::now().rand(1, 100)) . '.'. $imagemimetype;
        $file->move('../../'.$destinationPath, $fileName);
        $imglink = $destinationPath . $fileName;

        list($width, $height) = getimagesize('../../' . $destinationPath . $fileName);

        // save to file model
        $file = new FileModel();
        $file->object = $object;
        $file->object_id = $objectId;
        $file->path = $destinationPath;
        $file->file_name = $fileName;
        $file->width = $width;
        $file->height = $height;
        $file->link = $imglink;
        $file->save();

        $relatedClass = '\App\\'.ucfirst($object);
        $updateFields = [$fieldName => $imglink];
        if ($fieldName == 'image') $updateFields['image_id'] = $file->id;
        $relatedObj = $relatedClass::where('id',$objectId)->first();
        if ($relatedObj) {
            foreach ($updateFields as $field => $value) {
                $relatedObj->{$field} = $value;
            }
            $relatedObj->save();
        }

        return $imglink;
    }
    // upload image for campaign.
    public static function uploadCampaignImage($fieldName, $object, $objectId, $prefix = NULL) {
        $uploadPath = self::UPLOAD_PATH . 'campaigns/';
        return self::upload($fieldName, $object, $objectId, $prefix, $uploadPath);
    }
    // upload image for user.
    public static function uploadUserImage($fieldName, $object, $objectId, $prefix = NULL) {
        $uploadPath = self::UPLOAD_PATH . 'users/';
        return self::upload($fieldName, $object, $objectId, $prefix, $uploadPath);
    }
    // upload image for groups.
    public static function uploadGroupImage($fieldName, $object, $objectId, $prefix = NULL) {
        $uploadPath = self::UPLOAD_PATH . 'groups/';
        return self::upload($fieldName, $object, $objectId, $prefix, $uploadPath);
    }
    // upload image for meetups
    public static function uploadMeetupImage($fieldName, $object, $objectId, $prefix = NULL) {
        $uploadPath = self::UPLOAD_PATH . 'meetups/';
        return self::upload($fieldName, $object, $objectId, $prefix, $uploadPath);
    }
    // get full image link
    public static function getFullURL($url) {
        $rules = [
            'url' => 'required|url'
        ];

        $validator = Validator::make(['url' => $url], $rules);
        if ($validator->fails()) {
            if ($url) {
                return self::FULL_URL . $url;
            } else {
                return '';
            }
        } else {
            return $url;
        }
    }
}