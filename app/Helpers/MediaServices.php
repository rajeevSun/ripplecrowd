<?php
/**
 * Created by PhpStorm.
 * User: phonghdt
 * Date: 17-Jan-17
 * Time: 10:34
 */

namespace App\Helpers;

use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\Internal\MediaServicesSettings;
use WindowsAzure\Common\Internal\Utilities;
use WindowsAzure\MediaServices\Models\Asset;
use WindowsAzure\MediaServices\Models\AccessPolicy;
use WindowsAzure\MediaServices\Models\Locator;
use WindowsAzure\MediaServices\Models\Task;
use WindowsAzure\MediaServices\Models\Job;
use WindowsAzure\MediaServices\Models\TaskOptions;
use WindowsAzure\MediaServices\Models\ContentKey;
use WindowsAzure\MediaServices\Models\ProtectionKeyTypes;
use WindowsAzure\MediaServices\Models\ContentKeyTypes;
use WindowsAzure\MediaServices\Models\ContentKeyAuthorizationPolicy;
use WindowsAzure\MediaServices\Models\ContentKeyAuthorizationPolicyOption;
use WindowsAzure\MediaServices\Models\ContentKeyAuthorizationPolicyRestriction;
use WindowsAzure\MediaServices\Models\ContentKeyDeliveryType;
use WindowsAzure\MediaServices\Models\ContentKeyRestrictionType;
use WindowsAzure\MediaServices\Models\AssetDeliveryPolicy;
use WindowsAzure\MediaServices\Models\AssetDeliveryProtocol;
use WindowsAzure\MediaServices\Models\AssetDeliveryPolicyType;
use WindowsAzure\MediaServices\Models\AssetDeliveryPolicyConfigurationKey;
use WindowsAzure\MediaServices\Templates\PlayReadyLicenseResponseTemplate;
use WindowsAzure\MediaServices\Templates\PlayReadyLicenseTemplate;
use WindowsAzure\MediaServices\Templates\PlayReadyLicenseType;
use WindowsAzure\MediaServices\Templates\MediaServicesLicenseTemplateSerializer;
use WindowsAzure\MediaServices\Templates\WidevineMessage;
use WindowsAzure\MediaServices\Templates\AllowedTrackTypes;
use WindowsAzure\MediaServices\Templates\ContentKeySpecs;
use WindowsAzure\MediaServices\Templates\RequiredOutputProtection;
use WindowsAzure\MediaServices\Templates\Hdcp;
use WindowsAzure\MediaServices\Templates\TokenRestrictionTemplateSerializer;
use WindowsAzure\MediaServices\Templates\TokenRestrictionTemplate;
use WindowsAzure\MediaServices\Templates\SymmetricVerificationKey;
use WindowsAzure\MediaServices\Templates\TokenClaim;
use WindowsAzure\MediaServices\Templates\TokenType;
use WindowsAzure\MediaServices\Templates\WidevineMessageSerializer;
use Input;
use Carbon\Carbon;

class MediaServices
{
    protected $restProxy;
    protected $asset;
    protected $encodedAsset;
    protected $thumbnailAsset;
    protected $filePath;
    protected $videoUrl;
    protected $thumbnailUrl;

    const UPLOAD_PATH = 'uploads/video/';

    public function __construct(array $account) {
        $this->restProxy = ServicesBuilder::getInstance()->createMediaServicesService(
            new MediaServicesSettings($account['name'], $account['key']));
    }

    public function getVideoUrl() {
        if ($this->videoUrl) {
            return $this->videoUrl;
        } else {
            return '';
        }
    }

    public function getThumbnailUrl() {
        if ($this->thumbnailUrl) {
            return $this->thumbnailUrl;
        } else {
            return '';
        }
    }

    public function getFileStream($fieldName) {
        $file = Input::file($fieldName);
        if (!$file) {
            throw new \Exception("No file stream");
        }
        $fileMimeType = $file->getClientOriginalExtension();
        $fileName =  md5(Carbon::now().rand(1, 100)) . '.'. $fileMimeType;
        $file->move('../../'.self::UPLOAD_PATH, $fileName);
        $this->filePath = '../../'.self::UPLOAD_PATH.$fileName;
        return $this;
    }

    function uploadFileAndCreateAsset($file = NULL)
    {
        if (!$file) {
            $file = $this->filePath;
        }


        $fileName = basename($file);

        // create an empty "Asset" by specifying the name
        $this->asset = new Asset(Asset::OPTIONS_NONE);
        //$asset->setName('Mezzanine '.$mezzanineFileName);
        $this->asset->setName($fileName);
        $this->asset = $this->restProxy->createAsset($this->asset);
        $assetId = $this->asset->getId();

        //echo 'Asset created: name='.$asset->getName().' id='.$assetId."\r\n";

        // create an Access Policy with Write permissions
        $accessPolicy = new AccessPolicy('UploadAccessPolicy');
        $accessPolicy->setDurationInMinutes(60.0);
        //$accessPolicy->setPermissions(AccessPolicy::PERMISSIONS_WRITE );
        $accessPolicy->setPermissions(AccessPolicy::PERMISSIONS_WRITE |AccessPolicy::PERMISSIONS_LIST );
        $accessPolicy = $this->restProxy->createAccessPolicy($accessPolicy);

        // create a SAS Locator for the Asset
        $sasLocator = new Locator($this->asset,  $accessPolicy, Locator::TYPE_SAS);
        $sasLocator = $this->restProxy->createLocator($sasLocator);

        // get the mezzanine file content
        $fileContent = file_get_contents($file);

        //echo "Uploading...\r\n";

        // use the 'uploadAssetFile' to perform a multi-part upload using the Block Blobs REST API storage operations
        $this->restProxy->uploadAssetFile($sasLocator, $fileName, $fileContent);

        // notify Media Services that the file upload operation is done to generate the asset file metadata
        $this->restProxy->createFileInfos($this->asset);

        //echo 'File uploaded: size='.strlen($fileContent)."\r\n";

        // delete the SAS Locator (and Access Policy) for the Asset since we are done uploading files
        $this->restProxy->deleteLocator($sasLocator);
        $this->restProxy->deleteAccessPolicy($accessPolicy);

        //return $asset;
        return $this;
    }

    function encodeToAdaptiveBitrateMP4Set()
    {
        // Retrieve the latest 'Media Encoder Standard' processor version
        $mediaProcessor = $this->restProxy->getLatestMediaProcessor('Media Encoder Standard');
        //$mediaProcessor = $this->restProxy->getLatestMediaProcessor('Azure Media Encoder');

        //echo "Using Media Processor: {$mediaProcessor->getName()} version {$mediaProcessor->getVersion()}\r\n";
        // Create the Job; this automatically schedules and runs it
        $outputAssetName = 'Encoded '.$this->asset->getName();
        $outputAssetCreationOption = Asset::OPTIONS_NONE;

        $taskBody = '<?xml version="1.0" encoding="utf-8"?><taskBody><inputAsset>JobInputAsset(0)</inputAsset><outputAsset assetCreationOptions="' . $outputAssetCreationOption . '" assetName="' . $outputAssetName . '">JobOutputAsset(0)</outputAsset></taskBody>';


        $task = new Task($taskBody, $mediaProcessor->getId(), TaskOptions::NONE);
        //$task->setConfiguration('H264 Multiple Bitrate 720p');
        $task->setConfiguration('H264 Single Bitrate 720p');

        $job = new Job();
        $job->setName('Encoding Job');

        $job = $this->restProxy->createJob($job, array($this->asset), array($task));

        //echo "Created Job with Id: {$job->getId()}\r\n";

        // Check to see if the Job has completed
        $result = $this->restProxy->getJobStatus($job);

        $jobStatusMap = array('Queued', 'Scheduled', 'Processing', 'Finished', 'Error', 'Canceled', 'Canceling');

        while ($result != Job::STATE_FINISHED && $result != Job::STATE_ERROR && $result != Job::STATE_CANCELED) {
            //echo "Job status: {$jobStatusMap[$result]}\r\n";
            //sleep(10);
            $result = $this->restProxy->getJobStatus($job);
        }

        if ($result != Job::STATE_FINISHED) {
            //echo "The job has finished with a wrong status: {$jobStatusMap[$result]}\r\n";
            //exit(-1);
            throw new \Exception("The job has finished with a wrong status: {$jobStatusMap[$result]}");
        }

        //echo "Job Finished!\r\n";

        // Get output asset
        $outputAssets = $this->restProxy->getJobOutputMediaAssets($job);
        $this->encodedAsset = $outputAssets[0];

        //echo "Asset encoded: name={$this->encodedAsset->getName()} id={$this->encodedAsset->getId()}\r\n";
        //return $encodedAsset;
        //$this->encodedAsset = $this->asset;
        return $this;
    }

    function encodeToThumbnails()
    {
        // Retrieve the latest 'Media Encoder Standard' processor version
        $mediaProcessor = $this->restProxy->getLatestMediaProcessor('Azure Media Encoder');

        //echo "Using Media Processor: {$mediaProcessor->getName()} version {$mediaProcessor->getVersion()}\r\n";
        // Create the Job; this automatically schedules and runs it
        $outputAssetName = 'Encoded '.$this->asset->getName();
        $outputAssetCreationOption = Asset::OPTIONS_NONE;
        $taskBody = '<taskBody><inputAsset>JobInputAsset(0)</inputAsset>'
            .'<outputAsset>JobOutputAsset(0)</outputAsset></taskBody>';
        $thumbnailsConfig = <<<EOT
<?xml version="1.0" encoding="utf-8"?>
<Thumbnail Size="100%,*" Type="Jpeg" Filename="{OriginalFilename}_{ThumbnailIndex}.{DefaultExtension}">
  <Time Value="10%"/>
</Thumbnail>
EOT;


        $task = new Task($taskBody, $mediaProcessor->getId(), TaskOptions::NONE);
        $task->setConfiguration($thumbnailsConfig);

        $job = new Job();
        $job->setName('Encoding Job');

        $job = $this->restProxy->createJob($job, array($this->asset), array($task));

        //echo "Created Job with Id: {$job->getId()}\r\n";

        // Check to see if the Job has completed
        $result = $this->restProxy->getJobStatus($job);

        $jobStatusMap = array('Queued', 'Scheduled', 'Processing', 'Finished', 'Error', 'Canceled', 'Canceling');

        while ($result != Job::STATE_FINISHED && $result != Job::STATE_ERROR && $result != Job::STATE_CANCELED) {
            //echo "Job status: {$jobStatusMap[$result]}\r\n";
            //sleep(10);
            $result = $this->restProxy->getJobStatus($job);
        }

        if ($result != Job::STATE_FINISHED) {
            //echo "The job has finished with a wrong status: {$jobStatusMap[$result]}\r\n";
            //exit(-1);
            throw new \Exception("The job has finished with a wrong status: {$jobStatusMap[$result]}");
        }

        //echo "Job Finished!\r\n";

        // Get output asset
        $outputAssets = $this->restProxy->getJobOutputMediaAssets($job);
        $this->thumbnailAsset = $outputAssets[0];

        //echo "Asset encoded: name={$this->encodedAsset->getName()} id={$this->encodedAsset->getId()}\r\n";
        //return $encodedAsset;
        return $this;
    }

    function publishThumbnailAsset()
    {
        // Get the .ISM AssetFile
        $files = $this->restProxy->getAssetAssetFileList($this->thumbnailAsset);
        $manifestFile = null;

        foreach ($files as $file) {
            if ($file->getMimeType() == 'image/jpeg') {
                $manifestFile = $file;
            }
        }

        if ($manifestFile == null) {
            throw new \Exception("Unable to found the manifest file.");
        }

        // Create a 30-day read-only AccessPolicy
        $access = new AccessPolicy('Progressive Access Policy');
        $access->setDurationInMinutes(60 * 24 * 30);
        $access->setPermissions(AccessPolicy::PERMISSIONS_READ);
        $access = $this->restProxy->createAccessPolicy($access);

        // Create a Locator using the AccessPolicy and Asset
        $locator = new Locator($this->thumbnailAsset, $access, Locator::TYPE_SAS);
        $locator->setName('Progressive Locator');
        $locator = $this->restProxy->createLocator($locator);

        // Create a Smooth Streaming base URL
        $thumbnailUrl = $this->buildMediaUrl($locator->getPath(), $manifestFile->getName());
        $this->thumbnailUrl = $thumbnailUrl;
        //echo "Streaming URL: {$stremingUrl}\r\n";
        return $this;
    }

    function publishEncodedAsset()
    {
        // Get the .ISM AssetFile
        //$files = $this->restProxy->getAssetAssetFileList($this->encodedAsset); // we not need to get encoded asset. current we get origin asset
        $files = $this->restProxy->getAssetAssetFileList($this->asset);
        $manifestFile = null;

        foreach ($files as $file) {
            if (endsWith(strtolower($file->getName()), '.mp4') || endsWith(strtolower($file->getName()), '.mov')) {
                $manifestFile = $file;
            }
        }

        if ($manifestFile == null) {
            throw new \Exception("Unable to found the manifest file.");
        }

        // Create a 30-day read-only AccessPolicy
        $access = new AccessPolicy('Progressive Access Policy');
        $access->setDurationInMinutes(60 * 24 * 30);
        $access->setPermissions(AccessPolicy::PERMISSIONS_READ);
        $access = $this->restProxy->createAccessPolicy($access);

        // Create a Locator using the AccessPolicy and Asset
        $locator = new Locator($this->asset, $access, Locator::TYPE_SAS);
        $locator->setName('Progressive Locator');
        $locator = $this->restProxy->createLocator($locator);

        // Create a Smooth Streaming base URL
        $videoUrl = $this->buildMediaUrl($locator->getPath(), $manifestFile->getName());
        $this->videoUrl = $videoUrl;
        //echo "Streaming URL: {$stremingUrl}\r\n";
        return $this;
    }

    public function buildMediaUrl($path, $fileName) {
        return str_replace('?', '/'.$fileName.'?', $path);
    }
}