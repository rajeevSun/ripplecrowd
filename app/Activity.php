<?php

namespace App;

use App\Follow;
use App\Owner;
use App\Share;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\NewsfeedMessage;
use App\User;
use App\Like;
use App\Comment;
use App\GroupMember;
use App\Compilation;
use App\Campaign;
use Image;
use Carbon\Carbon;

class Activity extends Model
{
    use SoftDeletes, Owner;

    protected $table = 'activities';

    protected $fillable = ['user_id', 'action', 'action_id', 'message', 'likes', 'wall_id', 'type'];

    public $notificationMap = [
        'yourself' => [
            'follow_user' => 'FOLLOW_YOU',
            'follow_campaign' => 'FOLLOW_YOUR_CAMPAIGN',
            'share_campaign' => 'SHARE_YOUR_CAMPAIGN',
            'share_post_text' => 'SHARE_YOUR_POST',
            'share_post_image_internal' => 'SHARE_YOUR_POST',
            'share_post_video_internal' => 'SHARE_YOUR_POST',
            'share_post_video_external' => 'SHARE_YOUR_POST',
            'share_post_image_external' => 'SHARE_YOUR_POST',
            'share_post_campaign_link' => 'SHARE_YOUR_POST',
            'share_post_website_external' => 'SHARE_YOUR_POST',
            'like_post' => 'LIKE_YOUR_POST',
            'like_campaign' => 'LIKE_YOUR_CAMPAIGN',
            'comment_post' => 'COMMENT_YOUR_POST',
            'comment_campaign' => 'COMMENT_YOUR_CAMPAIGN',
        ],
        'followers' => [
            'create_milestone' => 'CREATE_MILESTONE',
            'create_campaign' => 'CREATE_CAMPAIGN'
        ],
    ];

    public function getNotificationMap() {
        return $this->notificationMap;
    }

    protected $attributes = [
        'user_id' => 0,
        'action' => '',
        'action_id' => '',
        'wall_id' => '',
        'type' => ''
    ];

    protected $hidden = ['activity', 'activitable'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;

    public function retrieveInfo($userId) {
        $this->setInfoAttribute($userId);
        $this->setRelatedInfoAttribute($userId);
    }

    public function getInfoAttribute($value) {
        return $value;
    }

    public function getRelatedInfoAttribute($value) {
        return $value;
    }

    public function getSuggestedUsers($value) {
        return $value;
    }

    public function getSuggestedCampaigns($value) {
        return $value;
    }

    public function setSuggestedUsers($value) {
        $this->attributes['suggested_users'] = [];
    }

    public function setSuggestedCampaigns($value) {
        $this->attributes['suggested_campaigns'] = [];
    }

    // set info attribute
    public function setInfoAttribute($userId)
    {
        try {
            $activityObject = $this->getRelation('activity');
            if (!$activityObject) {
                $activityObject = $this->load('activity')->getRelation('activity');
            }

            if (get_class($activityObject) == Post::class) {
                $this->attributes['info'] = $activityObject->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }

            if (get_class($activityObject) == Follow::class) {
                $this->attributes['info'] = $activityObject->followable()->first()->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }

            if (get_class($activityObject) == Like::class) {
                $this->attributes['info'] = $activityObject->likable()->first()->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }

            if (get_class($activityObject) == Comment::class) {
                $this->attributes['info'] = $activityObject->commentable()->first()->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }

            if (get_class($activityObject) == GroupMember::class) {
                $this->attributes['info'] = $activityObject->group()->first()->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }

            if (get_class($activityObject) == Milestone::class) {
                $this->attributes['info'] = $activityObject->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }

            if (get_class($activityObject) == Share::class) {
                $this->attributes['info'] = $activityObject->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }

            if (get_class($activityObject) == Campaign::class) {
                $this->attributes['info'] = $activityObject->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }

            if (get_class($activityObject) == Compilation::class) {
                $this->attributes['info'] = $activityObject->toNewsFeed($userId, $this->id, $this->action, $this->action_id);
            }
        } catch (\Exception $e) {
            $this->attributes['info'] = NULL;
        }
    }

    public function setRelatedInfoAttribute($value) {
        try {
            $activityObject = $this->getRelation('activity');
            if (!$activityObject) {
                $activityObject = $this->load('activity')->getRelation('activity');
            }

            $this->attributes['related_info'] = [];

            if (get_class($activityObject) == Post::class) {
                $relatedObject = $activityObject;
                $relatedObject->append('user');
                $relatedObject->fetchDimension();
                // get campaign of post
                $campaign = $activityObject->campaign()->first();
                if (get_class($campaign) == Campaign::class) {
                    $className = new \ReflectionClass($campaign);
                    $campaign->append('category_name', 'days_remaining', 'percent_funded', 'user');
                    $this->attributes['related_info'][strtolower($className->getShortName())] = $campaign;
                }
            }

            if (get_class($activityObject) == Campaign::class) {
                $activityObject->append('category_name', 'days_remaining', 'percent_funded', 'user');
                $relatedObject = $activityObject;
            }

            if (get_class($activityObject) == Compilation::class) {
                $relatedObject = $activityObject;
                if ($activityObject->object == 'follow') {
                    $objects = $activityObject->followUsers()->distinct()->get();
                    foreach ($objects as $object) {
                        $object->checkFollowed($value);
                    }
                    if ($objects->count()) {
                        $this->attributes['additional_info']['users'] = $objects->slice(0, 2);
                        $this->attributes['additional_info']['view_more'] = $objects->count()>2;
                    }
                } elseif ($activityObject->object == 'join') {
                    $objects = $activityObject->groups()->take(2)->distinct()->get();
                    foreach ($objects as $object) {
                        $object->checkJoined($value);
                    }
                    $this->attributes['additional_info']['view_more'] = $objects->count()>2;
                    if ($objects->count()) {
                        $this->attributes['additional_info']['groups'] = $objects->slice(0, 2);
                        $this->attributes['additional_info']['view_more'] = $objects->count()>2;
                    }
                } elseif ($activityObject->object == 'suggest') {
                    $objects = $activityObject->suggestUsers()->distinct()->get();
                    if ($objects->count()) {
                        foreach ($objects as $object) {
                            $object->checkFollowed($value);
                        }
                        $this->attributes['additional_info']['users'] = $objects->slice(0, 2);
                        // check recommend users
                        $currentUser = User::find($value);
                        $followings = $currentUser->usersFollowing()->pluck('follows.object_id');
                        $categories = $currentUser->categories()->pluck('categories.id');
                        $suggestUsersCount = User::whereNotIn('users.id', $followings)
                            ->join('interests', 'interests.user_id', '=', 'users.id')
                            ->whereIn('interests.category_id', $categories)
                            ->where('users.id', '<>', $currentUser->id)
                            ->distinct()
                            ->count();
                        $this->attributes['additional_info']['view_more'] = $suggestUsersCount>2;
                    } else {
                        $objects = $activityObject->suggestCampaigns()->distinct()->get();
                        if ($objects->count()) {
                            foreach ($objects as $object) {
                                $object->checkFollowed($value);
                                $object->append('category_name', 'days_remaining', 'percent_funded', 'user');
                            }
                            $this->attributes['additional_info']['campaigns'] = $objects->slice(0,1);
                            // check recommend campaign
                            $currentUser = User::find($value);
                            $followedCampaignIds = $currentUser->followedCampaigns()->pluck('campaigns.id');
                            $suggestCampaignsCount = Campaign::join('interests', 'interests.category_id', '=', 'campaigns.category_id')
                                ->join('users', 'users.id', '=', 'campaigns.user_id')
                                ->where('interests.user_id', '=', $currentUser->id)
                                ->where('campaigns.user_id', '<>', $currentUser->id)
                                ->whereNotIn('campaigns.id', $followedCampaignIds)
                                ->join('categories', 'categories.id', '=', 'campaigns.category_id')
                                ->where('campaigns.funding_goal', '!=', 0)
                                ->where('campaigns.campaign_verified', '=', 2)
                                ->distinct()
                                ->count();
                            $this->attributes['additional_info']['view_more'] = $suggestCampaignsCount>1;
                        }
                    }
                }
            }

            if (get_class($activityObject) == Milestone::class) {
                $relatedObject = $activityObject;
                $relatedObject->fetchDimension();
                // get campaign of milestone.
                $campaign = $activityObject->campaign()->first();
                if (get_class($campaign) == Campaign::class) {
                    $className = new \ReflectionClass($campaign);
                    $campaign->append('category_name', 'days_remaining', 'percent_funded', 'user');
                    $this->attributes['related_info'][strtolower($className->getShortName())] = $campaign;
                }
            }

            if (get_class($activityObject) == Share::class) {
                $relatedObject = $activityObject;
                // get sharable object
                $sharableObject = $activityObject->sharable()->first();
                $className = new \ReflectionClass($sharableObject);
                $this->attributes['related_info'][strtolower($className->getShortName())] = $sharableObject;
                // if sharable object is a milestone, get campaign of that milestone also
                if (get_class($sharableObject) == Milestone::class || (get_class($sharableObject) == Post::class && $sharableObject->type == 'campaign_link')) {
                    $campaign = $sharableObject->campaign()->first();
                    if (get_class($campaign) == Campaign::class) {
                        $className = new \ReflectionClass($campaign);
                        if ($campaign) {
                            $campaign->append('category_name', 'days_remaining', 'percent_funded', 'user');
                            $this->attributes['related_info']['user'] = $campaign->user()->first();
                        }
                        $this->attributes['related_info'][strtolower($className->getShortName())] = $campaign;
                    }
                }
            }

            if (get_class($activityObject) == Follow::class) {
                $relatedObject = $activityObject->followable()->first();
                if (get_class($relatedObject) == Campaign::class) {
                    $relatedObject->append('category_name', 'days_remaining', 'percent_funded', 'user');
                }
            }

            if (get_class($activityObject) == Like::class) {
                $relatedObject = $activityObject->likable()->first();
                if (get_class($relatedObject) == Campaign::class) {
                    $relatedObject->append('category_name', 'days_remaining', 'percent_funded', 'user');
                }
                elseif (get_class($relatedObject) == Post::class) {
                    $relatedObject->append('user');
                }
            }

            if (get_class($activityObject) == Comment::class) {
                $relatedObject = $activityObject->commentable()->first();
                if (get_class($relatedObject) == Campaign::class) {
                    $relatedObject->append('category_name', 'days_remaining', 'percent_funded', 'user');
                }
                elseif (get_class($relatedObject) == Post::class) {
                    $relatedObject->append('user');
                }
            }

            if (get_class($activityObject) == GroupMember::class) {
                $relatedObject = $activityObject->group()->first();
            }

            if (isset($relatedObject) && $relatedObject) {
                $className = new \ReflectionClass($relatedObject);
                $this->attributes['related_info'][strtolower($className->getShortName())] = $relatedObject;
            } else {
                $this->attributes['related_info'] = new \stdClass();
            }

            if (!isset($this->attributes['additional_info'])) {
                $this->attributes['additional_info'] = new \stdClass();
            }

        } catch (\Exception $e) {
            $this->attributes['related_info'] = new \stdClass();
            $this->attributes['additional_info'] = new \stdClass();
        }
    }

    public function getCommentableRelateObject()
    {
        try {
            $activityObject = $this->getRelation('activity');

            if (!$activityObject) {
                $activityObject = $this->load('activity')->getRelation('activity');
            }

            $activityObjectClass = get_class($activityObject);
            if (($activityObjectClass == Post::class) || ($activityObjectClass == Milestone::class) || ($activityObjectClass == Share::class) || ($activityObjectClass == Campaign::class)) {
                return $activityObject;
            }

            if ($activityObjectClass == Follow::class) {
                return $activityObject->followable()->first();
            }

            if ($activityObjectClass == Like::class) {
                return $activityObject->likable()->first();
            }

            if ($activityObjectClass == Comment::class) {
                return $activityObject->commentable()->first();
            }

            return NULL;

        } catch (\Exception $e) {
            return NULL;
        }
    }

    public function getLikableRelateObject()
    {
        try {
            $activityObject = $this->getRelation('activity');

            if (!$activityObject) {
                $activityObject = $this->load('activity')->getRelation('activity');
            }

            $activityObjectClass = get_class($activityObject);
            if (($activityObjectClass == Post::class) || ($activityObjectClass == Milestone::class) || ($activityObjectClass == Share::class) || ($activityObjectClass == Campaign::class)) {
                return $activityObject;
            }

            if ($activityObjectClass == Follow::class) {
                return $activityObject->followable()->first();
            }

            return NULL;

        } catch (\Exception $e) {
            return NULL;
        }
    }

    public function getSharableRelateObject()
    {
        try {
            $activityObject = $this->getRelation('activity');

            if (!$activityObject) {
                $activityObject = $this->load('activity')->getRelation('activity');
            }

            $activityObjectClass = get_class($activityObject);
            if (($activityObjectClass == Post::class) || ($activityObjectClass == Milestone::class) || ($activityObjectClass == Campaign::class)) {
                return $activityObject;
            }

            if ($activityObjectClass == Share::class) {
                return $activityObject->sharable()->first();
            }

            return NULL;

        } catch (\Exception $e) {
            return NULL;
        }
    }

    // set message attribute
    public function setMessageAttribute($value)
    {
        $this->attributes['message'] = json_encode($value);
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likes', 'object', 'object_id', 'id');
    }

    public function likedUsers()
    {
        return $this->morphToMany(User::class, NULL, 'likes', 'object', 'object_id', 'user_id');
    }

    public function shares()
    {
        return $this->morphMany(Share::class, 'shares', 'object', 'object_id', 'id');
    }

    public function sharedUsers()
    {
        return $this->morphToMany(User::class, NULL, 'shares', 'object', 'object_id', 'user_id');
    }

    public function commentUsers()
    {
        return $this->morphToMany(User::class, NULL, 'comments', 'object', 'object_id', 'user_id');
    }

    public function activitable()
    {
        return $this->morphTo('activity', 'action', 'action_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getAvatarAttribute($value)
    {
        if (!$value) {
            return Image::getFullURL(config('setting.default_avatar'));
        }
        return Image::getFullURL($value);
    }

    public function getPositionAttribute($value) {
        if (!$value) {
            return config('setting.default_position');
        }
        return $value;
    }

    public function getOrganizationAttribute($value) {
        if (!$value) {
            return config('setting.default_organization');
        }
        return $value;
    }

    public function getNotificationInfo() {
        $activitableObject = $this->activitable()->first();
        $user = $this->user()->select(['users.*', 'users.image AS avatar'])->first();
        if (get_class($activitableObject) == Follow::class) { // if follow
            $followableObject = $activitableObject->followable()->first();
            //check if followable object is User
            if (get_class($followableObject) == User::class) {
                if ($followableObject->user_id != $activitableObject->user()->first()->id) {
                    if (isset($this->notificationMap['yourself'][$this->type])) {
                        $notificationKey = $this->notificationMap['yourself'][$this->type];
                    } else {
                        return NULL;
                    }
                    $notificationArgs = [$user->first_name.' '.$user->last_name];
                    $notificationRelatedObjectId = $followableObject->id;
                    $notificationOwnerId = $followableObject->id;
                    $notificationTargetId = $user->id;
                    $notificationTargetName = $user->first_name.' '.$user->last_name;
                    $notificationTargetObject = 'user';
                } else {
                    return NULL;
                }
            } elseif (get_class($followableObject) == Campaign::class) { // check if followable object is Campaign
                if ($followableObject->user_id != $activitableObject->user()->first()->id) {
                    if (isset($this->notificationMap['yourself'][$this->type])) {
                        $notificationKey = $this->notificationMap['yourself'][$this->type];
                    } else {
                        return NULL;
                    }
                    $notificationArgs = [$user->first_name.' '.$user->last_name, $followableObject->name];
                    $notificationRelatedObjectId = $followableObject->id;
                    $notificationOwnerId = $followableObject->user_id;
                    $notificationTargetId = $followableObject->id;
                    $notificationTargetName = $followableObject->name;
                    $notificationTargetObject = 'campaign';
                } else {
                    return NULL;
                }
            }
        } elseif (get_class($activitableObject) == Share::class) { // if share
            $sharableObject = $activitableObject->sharable()->first();
            // check if sharable object is Post
            if (get_class($sharableObject) == Post::class) {
                if ($sharableObject->user_id != $activitableObject->user()->first()->id) {
                    if (isset($this->notificationMap['yourself'][$this->type])) {
                        $notificationKey = $this->notificationMap['yourself'][$this->type];
                    } else {
                        return NULL;
                    }
                    $notificationArgs = [$user->first_name.' '.$user->last_name];
                    $notificationRelatedObjectId = $sharableObject->id;
                    $notificationOwnerId = $sharableObject->user_id;
                    $notificationTargetId = $sharableObject->id;
                    $notificationTargetName = '';
                    $notificationTargetObject = 'post';
                } else {
                    return NULL;
                }
            } elseif (get_class($sharableObject) == Campaign::class) { // check if sharable object is Campaign
                if ($sharableObject->user_id != $activitableObject->user()->first()->id) {
                    if (isset($this->notificationMap['yourself'][$this->type])) {
                        $notificationKey = $this->notificationMap['yourself'][$this->type];
                    } else {
                        return NULL;
                    }
                    $notificationArgs = [$user->first_name.' '.$user->last_name, $sharableObject->name];
                    $notificationRelatedObjectId = $sharableObject->id;
                    $notificationOwnerId = $sharableObject->user_id;
                    $notificationTargetId = $sharableObject->id;
                    $notificationTargetName = $sharableObject->name;
                    $notificationTargetObject = 'campaign';
                } else {
                    return NULL;
                }
            }
        } elseif (get_class($activitableObject) == Comment::class) { // if comment
            $commentableObject = $activitableObject->commentable()->first();
            // check if commentable object is Post
            if (get_class($commentableObject) == Post::class) {
                if ($commentableObject->user_id != $activitableObject->user()->first()->id) {
                    if (isset($this->notificationMap['yourself'][$this->type])) {
                        $notificationKey = $this->notificationMap['yourself'][$this->type];
                    } else {
                        return NULL;
                    }
                    $notificationArgs = [$user->first_name.' '.$user->last_name];
                    $notificationRelatedObjectId = $commentableObject->id;
                    $notificationOwnerId = $commentableObject->user_id;
                    $notificationTargetId = $commentableObject->id;
                    $notificationTargetName = '';
                    $notificationTargetObject = 'post';
                } else {
                    return NULL;
                }
            } elseif (get_class($commentableObject) == Campaign::class) { // check if commentable object is Campaign
                if ($commentableObject->user_id != $activitableObject->user()->first()->id) {
                    if (isset($this->notificationMap['yourself'][$this->type])) {
                        $notificationKey = $this->notificationMap['yourself'][$this->type];
                    } else {
                        return NULL;
                    }
                    $notificationArgs = [$user->first_name.' '.$user->last_name, $commentableObject->name];
                    $notificationRelatedObjectId = $commentableObject->id;
                    $notificationOwnerId = $commentableObject->user_id;
                    $notificationTargetId = $commentableObject->id;
                    $notificationTargetName = $commentableObject->name;
                    $notificationTargetObject = 'campaign';
                } else {
                    return NULL;
                }
            }
        } elseif (get_class($activitableObject) == Like::class) { // if like
            $likableObject = $activitableObject->likable()->first();
            // check if likable object is Post
            if (get_class($likableObject) == Post::class) {
                if ($likableObject->user_id != $activitableObject->user()->first()->id) {
                    if (isset($this->notificationMap['yourself'][$this->type])) {
                        $notificationKey = $this->notificationMap['yourself'][$this->type];
                    } else {
                        return NULL;
                    }
                    $notificationArgs = [$user->first_name.' '.$user->last_name];
                    $notificationRelatedObjectId = $likableObject->id;
                    $notificationOwnerId = $likableObject->user_id;
                    $notificationTargetId = $likableObject->id;
                    $notificationTargetName = '';
                    $notificationTargetObject = 'post';
                } else {
                    return NULL;
                }
            } elseif (get_class($likableObject) == Campaign::class) { // check if likable object is Campaign
                if ($likableObject->user_id != $activitableObject->user()->first()->id) {
                    if (isset($this->notificationMap['yourself'][$this->type])) {
                        $notificationKey = $this->notificationMap['yourself'][$this->type];
                    } else {
                        return NULL;
                    }
                    $notificationArgs = [$user->first_name.' '.$user->last_name, $likableObject->name];
                    $notificationRelatedObjectId = $likableObject->id;
                    $notificationOwnerId = $likableObject->user_id;
                    $notificationTargetId = $likableObject->id;
                    $notificationTargetName = $likableObject->name;
                    $notificationTargetObject = 'campaign';
                } else {
                    return NULL;
                }
            }
        } elseif (get_class($activitableObject) == Milestone::class) {
            if ($activitableObject->user_id != $activitableObject->user()->first()->id) {
                return NULL;
            } else {
                if (isset($this->notificationMap['followers'][$this->type])) {
                    $notificationKey = $this->notificationMap['followers'][$this->type];
                } else {
                    return NULL;
                }
                $notificationArgs = [$user->first_name.' '.$user->last_name, $activitableObject->title];
                $notificationRelatedObjectId = $activitableObject->id;
                $notificationOwnerId = $activitableObject->user_id;
                $notificationTargetId = $activitableObject->campaign_id;
                $campaign = $activitableObject->campaign()->first();
                $notificationTargetName = $campaign ? $campaign->name : '';
                $notificationTargetObject = 'campaign';
            }
        } elseif (get_class($activitableObject) == Campaign::class) {
            if ($activitableObject->user_id != $activitableObject->user()->first()->id) {
                return NULL;
            } else {
                if (isset($this->notificationMap['followers'][$this->type])) {
                    $notificationKey = $this->notificationMap['followers'][$this->type];
                } else {
                    return NULL;
                }
                $notificationArgs = [$user->first_name.' '.$user->last_name, $activitableObject->name];
                $notificationRelatedObjectId = $activitableObject->id;
                $notificationOwnerId = $activitableObject->user_id;
                $notificationTargetId = $activitableObject->id;
                $notificationTargetName = $activitableObject->name;
                $notificationTargetObject = 'campaign';
            }
        } else {
            return NULL;
        }

        if (!isset($notificationKey)) {
            return NULL;
        }

        return [
            'notificationKey' => $notificationKey,
            'notificationArgs' => $notificationArgs,
            'notificationRelatedObjectId' => $notificationRelatedObjectId,
            'notificationOwnerId' => $notificationOwnerId,
            'notificationObjectId' => $this->action_id,
            'notificationObjectType' => $this->action,
            'notificationTargetId' => $notificationTargetId,
            'notificationTargetName' => $notificationTargetName,
            'notificationTargetObject' => $notificationTargetObject,
            'notificationUserId' => $user->id,
            'notificationTargetActivityId' => $this->id,
            'notificationAvatar' => $user->avatar
        ];
    }
}