<?php
namespace App\Facades;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 16-Jan-17
 * Time: 11:45
 */
use Illuminate\Support\Facades\Facade;

/**
 * @see \Illuminate\Notifications\ChannelManager
 */
class MediaServices extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Helpers\MediaServices';
    }
}
