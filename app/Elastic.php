<?php
/**
 * Created by PhpStorm.
 * User: phonghdt
 * Date: 28-Dec-16
 * Time: 11:39
 */

namespace App;

use Elasticsearch\Client;

class Elastic
{
    protected $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    /**
     * Index a signle item
     *
     * @param array $parameters [index, type, id, body]
     * @return array
     */
    public function index(array $parameters) {
        try {
            return $this->client->index($parameters);
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    /**
     * Delete a single item
     *
     * @param array $parameters
     * @return array
     */
    public function delete(array $parameters) {
        try {
            return $this->client->delete($parameters);
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function search(array $parameters) {
        try {
            return $this->client->search($parameters);
        } catch(\Exception $e) {
            return [];
        }
    }

    public function msearch(array $parameters) {
        try {
            return $this->client->msearch($parameters);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function indices() {
        try {
            return $this->client->indices();
        } catch (\Exception $e) {
            return NULL;
        }
    }
}