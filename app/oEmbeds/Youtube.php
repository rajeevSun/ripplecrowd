<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 10-Jan-17
 * Time: 14:42
 */

namespace App\oEmbeds;
use App\Entities\PostOEmbed;

class Youtube extends oEmbed implements oEmbedInterface
{
    /**
     * Youtube constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $base = 'https://www.youtube.com/oembed?url=';
        parent::__construct($base, $url);
    }

    /**
     * @return string
     */
    public function buildUri()
    {
        $uri = parent::buildUri();
        return $uri . '&format=json';
    }

    /**
     * @return PostOEmbed
     */
    public function getPostOEmbed()
    {
        $result = $this->makeCall();
        if ($result) {
            return new PostOEmbed([
                'title' => $result['title'],
                'description' => '',
                'url' => $this->url,
                'thumbnail' => $result['thumbnail_url']
            ]);
        }
    }
}