<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 10-Jan-17
 * Time: 14:42
 */

namespace App\oEmbeds;


use App\Entities\PostOEmbed;
use Dusterio\LinkPreview\Client as Preview;

class Link extends oEmbed implements oEmbedInterface
{
    /**
     * Vimeo constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $base = '';
        parent::__construct($base, $url);
    }

    /**
     * @return PostOEmbed
     */
    public function getPostOEmbed()
    {
        try {
            $website = new Preview($this->getUrl());
            $preview = $website->getPreview('general');
            if ($preview) {
                return new PostOEmbed([
                    'title' => $preview->getTitle(),
                    'description' => $preview->getDescription(),
                    'url' => $this->url,
                    'thumbnail' => $preview->getCover()
                ]);
            }
        } catch (\Exception $e) {
            return FALSE;
        }
    }
}