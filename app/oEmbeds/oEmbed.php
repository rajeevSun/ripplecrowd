<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 10-Jan-17
 * Time: 14:43
 */

namespace App\oEmbeds;


class oEmbed
{
    public $url;
    public $base;
    public $uri;

    /**
     * oEmbed constructor.
     * @param $base
     * @param $url
     */
    public function __construct($base, $url) {
        $this->setBase($base);
        $this->setUrl($url);
        $this->buildUri();
    }

    /**
     * @param $base
     */
    public function setBase($base) {
        $this->base = $base;
    }

    /**
     * @return mixed
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * @param $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl() {
        return $this->url;
    }

    public function buildUri() {
        $this->uri = $this->base . $this->url;
    }

    /**
     * Get oEmbed data from base + url
     * @return mixed
     */
    public function makeCall() {
        // init api request
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        // process request api
        $result = curl_exec($ch);

        if (!$result) {
            throw new RuntimeException(
                "oEmbed send error:" . curl_error($ch)
            );
        }

        // close connection
        curl_close($ch);
        return json_decode($result, TRUE);
    }
}