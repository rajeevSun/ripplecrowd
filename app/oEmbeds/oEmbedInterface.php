<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 10-Jan-17
 * Time: 15:01
 */

namespace App\oEmbeds;

interface oEmbedInterface
{
    public function getPostOEmbed();
}