<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 10-Jan-17
 * Time: 14:42
 */

namespace App\oEmbeds;


use App\Entities\PostOEmbed;

class Vimeo extends oEmbed implements oEmbedInterface
{
    /**
     * Vimeo constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $base = 'https://vimeo.com/api/oembed.json?url=';
        parent::__construct($base, $url);
    }

    /**
     * @return PostOEmbed
     */
    public function getPostOEmbed()
    {
        $result = $this->makeCall();
        if ($result) {
            return new PostOEmbed([
                'title' => $result['title'],
                'description' => $result['description'],
                'url' => $this->url,
                'thumbnail' => $result['thumbnail_url']
            ]);
        }
    }
}