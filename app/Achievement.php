<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Achievement extends Model
{
    use SoftDeletes;

    protected $table = 'achievements';

    protected $fillable = ['name','description', 'type', 'point'];

    protected $hidden = ['deleted_at'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = TRUE;

    public function image() {
        return $this->morphOne(File::class, NULL, 'object', 'object_id', 'image_id');
    }
}