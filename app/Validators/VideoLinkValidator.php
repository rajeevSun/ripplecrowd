<?php
namespace App\Validators;
use Illuminate\Validation\Validator;

class VideoLinkValidator extends Validator {
    public static function checkVideoLink($attribute, $value, $parameters) {
        $link = parse_url($value, PHP_URL_HOST);
        if ($link == 'vimeo.com' || $link == 'www.youtube.com' || $link == '') {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}