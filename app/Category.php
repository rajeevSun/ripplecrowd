<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Group;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    protected $fillable = ['name'];

    protected $hidden = ['status'];

    // date mutators
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public $timestamps = TRUE;
    // groups that have this category.
    public function groups() {
        return $this->hasMany(Group::class, 'category_id', 'id');
    }
    // groups that have this category.
    public function groupsWithPage($page = NULL, $perPage = NULL) {
        if (!$page) {
            $page = config('setting.group.default_page');
        }
        if (!$perPage) {
            $perPage = config('setting.group.default_per_page_on_category');
        }
        return $this->hasMany(Group::class, 'category_id', 'id')
            ->skip(($page-1)*$perPage)
            ->take($perPage);
    }
}