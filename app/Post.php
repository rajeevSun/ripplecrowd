<?php

namespace App;

use App\Owner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\NewsfeedMessage;
use Image;
use App\NewsFeedInterface;
use App\User;
use App\Like;
use App\Share;
use App\Comment;
use App\Activity;
use App\Wall;
use App\File;

class Post extends Model implements NewsFeedInterface
{
    use SoftDeletes, Owner;

    protected $table = 'posts';

    protected $fillable = ['message', 'type', 'image', 'image_id', 'video'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    protected $touches = ['activity'];

    public $timestamps = TRUE;

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function campaign() {
        return $this->belongsTo(Campaign::class, 'campaign_id', 'id');
    }

    public function likes() {
        return $this->morphMany(Like::class, NULL, 'object', 'object_id', 'id');
    }

    public function likedUsers() {
        return $this->morphToMany(User::class, NULL, 'likes', 'object', 'object_id', 'user_id');
    }

    public function shares() {
        return $this->morphMany(Share::class, 'shares', 'object', 'object_id', 'id');
    }

    public function sharedUsers() {
        return $this->morphToMany(User::class, NULL, 'shares', 'object', 'object_id', 'user_id');
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'comments', 'object', 'object_id', 'id');
    }

    public function commentUsers() {
        return $this->morphToMany(User::class, NULL, 'comments', 'object', 'object_id', 'user_id');
    }

    public function activity() {
        return $this->morphMany(Activity::class, 'activities', 'action', 'action_id', 'id');
    }

    public function activityUsers() {
        return $this->morphToMany(User::class, NULL, 'activities', 'action', 'action_id', 'user_id');
    }

    public function wall() {
        return $this->morphOne(Wall::class, 'walls', 'type', 'object_id', 'id');
    }

    public function files() {
        return $this->morphMany(File::class, 'files', 'object', 'object_id', 'id');
    }

    public function image() {
        return $this->morphOne(File::class, NULL, 'object', 'object_id', 'image_id');
    }

    public function reports() {
        return $this->hasMany(Report::class, 'post_id', 'id');
    }


    public function reportUsers() {
        return $this->belongsToMany(User::class, 'reports', 'post_id', 'user_id')->withTimestamps();
    }

    public function getImageAttribute($value) {
        return Image::getFullURL($value);
    }

    public function getVideoAttribute($value) {
        return Image::getFullURL($value);
    }

    public function getUserAttribute($value) {
        return $this->user()->first();
    }

    public function fetchDimension() {
        $image = $this->files()->select([
            'id',
            'link AS url',
            'width',
            'height'
        ])->first();
        if ($this->image_id && $image) {
            $this->attributes['image_object'] = $image;
        }
    }

    public function toNewsFeed($userId, $parentId, $action, $actionId) {
        $liked = $this->likes()->where('likes.user_id', $userId)->first();
        $shared = $this->shares()->where('user_id', $userId)->first();
        // check if post owner is login-ed user

        $currentUser = User::find($userId);
        if ($currentUser) {
            $followed = $currentUser->followings()
                ->where('object_id', $this->user_id)
                ->first();
        } else {
            $followed = FALSE;
        }

        $campaign = $this->campaign()->first();
        if ($campaign) {
            $user = $campaign->user()->first();
        } else {
            $user = $this->user()->first();
        }

        $this->fetchDimension();
        $result = [
            'parent_id' => $parentId,
            'action' => $action,
            'action_id' => $actionId,
            'shared' => $shared ? TRUE : FALSE,
            'liked' => $liked ? TRUE : FALSE,
            'followed' => $followed ? TRUE : FALSE,
            'views_count' => $this->views_count ? $this->views_count: 0,
            'shares_count' => $this->shares_count ? $this->shares_count : 0,
            'likes_count' => $this->likes_count ? $this->likes_count : 0,
            'comments_count' => $this->comments_count ? $this->comments_count : 0,
            'campaign_id' => $campaign ? $campaign->id : 0,
            'campaign_name' => $campaign ? $campaign->name : '',
            'campaign_image' => $campaign ? $campaign->image : '',
            'campaign_short_blurb' => $campaign ? $campaign->short_blurb : '',
            'campaign_funding_kind' => $campaign ? $campaign->funding_kind : '',
            'campaign_days_remaining' => $campaign ? $campaign->days_remaining: 0,
            'campaign_funding_current' => $campaign ? $campaign->funding_current : 0,
            'campaign_percent_funded' => $campaign ? $campaign->percent_funded : 0,
            'campaign_category_name' => $campaign ? $campaign->category()->first()->name : '',
            'campaign_date' => $campaign ? $campaign->created_at->toDateTimeString() : '',
            'user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'avatar' => $user->image,
            'organization' => $user->organization,
            'position' => $user->position,
            'post_message' => $this->message,
            'post_id' => $this->id,
            'post_title' => $this->title,
            'post_description' => $this->description,
            'post_image' => $this->image,
            'post_video' => $this->video,
            'post_duration' => $this->duration
        ];

        if (isset($this->attributes['image_object'])) {
            $result['image_object'] = $this->attributes['image_object'];
        }

        return new NewsfeedMessage($result);
    }
}