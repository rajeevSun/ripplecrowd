<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Activity;

class Follow extends Model
{
    use SoftDeletes;

    protected $table = 'follows';

    protected $fillable = ['user_id', 'object', 'object_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['deleted_at'];

    public $timestamps = TRUE;

    public function followable() {
        return $this->morphTo('follow', 'object', 'object_id');
    }

    public function compilation() {
        return $this->belongsTo(Compilation::class, 'compilation_id', 'id');
    }

    public function activity() {
        return $this->morphOne(Activity::class, 'activities', 'action', 'action_id', 'id');
    }

    public function activityUsers() {
        return $this->morphToMany(User::class, NULL, 'activities', 'action', 'action_id', 'user_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
