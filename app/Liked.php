<?php
namespace App;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 21-Dec-16
 * Time: 10:37
 */
trait Liked {
    public function checkLiked($userId) {
        $liked = $this->likes()->where('likes.user_id', $userId)->first();
        if ($liked) {
            return $this->setLikedAttribute(TRUE);
        } else {
            return $this->setLikedAttribute(FALSE);
        }
    }

    public function getLikedAttribute($value) {
        return $value;
    }

    public function setLikedAttribute($value) {
        $this->attributes['liked'] = $value;
    }
}