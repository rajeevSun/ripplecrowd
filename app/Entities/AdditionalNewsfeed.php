<?php
namespace App\Entities;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 11-Nov-16
 * Time: 10:22
 */

class AdditionalNewsfeed
{
    public $user_id = 0;
    public $first_name = '';
    public $last_name = '';
    public $followed = FALSE;
    public $avatar = '';
    public $organization = '';
    public $position = '';
    public $group_id = 0;
    public $group_name = '';
    public $group_image = '';
    public $group_members_count = 0;
    public $joined = FALSE;
    public $campaign_id = 0;
    public $campaign_name = '';
    public $campaign_image = '';
    public $campaign_short_blurb = '';
    public $campaign_funding_kind = '';
    public $campaign_days_remaining = 0;
    public $campaign_funding_current = 0;
    public $campaign_percent_funded = 0;
    public $campaign_category_name = '';
    public $campaign_date = '';
    public $composite_key = NULL;

    public function __construct($object) {
        foreach ($object as $attribute => $value) {
            $this->{$attribute} = $value;
        }
        $this->encodeCompositeKey();
    }

    private function encodeCompositeKey() {
        $keys = [$this->user_id, $this->group_id, $this->campaign_id];
        $this->composite_key = md5(implode('-', $keys));
    }
}