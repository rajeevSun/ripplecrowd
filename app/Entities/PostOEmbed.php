<?php
/**
 * Created by PhpStorm.
 * User: phonghdt
 * Date: 10-Jan-17
 * Time: 15:03
 */

namespace App\Entities;


class PostOEmbed
{
    public $title;
    public $description;
    public $url;
    public $thumbnail;

    public function __construct(array $data) {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }
}