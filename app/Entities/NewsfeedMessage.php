<?php
namespace App\Entities;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 11-Nov-16
 * Time: 10:22
 */

class NewsfeedMessage
{
    public $parent_id = 0;
    public $action = '';
    public $action_id = 0;
    public $shared = FALSE;
    public $liked = FALSE;
    public $followed = FALSE;
    public $views_count = 0;
    public $shares_count = 0;
    public $likes_count = 0;
    public $comments_count = 0;
    public $campaign_id = 0;
    public $campaign_name = '';
    public $campaign_image = '';
    public $campaign_short_blurb = '';
    public $campaign_funding_kind = '';
    public $campaign_days_remaining = 0;
    public $campaign_funding_current = 0;
    public $campaign_percent_funded = 0;
    public $campaign_category_name = '';
    public $campaign_date = '';
    public $is_campaign_owner = FALSE;
    public $user_id = 0;
    public $first_name = '';
    public $last_name = '';
    public $avatar = '';
    public $organization = '';
    public $position = '';
    public $group_id = 0;
    public $group_name = '';
    public $joined = FALSE;
    public $share_id = 0;
    public $share_message = '';
    public $milestone_id = 0;
    public $milestone_title = '';
    public $milestone_description = '';
    public $milestone_image = '';
    public $milestone_date = '';
    public $post_id = 0;
    public $post_title = '';
    public $post_message = '';
    public $post_description = '';
    public $post_image = '';
    public $post_video = '';
    public $post_duration = 0;
    public $view_more = TRUE;
    public $additionals = [];

    public function __construct($object) {
        foreach ($object as $attribute => $value) {
            $this->{$attribute} = $value;
        }
    }
}