<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OAuthClient extends Model
{
    protected $table = 'oauth_clients';

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;
}
