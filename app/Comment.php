<?php

namespace App;

use App\Helpers\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Like;

class Comment extends Model
{
    use SoftDeletes;

    protected $table = 'comments';

    protected $fillable = ['user_id', 'message', 'object','object_id', 'likes_count', 'comments_count'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['deleted_at', 'comments_count'];

    public $timestamps = TRUE;

    public function likes() {
        return $this->morphMany(Like::class, 'likes', 'object', 'object_id', 'id');
    }
    public function likedUsers() {
        return $this->morphToMany(User::class, NULL, 'likes', 'object', 'object_id', 'user_id');
    }
    public function comments() {
        return $this->morphMany(Comment::class, 'comments', 'object', 'object_id', 'id');
    }
    public function commentUsers() {
        return $this->morphToMany(User::class, NULL, 'comments', 'object', 'object_id', 'user_id');
    }
    public function commentable() {
        return $this->morphTo('comment', 'object', 'object_id');
    }
    public function activity() {
        return $this->morphOne(Activity::class, 'activities', 'action', 'action_id', 'id');
    }
    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function setFirstNameAttribute($value) {
        $this->attributes['first_name'] = $value;
    }
    public function setLastNameAttribute($value) {
        $this->attributes['last_name'] = $value;
    }
    public function setAvatarAttribute($value) {
        $this->attributes['avatar'] = $value;
    }
    public function getAvatarAttribute($value) {
        if (!$value) {
            return Image::getFullURL(config('setting.default_avatar'));
        }
        return Image::getFullURL($value);
    }

    public function getPositionAttribute($value) {
        if (!$value) {
            return config('setting.default_position');
        }
        return $value;
    }

    public function getOrganizationAttribute($value) {
        if (!$value) {
            return config('setting.default_organization');
        }
        return $value;
    }
}