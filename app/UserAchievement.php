<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAchievement extends Model
{
    use SoftDeletes;

    protected $table = 'user_achievements';

    protected $fillable = ['user_id','achievement_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = TRUE;
}
