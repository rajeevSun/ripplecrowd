<?php
/**
 * Created by PhpStorm.
 * User: phonghdt
 * Date: 04-Jan-17
 * Time: 16:58
 */

namespace App\Channels;
use App\User;
use Illuminate\Notifications\Notification;
use RuntimeException;

class FcmChannel
{
    protected $url;
    protected $apiKey;

    public function __construct(array $configs) {
        foreach ($configs as $key => $value) {
            $this->{camel_case($key)} = $value;
        }
    }

    public function send($notifiable, Notification $notification) {
        return $this->sendToFcm($notifiable, $notification);
    }

    /**
     * Send message to Fcm server
     *
     * @param $notifiable
     * @param Notification $notification
     * @return mixed
     */
    protected function sendToFcm($notifiable, Notification $notification) {

        if (method_exists($notification, 'toFcm')) {
            $message = $notification->toFcm($notifiable);
        } else {
            throw new RuntimeException(
                'Notification is missing toFcm method.'
            );
        }


        if (empty($message)) {
            return;
        }

        // api request header
        $headers = [
            'Content-Type: application/json',
            'Authorization:key=' . $this->apiKey
        ];

        // init api request
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($message));

        // process request api
        $result = curl_exec($ch);

        if (!$result) {
            throw new RuntimeException(
                "FCM send error:" . curl_error($ch)
            );
        }

        // close connection
        curl_close($ch);
        return $result;
    }
}