<?php
/**
 * Created by PhpStorm.
 * User: phonghdt
 * Date: 15-Feb-17
 * Time: 14:00
 */

namespace App\Channels;

use Carbon\Carbon;
use Illuminate\Notifications\Channels\DatabaseChannel as DBChanel;
use RuntimeException;
use Illuminate\Notifications\Notification;
use Ramsey\Uuid\Uuid;
use App\Notification as NotificationModel;

class DatabaseChannel extends DBChanel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function send($notifiable, Notification $notification)
    {
        $data = $this->getData($notifiable, $notification);
        $result = [];
        if (isset($data['users_id']) && !empty($data['users_id'])) {
            foreach ($data['users_id'] as $userId) {
                $result[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'type' => get_class($notification),
                    'data' => '',
                    'notifiable_type' => 'user', //TODO: set this dynamic to app provider config
                    'notifiable_id' => $userId,
                    'object_type' => $data['object_type'],
                    'object_id' => $data['object_id'],
                    'notification_type' => $data['notification_type'],
                    'target_id' => $data['target_id'],
                    'target_name' => $data['target_name'],
                    'target_object' => $data['target_object'],
                    'target_activity_id' => $data['target_activity_id'],
                    'user_id' => $data['user_id'],
                    'read_at' => null,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
        }

        if (!empty($result)) {
            NotificationModel::insert($result);
        }
    }

    /**
     * Get the data for the notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array
     *
     * @throws \RuntimeException
     */
    protected function getData($notifiable, Notification $notification)
    {
        if (method_exists($notification, 'toDatabase')) {
            $data = $notification->toDatabase($notifiable);

            return is_array($data) ? $data : [];
        } elseif (method_exists($notification, 'toArray')) {
            return $notification->toArray($notifiable);
        }

        throw new RuntimeException(
            'Notification is missing toDatabase / toArray method.'
        );
    }
}