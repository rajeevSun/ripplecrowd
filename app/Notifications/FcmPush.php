<?php

namespace App\Notifications;

use App\Channels\FcmChannel;
use App\Channels\DatabaseChannel;
use App\Activity;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;

class FcmPush extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $activity;

    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FcmChannel::class, DatabaseChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toFcm($notifiable) {
        $notificationInfo = $this->activity->getNotificationInfo();
        if (!$notificationInfo) {
            return;
        }
        // get deviceTokenIds
        $notificationMap = $this->activity->getNotificationMap();
        $notifiable = User::find($notificationInfo['notificationOwnerId']);
        if ($notifiable && in_array($notificationInfo['notificationKey'], $notificationMap['followers'])) {
            $deviceTokenIds = $notifiable->followers()->whereNotNull('device_token')->where('device_token', '<>', '')->pluck('device_token');
        } elseif ($notifiable && in_array($notificationInfo['notificationKey'], $notificationMap['yourself']) && $notifiable->device_token) {
            $deviceTokenIds = [$notifiable->device_token];
        } else {
            return;
        }
        return [
            'registration_ids' => $deviceTokenIds,
            'notification' => [
                'body_loc_key' => $notificationInfo['notificationKey'],
                'body_loc_args' => $notificationInfo['notificationArgs']
            ],
            'data' => [
                'target_id' => $notificationInfo['notificationRelatedObjectId'],
                'target_activity_id' => $this->activity->id,
                'avatar' => $notificationInfo['notificationAvatar']
            ]
        ];
    }

    public function toDatabase($notifiable) {
        $notificationInfo = $this->activity->getNotificationInfo();
        if (!$notificationInfo) {
            return;
        }
        // get deviceTokenIds
        $notificationMap = $this->activity->getNotificationMap();
        $notifiable = User::find($notificationInfo['notificationOwnerId']);
        if ($notifiable && in_array($notificationInfo['notificationKey'], $notificationMap['followers'])) {
            $userIds = $notifiable->followers()->pluck('users.id');
        } elseif ($notifiable && in_array($notificationInfo['notificationKey'], $notificationMap['yourself']) && $notifiable->device_token) {
            $userIds = [$notifiable->id];
        } else {
            return;
        }
        return [
            'users_id' => $userIds,
            'target_id' => $notificationInfo['notificationTargetId'],
            'target_name' => $notificationInfo['notificationTargetName'],
            'target_object' => $notificationInfo['notificationTargetObject'],
            'target_activity_id' => $notificationInfo['notificationTargetActivityId'],
            'object_type' => $notificationInfo['notificationObjectType'],
            'object_id' => $notificationInfo['notificationObjectId'],
            'notification_type' => $notificationInfo['notificationKey'],
            'user_id' => $notificationInfo['notificationUserId']
        ];
    }
}
