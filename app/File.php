<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Image;

class File extends Model
{
    use SoftDeletes;

    protected $table = 'files';

    protected $fillable = ['object', 'object_id', 'path','file_name', 'link', 'width', 'height'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['deleted_at'];

    public $timestamps = TRUE;

    public function fileable() {
        return $this->morphTo('file', 'object', 'object_id');
    }

    public function image() {
        return $this->morphOne('image', 'object', 'object_id');
    }

    public function getUrlAttribute($value) {
        return Image::getFullURL($value);
    }
}
