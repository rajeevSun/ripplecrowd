<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wall extends Model
{
    protected $table = 'walls';

    protected $fillable = ['type','object_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;

    public function wallable() {
        return $this->morphTo('wall', 'type', 'object_id');
    }
}
