<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suggestion extends Model
{
    use SoftDeletes;

    protected $table = 'suggestions';

    protected $fillable = ['user_id', 'object', 'object_id', 'compilation_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;

    public function suggestive() {
        return $this->morphTo('suggest', 'object', 'object_id');
    }
}
