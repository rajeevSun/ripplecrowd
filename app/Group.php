<?php

namespace App;

use App\Category;
use App\User;
use App\Wall;
use App\File;
use App\NewsFeedInterface;
use Illuminate\Database\Eloquent\Model;
use Image;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\NewsfeedMessage;
use App\Entities\AdditionalNewsfeed;

class Group extends Model implements NewsFeedInterface
{

    use SoftDeletes;

    protected $table = 'groups';

    protected $fillable = ['name', 'wall_id'];

    protected $hidden = ['pivot'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;
    // the owner of group.
    public function owner() {
        return $this->belongsTo(User::class, 'id', 'owner_id');
    }
    // the category of group.
    public function category() {
        return $this->belongsTo(Category::class, 'id', 'category_id');
    }
    // members of group
    public function members() {
        return $this->belongsToMany(User::class, 'group_members', 'group_id', 'user_id')->wherePivot('role', 'member');
    }
    // founder of group
    public function founder() {
        return $this->belongsToMany(User::class, 'group_members', 'group_id', 'user_id')->wherePivot('role', 'founder');
    }
    // all members
    public function allMembers() {
        return $this->belongsToMany(User::class, 'group_members', 'group_id', 'user_id');
    }

    // accessor for image field.
    public function getImageAttribute($value) {
        return Image::getFullURL($value);
    }

    public function wall() {
        return $this->morphOne(Wall::class, 'walls', 'type', 'object_id', 'id');
    }

    public function files() {
        return $this->morphMany(File::class, 'files', 'object', 'object_id', 'id');
    }

    public function image() {
        return $this->morphOne(File::class, NULL, 'object', 'object_id', 'image_id');
    }

    public function toNewsFeed($userId, $parentId, $action, $actionId) {
        $user = $this->allMembers()->where('group_members.user_id', $userId)->first();
        return new NewsfeedMessage([
            'parent_id' => $parentId,
            'action' => $action,
            'action_id' => $actionId,
            'shared' => FALSE,
            'liked' => FALSE,
            'followed' => FALSE,
            'views_count' => 0,
            'shares_count' => 0,
            'likes_count' => 0,
            'comments_count' => 0,
            'group_id' => $this->id,
            'group_name' => $this->name,
            'members_count' => $this->members_count,
            'joined' => $user ? TRUE : FALSE
        ]);
    }

    public function toAdditionalNewsFeed($userId) {
        $user = $this->allMembers()->where('group_members.user_id', $userId)->first();
        return new AdditionalNewsfeed([
            'group_id' => $this->id,
            'group_name' => $this->name,
            'group_image' => $this->image,
            'group_members_count' => $this->members_count,
            'joined' => $user ? TRUE : FALSE
        ]);
    }

    public function checkJoined($userId) {
        $user = $this->allMembers()->where('group_members.user_id', $userId)->first();
        if ($user) {
            $this->attributes['joined'] = TRUE;
            return $this->attributes['joined'];
        } else {
            $this->attributes['joined'] = FALSE;
            return $this->attributes['joined'];
        }
    }
}
