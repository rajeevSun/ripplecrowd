<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiling extends Model
{
    protected $table = 'profilings';

    protected $fillable = ['title','description', 'type', 'user_id', 'start_date', 'end_date'];

    protected $hidden = ['deleted_at'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = TRUE;
}
