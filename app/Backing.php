<?php

namespace App;

use App\Helpers\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Campaign;

class Backing extends Model
{
    use SoftDeletes;

    protected $table = 'backings';

    protected $fillable = ['user_id', 'fund_amount', 'campaigns_id', 'bank_name', 'bank_account', 'received_user_id'];

    protected $hidden = ['deleted_at'];

    protected $casts = [
        'fund_amount' => 'double',
        'bank_name' => 'string',
        'bank_account' => 'string'
    ];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = TRUE;

    public function campaign() {
        return $this->belongsTo(Campaign::class, 'campaign_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getBankAccountAttribute($value) {
        if ($value) {
            return $value;
        }
        return '';
    }

    public function getBankNameAttribute($value) {
        if ($value) {
            return $value;
        }
        return '';
    }

    public function getAvatarAttribute($value) {
        if (!$value) {
            return Image::getFullURL(config('setting.default_avatar'));
        }
        return Image::getFullURL($value);
    }

    public function getPositionAttribute($value) {
        if (!$value) {
            return config('setting.default_position');
        }
        return $value;
    }

    public function getOrganizationAttribute($value) {
        if (!$value) {
            return config('setting.default_organization');
        }
        return $value;
    }
}