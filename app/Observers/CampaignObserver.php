<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Elastic;
use App\Post;
use App\Activity;
use App\Campaign;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;

class CampaignObserver {

    protected $elastic;

    public function __construct(Elastic $elastic) {
        $this->elastic = $elastic;
    }

    public function creating(Campaign $campaign) {
        $campaign->meta_keyword = '';
        $campaign->meta_description = '';
    }

    // this action run after post was created
    public function created(Campaign $campaign) {
        // create new activity
        $activity = new Activity();
        $activity->user_id = $campaign->user_id;
        $activity->type = 'create_campaign';
        $activity->action = 'campaign';
        $activity->action_id = $campaign->id;
        $activity->save();
        // maybe create new nofitication too


        // update to elastic search
        $this->elastic->index([
            'index' => 'ripple',
            'type' => 'campaigns',
            'id' => $campaign->id,
            'body' => [
                'id' => $campaign->id,
                'image' => $campaign->image,
                'name' => $campaign->name,
                'type' => 'campaign'
            ]
        ]);
    }

    public function updated(Campaign $campaign) {
        /*$activity = Activity::where('action', 'campaign')
            ->where('action_id', $campaign->id)
            ->first();
        if ($activity) {
            $activity->save();
        }*/


        // update to elastic search
        $this->elastic->index([
            'index' => 'ripple',
            'type' => 'campaigns',
            'id' => $campaign->id,
            'body' => [
                'id' => $campaign->id,
                'image' => $campaign->image,
                'name' => $campaign->name,
                'type' => 'campaign'
            ]
        ]);
    }

    public function deleted(Campaign $campaign) {
        // delete related activities
        $activities = $campaign->activity()->get();
        foreach ($activities as $activity) {
            $activity->delete();
        }

        // delete related comments
        $comments = $campaign->comments()->get();
        foreach ($comments as $comment) {
            $comment->delete();
        }

        // delete related shares
        $shares = $campaign->shares()->get();
        foreach ($shares as $share) {
            $share->delete();
        }

        // delete related likes
        $likes = $campaign->likes()->get();
        foreach ($likes as $like) {
            $like->delete();
        }

        // delete related milestones
        $milestones = $campaign->milestones()->get();
        foreach ($milestones as $milestone) {
            $milestone->delete();
        }

        // update to elastic search
        $this->elastic->delete([
            'index' => 'ripple',
            'type' => 'campaigns',
            'id' => $campaign->id
        ]);
    }
}