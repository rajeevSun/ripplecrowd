<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Follow;
use App\Activity;
use App\User;
use App\Compilation;
use App\Campaign;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;

class FollowObserver {
    // this action run after post was created
    public function created(Follow $follow) {
        // create new activity
        $object = $follow->followable()->first();

        if ($object) {
            $activity = new Activity();
            $activity->user_id = $follow->user_id;
            $activity->type = 'follow_'.$follow->object;
            $activity->action = 'follow';
            $activity->action_id = $follow->id;
            $activity->save();

            // if follow user, make compilation for that
            if (get_class($object) == User::class) {
                // check if there is a follow compilation today
                $followCompilation = Compilation::where('user_id', $follow->user_id)
                    ->where('object', 'follow')
                    ->whereDate('created_at', $follow->created_at->toDateString())
                    ->first();
                // update follow with compilation
                if ($followCompilation) {
                    $follow->compilation_id = $followCompilation->id;
                    $follow->save();
                } else {
                    // create new compilation
                    $followCompilation = new Compilation();
                    $followCompilation->user_id = $follow->user_id;
                    $followCompilation->object = 'follow';
                    if ($followCompilation->save()) {
                        // update follow with new compilation
                        $follow->compilation_id = $followCompilation->id;
                        $follow->save();
                    }
                }

                // increase followers_count
                User::where('id', $follow->object_id)->increment('followers_count');

            }
        }
    }

    public function deleted(Follow $follow) {
        // delete related activities
        $activities = $follow->activity()->get();
        foreach ($activities as $activity) {
            $activity->delete();
        }

        $object = $follow->followable()->first();
        $compilation = $follow->compilation()->first();
        // if follow user, make compilation for that
        if (get_class($object) == User::class && $compilation) {
            $followCount = Follow::where('object', 'user')
                ->where('user_id', $follow->user_id)
                ->where('compilation_id', $compilation->id)
                ->count();
            if (!$followCount) {
                $compilation->delete();
            }
        }

        if (get_class($object) == User::class) {
            // decrease followers_count
            User::where('id', $follow->object_id)->where('followers_count', '<>', 0)->decrement('followers_count');
        }
    }
}