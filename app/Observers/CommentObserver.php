<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Share;
use App\Like;
use App\Activity;
use App\User;
use App\Campaign;
use App\Post;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;
use App\Comment;

class CommentObserver {
    // this action run after like was created
    public function created(Comment $comment) {

        // create new activity for comment action
        if ($comment->commentable()->first()) {
            // create new activity
            $activity = new Activity();
            $activity->user_id = $comment->user_id;
            $activity->type = 'comment_'.$comment->object;
            $activity->action = 'comment';
            $activity->action_id = $comment->id;
            $activity->save();
        }

        // handle likes count
        $commentableObject = $comment->commentable()->first();
        $commentableObject->increment('comments_count');
    }
    // this action run after like was deleted
    public function deleted(Comment $comment) {

        // delete activities related to comment
        $activities = $comment->activity()->get();
        foreach ($activities as $activity) {
            $activity->delete();
        }

        // delete related likes
        $likes = $comment->likes()->get();
        foreach ($likes as $like) {
            $like->delete();
        }

        // handle likes count
        $commentableObject = $comment->commentable()->first();
        if ($commentableObject) {
            $commentableObject->decrement('comments_count');
        }
    }
}