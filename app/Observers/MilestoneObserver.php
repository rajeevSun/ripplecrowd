<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Share;
use App\Like;
use App\Activity;
use App\User;
use App\Campaign;
use App\Post;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;
use App\Comment;
use App\Milestone;

class MilestoneObserver {
    // this action run after like was created
    public function created(Milestone $milestone) {
        // create new activity
        $activity = new Activity();
        $activity->user_id = $milestone->user_id;
        $activity->type = 'create_milestone';
        $activity->action = 'milestone';
        $activity->action_id = $milestone->id;
        $activity->save();
    }
    // this action run after like was deleted
    public function deleted(Milestone $milestone) {

        // delete activity relate to comment action
        $activities = $milestone->activity()->get();
        foreach ($activities as $activity) {
            $activity->delete();
        }

        // delete related shares
        $shares = $milestone->shares()->get();
        foreach ($shares as $share) {
            $share->delete();
        }

        // delete related comments
        $comments = $milestone->comments()->get();
        foreach ($comments as $comment) {
            $comment->delete();
        }

        // delete related likes
        $likes = $milestone->likes()->get();
        foreach ($likes as $like) {
            $like->delete();
        }
    }
}