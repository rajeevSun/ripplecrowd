<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Share;
use App\Like;
use App\Activity;
use App\User;
use App\Campaign;
use App\Post;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;

class LikeObserver {
    // this action run after like was created
    public function created(Like $like) {

        // create new activity for like action
        if ($like->likable()->first()) {
            // create new activity
            $activity = new Activity();
            $activity->user_id = $like->user_id;
            $activity->type = 'like_'.$like->object;
            $activity->action = 'like';
            $activity->action_id = $like->id;
            $activity->save();
        }

        // handle likes count
        $likableObject = $like->likable()->first();
        $likableObject->increment('likes_count');
    }
    // this action run after like was deleted
    public function deleted(Like $like) {
        // delete related activities
        $activities = $like->activity()->get();
        foreach ($activities as $activity) {
            $activity->delete();
        }

        // handle likes count
        $likableObject = $like->likable()->first();
        if ($likableObject) {
            $likableObject->where('likes_count', '>', 0)->decrement('likes_count');
        }
    }
}