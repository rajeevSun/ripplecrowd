<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\User;
use App\Wall;
use App\Elastic;

class UserObserver {

    protected $elastic;

    public function __construct(Elastic $elastic)
    {
        $this->elastic = $elastic;
    }

    // this action run after post was created
    public function created(User $user) {
        // check if use had wall
        $wall = Wall::where('type', 'user')
            ->where('object_id', $user->id)
            ->first();
        if (!$wall) {
            $wall = new Wall();
            $wall->type = 'user';
            $wall->object_id = $user->id;
            $wall->save();
            // update wall_id for user
            $user->wall_id = $wall->id;
            $user->save();
        }

        // update to elastic search
        $this->elastic->index([
            'index' => 'ripple',
            'type' => 'users',
            'id' => $user->id,
            'body' => [
                'id' => $user->id,
                'image' => $user->image,
                'name' => $user->first_name . ' ' . $user->last_name,
                'type' => 'user'
            ]
        ]);

        try {
            $user->createOnFirebase();
        } catch (\Exception $e) {
            // log here
        }
    }

    public function updated(User $user) {
        $this->elastic->index([
            'index' => 'ripple',
            'type' => 'users',
            'id' => $user->id,
            'body' => [
                'id' => $user->id,
                'image' => $user->image,
                'name' => $user->first_name . ' ' . $user->last_name,
                'type' => 'user'
            ]
        ]);

        try {
            $user->updateOnFirebase();
        } catch (\Exception $e) {
            // log here
        }
    }

    public function deleted(User $user) {
        $this->elastic->delete([
            'index' => 'ripple',
            'type' => 'users',
            'id' => $user->id
        ]);
    }
}