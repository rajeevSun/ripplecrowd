<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Post;
use App\Activity;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;
use App\Parsers\Video as VideoParser;
use App\oEmbeds\Youtube;
use App\oEmbeds\Vimeo;
use Parser;
use Input;
use MediaServices;

class PostObserver {

    public function creating(Post $post) {
        $message = $post->message;
        // need refactor parser
        if ($post->type == 'text') {
            $parserObject = Parser::parse($post->message);
            // parse image
            if ($parserObject && $parserObject->getProviderName() == 'Image') {
                $post->title = '';
                $post->description = '';
                $post->image = $parserObject->getUri();
                $post->video = $post->video ? $post->video : '';
                $post->type = 'image_external';
            }
            // parse youtube/vimeo
            if ($parserObject && ($parserObject->getProviderName() == 'Youtube' || $parserObject->getProviderName() == 'Vimeo') && $parserObject->getMetadata()) {
                $metadata = $parserObject->getMetadata();
                $post->title = $metadata->title;
                $post->description = $metadata->description;
                $post->image = $metadata->thumbnail;
                $post->video = $metadata->url;
                $post->type = 'video_external';
            }
            // parse website link
            if ($parserObject && $parserObject->getProviderName() == 'Link' && $parserObject->getMetadata()) {
                $metadata = $parserObject->getMetadata();
                $post->title = $metadata->title;
                $post->description = $metadata->description;
                $post->image = $metadata->thumbnail;
                $post->video = $metadata->url;
                $post->type = 'website_external';
            }

            if (Input::file('video')) {
                try {
                    $mediaAsset = MediaServices::getFileStream('video')
                        ->uploadFileAndCreateAsset()
                        ->encodeToAdaptiveBitrateMP4Set()
                        ->encodeToThumbnails()
                        ->publishThumbnailAsset()
                        ->publishEncodedAsset();
                    $post->video = $mediaAsset->getVideoUrl();
                    $post->image = $mediaAsset->getThumbnailUrl();
                    $post->type = 'video_internal';
                } catch (\Exception $e) {

                }
            }
        }
    }

    // this action run after post was created
    public function created(Post $post) {
        // create new activity
        $activity = new Activity();
        $activity->user_id = $post->user_id;
        $activity->type = 'create_post_'.$post->type;
        $activity->action = 'post';
        $activity->action_id = $post->id;
        $activity->wall_id = $post->wall_id;
        $activity->save();
        // maybe create new nofitication too
    }

    /*public function updated(Post $post) {
        $activity = Activity::where('action', 'post')
            ->where('action_id', $post->id)
            ->where('user_id', $post->user_id)
            ->where('wall_id', $post->wall_id)
            ->first();
        if ($activity) {
            $activity->save();
        }
    }*/

    public function updating(Post $post) {
        $parserObject = Parser::parse($post->message);
        // parse image
        if ($parserObject && $parserObject->getProviderName() == 'Image') {
            $post->title = '';
            $post->description = '';
            $post->image = $parserObject->getUri();
            $post->video = $post->video ? $post->video : '';
            $post->type = 'image_external';
        }
        // parse youtube/vimeo
        elseif ($parserObject && ($parserObject->getProviderName() == 'Youtube' || $parserObject->getProviderName() == 'Vimeo') && $parserObject->getMetadata()) {
            $metadata = $parserObject->getMetadata();
            $post->title = $metadata->title;
            $post->description = $metadata->description;
            $post->image = $metadata->thumbnail;
            $post->video = $metadata->url;
            $post->type = 'video_external';
        }
        // parse website link
        elseif ($parserObject && $parserObject->getProviderName() == 'Link' && $parserObject->getMetadata()) {
            $metadata = $parserObject->getMetadata();
            $post->title = $metadata->title;
            $post->description = $metadata->description;
            $post->image = $metadata->thumbnail;
            $post->video = $metadata->url;
            $post->type = 'website_external';
        }

        elseif (Input::file('video')) {
            try {
                $mediaAsset = MediaServices::getFileStream('video')
                    ->uploadFileAndCreateAsset()
                    ->encodeToAdaptiveBitrateMP4Set()
                    ->encodeToThumbnails()
                    ->publishThumbnailAsset()
                    ->publishEncodedAsset();
                $post->video = $mediaAsset->getVideoUrl();
                $post->image = $mediaAsset->getThumbnailUrl();
                $post->type = 'video_internal';
            } catch (\Exception $e) {

            }
        }

        $activity = $post->activity()->first();
        if ($activity) {
            $activity->type = 'create_post_'.$post->type;
            $activity->save();
        }
    }

    public function deleted(Post $post) {

        // delete related activities
        $activities = $post->activity()->get();
        foreach ($activities as $activity) {
            $activity->delete();
        }

        // delete all related shares of post
        $shares = $post->shares()->get();
        foreach ($shares as $share) {
            $share->delete(); // delete all shares.
        }

        // delete related comments
        $comments = $post->comments()->get();
        foreach ($comments as $comment) {
            $comment->delete();
        }

        // delete related likes
        $likes = $post->likes()->get();
        foreach ($likes as $like) {
            $like->delete();
        }
    }
}