<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Follow;
use App\Activity;
use App\Compilation;

class CompilationObserver {
    // this action run after post was created
    public function created(Compilation $compilation) {
        // create new activity
        $type = 'follow_user_compilation';
        if ($compilation->object == 'follow') {
            $type = 'follow_user_compilation';
        } elseif ($compilation->object == 'join') {
            $type='join_group_compilation';
        }

        $activity = new Activity();
        $activity->user_id = $compilation->user_id;
        $activity->type = $type;
        $activity->action = 'compilation';
        $activity->action_id = $compilation->id;
        $activity->save();
    }

    public function deleted(Compilation $compilation) {
        $compilation->activity()->delete();
    }
}