<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Share;
use App\Activity;
use App\User;
use App\Campaign;
use App\Post;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;

class ShareObserver {
    // this action run after share was created
    public function created(Share $share) {

        // create new activity for share action
        if ($share->sharable()->first()) {
            // create new activity
            $activity = new Activity();
            $activity->user_id = $share->user_id;

            if ($share->object == 'post') {
                //$postActivity = Post::find($share->object_id)->activity()->first();
                //$type = str_replace('create', 'share', $postActivity->type);
                $type = 'share_post_'.Post::find($share->object_id)->type;
            } else {
                $type = 'share_'.$share->object;
            }
            $activity->type = $type;
            $activity->action = 'share';
            $activity->action_id = $share->id;
            $activity->save();
        }

        // handle shares count
        $sharableObject = $share->sharable()->first();
        $sharableObject->increment('shares_count');
    }

    public function deleted(Share $share) {

        // delete relate activity
        $activities = $share->activity()->get();
        foreach ($activities as $activity) {
            $activity->delete();
        }

        // delete related comments
        $comments = $share->comments()->get();
        foreach ($comments as $comment) {
            $comment->delete();
        }

        // delete related likes
        $likes = $share->likes()->get();
        foreach ($likes as $like) {
            $like->delete();
        }

        //handle shares count
        $sharableObject = $share->sharable()->first();
        if ($sharableObject) {
            $sharableObject->decrement('shares_count');
        }
    }
}