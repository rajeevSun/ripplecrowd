<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Follow;
use App\Activity;
use App\Group;
use App\User;
use App\Compilation;
use App\GroupMember;
use App\Campaign;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;

class GroupMemberObserver {
    // this action run after post was created
    public function created(GroupMember $groupMember) {
        // create new activity
        $group = $groupMember->group()->first();

        if ($group) {
            $activity = new Activity();
            $activity->user_id = $groupMember->user_id;
            $activity->type = 'join_group';
            $activity->action = 'join';
            $activity->action_id = $groupMember->id;
            $activity->save();

            // if join group, make compilation for that
            if ($group) {
                // check if there is a follow compilation today
                $joinGroupCompilation = Compilation::where('user_id', $groupMember->user_id)
                    ->where('object', 'join')
                    ->whereDate('created_at', $groupMember->created_at->toDateString())
                    ->first();
                // update follow with compilation
                if ($joinGroupCompilation) {
                    $groupMember->compilation_id = $joinGroupCompilation->id;
                    $groupMember->save();
                } else {
                    // create new compilation
                    $joinGroupCompilation = new Compilation();
                    $joinGroupCompilation->user_id = $groupMember->user_id;
                    $joinGroupCompilation->object = 'join';
                    if ($joinGroupCompilation->save()) {
                        // update follow with new compilation
                        $groupMember->compilation_id = $joinGroupCompilation->id;
                        $groupMember->save();
                    }
                }

                // increase followers_count
                Group::where('id', $group->id)->increment('members_count');

            }
        }
    }

    public function deleted(GroupMember $groupMember) {
        // delete related activities
        $activities = $groupMember->activity()->get();
        foreach ($activities as $activity) {
            $activity->delete();
        }

        $group = $groupMember->group()->first();
        $compilation = $groupMember->compilation()->first();
        // if follow user, make compilation for that
        if ($group && $compilation) {
            $groupMemberCount = GroupMember::where('object', 'user')
                ->where('user_id', $groupMember->user_id)
                ->where('compilation_id', $compilation->id)
                ->count();
            if (!$groupMemberCount) {
                $compilation->delete();
            }
        }

        if ($group) {
            // decrease followers_count
            Group::where('id', $group->id)->decrement('members_count');
        }
    }
}