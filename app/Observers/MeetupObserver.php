<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Elastic;
use App\Group;
use App\Meetup;
use App\Wall;
use App\GroupMember;
use Carbon\Carbon;

class MeetupObserver {

    protected $elastic;

    public function __construct(Elastic $elastic) {
        $this->elastic = $elastic;
    }

    // this action run after post was created
    public function created(Meetup $meetup) {
        // update to elastic search
        $this->elastic->index([
            'index' => 'ripple',
            'type' => 'meetups',
            'id' => $meetup->id,
            'body' => [
                'id' => $meetup->id,
                'image' => $meetup->image,
                'name' => $meetup->title,
                'type' => 'meetup'
            ]
        ]);
    }

    public function updated(Meetup $meetup) {
        // update the elastic
        $this->elastic->index([
            'index' => 'ripple',
            'type' => 'meetups',
            'id' => $meetup->id,
            'body' => [
                'id' => $meetup->id,
                'image' => $meetup->image,
                'name' => $meetup->title,
                'type' => 'meetup'
            ]
        ]);
    }

    public function deleted(Meetup $meetup) {
        $this->elastic->delete([
            'index' => 'ripple',
            'type' => 'meetups',
            'id' => $meetup->id
        ]);
    }
}