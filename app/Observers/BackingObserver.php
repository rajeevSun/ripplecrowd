<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Backing;
use App\User;

class BackingObserver {
    // this action run after backing was created
    public function created(Backing $backing) {
        $campaign = $backing->campaign()->first();
        if ($campaign) {
            $campaign->increment('backings_count');
            $campaign->increment('funding_current', doubleval($backing->fund_amount));

            // update owner
            $owner = $campaign->user()->first();
            if ($owner) {
                $owner->increment('raised_count');
                $owner->increment('raised_amount', doubleval($backing->fund_amount));
            }
        }

        $funder = $backing->user()->first();
        if ($funder) {
            $funder->increment('invested_count');
            $funder->increment('invested_amount', doubleval($backing->fund_amount));
        }
    }

    // this action run after backing was deleted
    public function deleted(Backing $backing) {
        $campaign = $backing->campaign()->first();
        if ($campaign) {
            $campaign->decrement('backings_count');
            $campaign->decrement('funding_current', doubleval($backing->fund_amount));

            // update owner
            $owner = $campaign->user()->first();
            if ($owner) {
                $owner->decrement('raised_count');
                $owner->decrement('raised_amount', doubleval($backing->fund_amount));
            }
        }

        $funder = $backing->user()->first();
        if ($funder) {
            $funder->decrement('invested_count');
            $funder->decrement('invested_amount', doubleval($backing->fund_amount));
        }
    }
}