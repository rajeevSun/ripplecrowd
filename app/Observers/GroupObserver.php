<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Elastic;
use App\Group;
use App\Wall;
use App\GroupMember;
use Carbon\Carbon;

class GroupObserver {

    protected $elastic;

    public function __construct(Elastic $elastic) {
        $this->elastic = $elastic;
    }

    // this action run after post was created
    public function created(Group $group) {
        // create new wall for group
        $wall = new Wall();
        $wall->type = 'group';
        $wall->object_id = $group->id;
        $wall->save();
        // update wall_id for group.
        $group->wall_id = $wall->id;
        $group->save();

        // create founder for group
        GroupMember::create([
            'user_id' => $group->owner_id,
            'group_id' => $group->id,
            'role' => 'founder'
        ]);

        // update to elastic search
        $this->elastic->index([
            'index' => 'ripple',
            'type' => 'groups',
            'id' => $group->id,
            'body' => [
                'id' => $group->id,
                'image' => $group->image,
                'name' => $group->name,
                'type' => 'group'
            ]
        ]);
    }

    public function updated(Group $group) {
        // update to elastic search
        $this->elastic->index([
            'index' => 'ripple',
            'type' => 'groups',
            'id' => $group->id,
            'body' => [
                'id' => $group->id,
                'image' => $group->image,
                'name' => $group->name,
                'type' => 'group'
            ]
        ]);
    }

    public function deleted(Group $group) {
        $this->elastic->delete([
            'index' => 'ripple',
            'type' => 'groups',
            'id' => $group->id
        ]);
    }
}