<?php
namespace App\Observers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 02-Nov-16
 * Time: 08:54
 */
use App\Share;
use App\Like;
use App\Activity;
use App\User;
use App\Campaign;
use App\Post;
use Carbon\Carbon;
use App\Entities\NewsfeedMessage;
use App\Notifications\FcmPush;
use App\Notification;

class ActivityObserver {
    // this action run after like was deleted
    public function deleted(Activity $activity) {
        Notification::where('target_activity_id', $activity->id)->delete();
    }

    public function created(Activity $activity) {
        $user = User::find($activity->user_id);
        if ($user) {
            $user->notify(new FcmPush($activity));
        }
    }
}