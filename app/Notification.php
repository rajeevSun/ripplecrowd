<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\DatabaseNotification;
use Image;

class Notification extends DatabaseNotification
{
    use SoftDeletes;

    public $timestamps = TRUE;

    // accessor for avatar
    public function getAvatarAttribute($value) {
        if (!$value) {
            return Image::getFullURL(config('setting.default_avatar'));
        }
        return Image::getFullURL($value);
    }

    public function getPositionAttribute($value) {
        if (!$value) {
            return config('setting.default_position');
        }
        return $value;
    }

    public function getOrganizationAttribute($value) {
        if (!$value) {
            return config('setting.default_organization');
        }
        return $value;
    }
}
