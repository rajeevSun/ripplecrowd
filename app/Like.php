<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Activity;

class Like extends Model
{
    use SoftDeletes;

    protected $table = 'likes';

    protected $fillable = ['user_id', 'object','object_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['deleted_at'];

    public $timestamps = TRUE;

    public function likable() {
        return $this->morphTo('like', 'object', 'object_id');
    }

    public function activity() {
        return $this->morphOne(Activity::class, 'activities', 'action', 'action_id', 'id');
    }

    public function activityUsers() {
        return $this->morphToMany(User::class, NULL, 'activities', 'action', 'action_id', 'user_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
