<?php

namespace App;

use App\Helpers\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Activity;
use App\Comment;
use App\Like;
use App\Entities\NewsfeedMessage;
use App\NewsFeedInterface;

class Share extends Model implements NewsFeedInterface
{
    use SoftDeletes;

    protected $table = 'shares';

    protected $fillable = ['user_id', 'object','object_id', 'wall_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['deleted_at'];

    protected $touches = ['activity'];

    public $timestamps = TRUE;

    public function sharable() {
        return $this->morphTo('share', 'object', 'object_id');
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'comments', 'object', 'object_id', 'id');
    }

    public function activity() {
        return $this->morphMany(Activity::class, 'activities', 'action', 'action_id', 'id');
    }

    public function activityUsers() {
        return $this->morphToMany(User::class, NULL, 'activities', 'action', 'action_id', 'user_id');
    }

    public function likes() {
        return $this->morphMany(Like::class, 'likes', 'object', 'object_id', 'id');
    }

    public function getImageAttribute($value) {
        return Image::getFullURL($value);
    }

    public function getMessageAttribute($value) {
        return ($value === NULL) ? '' : $value;
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function toNewsFeed($userId, $parentId, $action, $actionId) {
        $liked = $this->likes()->where('likes.user_id', $userId)->first();
        $object = $this->sharable()->first();
        if ($object) {
            $newsFeedMessage = $object->toNewsFeed($userId, $parentId, $action, $actionId);
            $newsFeedMessage->likes_count = $this->likes_count;
            $newsFeedMessage->comments_count = $this->comments_count;
            $newsFeedMessage->liked = $liked ? TRUE : FALSE;
            $newsFeedMessage->share_id = $this->id;
            $newsFeedMessage->share_message = $this->message;
            return $newsFeedMessage;
        }
        return new NewsfeedMessage([]);
    }
}
