<?php
namespace App\Parsers;

/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 16-Jan-17
 * Time: 10:17
 */
interface ParserInterface {
    /**
     * @param $text string
     * @return mixed
     */
    public function parse($text);
}