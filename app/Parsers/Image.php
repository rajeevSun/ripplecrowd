<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 10-Jan-17
 * Time: 15:53
 */

namespace App\Parsers;
use App\Parsers\ParserTrait;

class Image implements ParserInterface
{
    use ParserTrait;

    public function parse($text) {
        preg_match('/\bhttp(s?):\/\/(?:www.|m.)?(\S*)\b/', $text, $match);
        if (isset($match[0])) {
            //$headers = get_headers($match[0], 1);
            $ch = curl_init($match[0]);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_exec($ch);
            $result = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
            if ($result && strpos($result, 'image/') !== false) {
                $this->uri = $match[0];
                $this->providerName = 'Image';
                return $this;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
}