<?php
namespace App\Parsers;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 16-Jan-17
 * Time: 10:51
 */
trait ParserTrait {
    public $providerName;
    public $uri;

    public function setProviderName($value) {
        $this->providerName = $value;
    }

    public function setUri($value) {
        $this->uri = $value;
    }

    public function getProviderName() {
        return $this->providerName;
    }

    public function getUri() {
        return $this->uri;
    }
}