<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 10-Jan-17
 * Time: 15:53
 */

namespace App\Parsers;
use App\Parsers\ParserTrait;
use Dusterio\LinkPreview\Client as Preview;
use App\oEmbeds\Link;

class LinkPreview implements ParserInterface
{
    use ParserTrait;

    public $metadata;

    public function parse($text) {
        preg_match('/\bhttp(s?):\/\/(?:www.|m.)?(\S*)\b/', $text, $match);
        if (isset($match[0])) {
            $this->setProviderName('Link');
            $this->setUri($match[0]);
            $this->setMetadata();
            return $this;
        } else {
            return FALSE;
        }
    }

    public function setMetadata() {
        $this->metadata = (new Link($this->getUri()))->getPostOEmbed();
    }

    public function getMetadata() {
        return $this->metadata;
    }
}