<?php
namespace App\Parsers;
use Illuminate\Support\Manager;
use App\Parsers\Video;
use App\Parsers\Image;
use App\Parsers\LinkPreview;

/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 16-Jan-17
 * Time: 10:24
 */
class ParserManager extends Manager implements ParserInterface {

    protected $defaultDriver = 'video';

    protected $parsers = ['image', 'video', 'link_preview'];

    public function getDefaultDriver()
    {
        return $this->defaultDriver;
    }

    public function parse($text) {
        foreach ($this->parsers as $parser) {
            $result = $this->driver($parser)->parse($text);
            if ($result) {
                return $result;
            }
        }
        return FALSE;
    }

    protected function createVideoDriver() {
        return $this->app->make(Video::class);
    }

    protected function createImageDriver() {
        return $this->app->make(Image::class);
    }

    protected function createLinkPreviewDriver() {
        return $this->app->make(LinkPreview::class);
    }

    /*
     * Create a new driver instance.
     *
     * @param  string  $driver
     * @return mixed
     *
     * @throws \InvalidArgumentException
     */
    protected function createDriver($driver)
    {
        try {
            return parent::createDriver($driver);
        } catch (InvalidArgumentException $e) {
            if (class_exists($driver)) {
                return $this->app->make($driver);
            }

            throw $e;
        }
    }
}