<?php
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 10-Jan-17
 * Time: 15:53
 */

namespace App\Parsers;
use App\Parsers\ParserTrait;
use App\oEmbeds\Youtube;
use App\oEmbeds\Vimeo;

class Video implements ParserInterface
{
    use ParserTrait;

    public $metadata;

    public function parse($text) {
        preg_match('/\bhttp(s?):\/\/(?:www.|m.)?(vimeo|youtube).com\/((watch\?v=|v\/)([a-zA-Z0-9\-\_]{11})|([0-9]{9}))\b/', $text, $match);
        if (isset($match[0])) {
            if (isset($match[2]) && ($match[2] == 'vimeo' || $match[2] == 'youtube')) {
                $this->setProviderName(ucfirst($match[2]));
                $this->setUri($match[0]);
                $this->setMetadata();
                return $this;
            }
            return FALSE;
        } else {
            return FALSE;
        }
    }

    public function setMetadata() {
        $providerClass = 'App\\oEmbeds\\' . $this->getProviderName();
        if ($providerClass) {
            $this->metadata = (new $providerClass($this->getUri()))->getPostOEmbed();
        } else {
            $this->metadata = FALSE;
        }
    }

    public function getMetadata() {
        return $this->metadata;
    }
}