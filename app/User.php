<?php

namespace App;

use App\Category;
use App\Followed;
use App\Group;
use App\Campaign;
use App\Helpers\Image;
use App\OAuthAccessToken;
use App\Achievement;
use App\Profiling;
use App\Backing;
use App\Follow;
use App\File;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\NewsfeedMessage;
use App\Entities\AdditionalNewsfeed;
use App\NewsFeedInterface;
use Laravel\Passport\Token;
use App\Tokens\Token as FirebaseToken;


class User extends Authenticatable implements NewsFeedInterface
{
    use HasApiTokens, Followed, SoftDeletes, Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pivot', 'deleted_at', 'device_token'
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public $timestamps = TRUE;
    // get campaigns that current user is following.
    public function followedCampaigns() {
        return $this->belongsToMany(Campaign::class, 'follows', 'user_id', 'object_id')->wherePivot('object', 'campaign')->wherePivot('deleted_at', NULL);
    }
    // get users who this user is following.
    public function usersFollowing() {
        return $this->belongsToMany(self::class, 'follows', 'user_id', 'object_id')->wherePivot('object', 'user')->wherePivot('deleted_at', NULL);
    }
    // get users who following this user.
    public function followers() {
        return $this->belongsToMany(self::class, 'follows', 'object_id', 'user_id')->wherePivot('object', 'user');
    }
    // get categories that current user interest.
    public function categories() {
        return $this->belongsToMany(Category::class, 'interests', 'user_id', 'category_id');
    }
    // get groups that current user joined.
    public function groups() {
        return $this->belongsToMany(Group::class, 'group_members', 'user_id', 'group_id');
    }
    // get owner groups of current user.
    public function ownerGroups() {
        return $this->hasMany(Group::class, 'owner_id', 'id');
    }
    // get tokens of this user.
    public function tokens() {
        return $this->hasMany(Token::class, 'user_id', 'id');
    }
    // get recommend campaigns.
    public function ownCampaigns() {
        return $this->hasMany(Campaign::class, 'user_id', 'id');
    }
    // get achivements
    public function achievements() {
        return $this->belongsToMany(Achievement::class, 'user_achievements', 'user_id', 'achievement_id');
    }
    // profilings
    public function profilings() {
        return $this->hasMany(Profiling::class, 'user_id', 'id');
    }
    // get raised backings
    public function raisedBackings() {
        return $this->hasMany(Backing::class, 'user_id', 'id');
    }

    // get invested backing
    public function investedBackings() {
        return $this->hasMany(Backing::class, 'received_user_id', 'id');
    }

    public function likedPosts() {
        return $this->morphedByMany(Post::class, NULL, 'likes', 'object', 'user_id', 'object_id');
    }

    public function follows() {
        return $this->morphMany(Follow::class, 'follows', 'object', 'object_id', 'id');
    }

    public function followings() {
        return $this->morphMany(Follow::class, 'follows', 'object', 'user_id', 'id');
    }

    public function followUsers() {
        return $this->morphToMany(User::class, NULL, 'follows', 'object', 'object_id', 'user_id');
    }

    public function suggestCompilations() {
        return $this->morphToMany(Compilation::class, 'suggestCompilation', 'suggestions', 'object', 'object_id', 'compilation_id')
            ->wherePivot('deleted_at', NULL);
    }

    public function followedUsers() {
        return $this->morphedByMany(User::class, NULL, 'follows', 'object', 'user_id', 'object_id')->wherePivot('deleted_at', NULL);
    }

    public function followingCampaigns() {
        return $this->morphedByMany(Campaign::class, NULL, 'follows', 'object', 'user_id', 'object_id')->wherePivot('deleted_at', NULL);
    }

    public function files() {
        return $this->morphMany(File::class, 'files', 'object', 'object_id', 'id');
    }

    public function image() {
        return $this->morphOne(File::class, NULL, 'object', 'object_id', 'image_id');
    }

    public function reports() {
        return $this->hasMany(Report::class, 'user_id', 'id');
    }

    public function reportPosts() {
        return $this->belongsToMany(Post::class, 'reports', 'post_id', 'user_id')->withTimestamps();
    }

    // project image field accessor.
    public function getImageAttribute($value) {
        return Image::getFullURL($value);
    }

    // project image field accessor.
    public function getVideoAttribute($value) {
        return Image::getFullURL($value);
    }

    // accessor for avatar
    public function getAvatarAttribute($value) {
        if (!$value) {
            return Image::getFullURL(config('setting.default_avatar'));
        }
        return Image::getFullURL($value);
    }

    public function getPositionAttribute($value) {
        if (!$value) {
            return config('setting.default_position');
        }
        return $value;
    }

    public function getOrganizationAttribute($value) {
        if (!$value) {
            return config('setting.default_organization');
        }
        return $value;
    }

    public function getFollowersCountAttribute($value) {
        if ($value) {
            return $value;
        }
        return 0;
    }

    public function getFirebaseTokenAttribute($value) {
        return FirebaseToken::createCustomToken($this->id);
    }

    public function toNewsFeed($userId, $parentId, $action, $actionId) {
        $user = User::find($userId)->followedUsers()->where('follows.object_id', $this->id)->first();
        return new NewsfeedMessage([
            'parent_id' => $parentId,
            'action' => $action,
            'action_id' => $actionId,
            'shared' => FALSE,
            'liked' => FALSE,
            'followed' => $user ? TRUE : FALSE,
            'user_id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'avatar' => $this->image,
            'organization' => $this->organization,
            'position' => $this->position,
        ]);
    }

    public function toAdditionalNewsFeed($userId) {
        $user = User::find($userId)->followedUsers()->where('follows.object_id', $this->id)->first();
        return new AdditionalNewsfeed([
            'user_id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'followed' => $user ? TRUE : FALSE,
            'avatar' => $this->image,
            'organization' => $this->organization,
            'position' => $this->position
        ]);
    }

    public function updateOnFirebase() {
        $dirtyFields = $this->getDirty();

        if (isset($dirtyFields['image']) || isset($dirtyFields['first_name']) || isset($dirtyFields['last_name'])) {
            // api request header
            $headers = [
                'Content-Type: application/json'
            ];

            // init api request
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, config('firebase-database.database_uri').'users/'.$this->id.'.json?auth='.config('firebase-database.secret'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
                'firstName' => $this->first_name,
                'lastName' => $this->last_name,
                'avatar' => $this->image
            ]));

            // process request api
            $result = curl_exec($ch);

            if (!$result) {
                throw new RuntimeException(
                    "Firebase sync error:" . curl_error($ch)
                );
            }

            // close connection
            curl_close($ch);
            return $result;
        }
    }

    public function createOnFirebase() {
        // api request header
        $headers = [
            'Content-Type: application/json'
        ];

        // init api request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('firebase-database.database_uri').'users/'.$this->id.'.json?auth='.config('firebase-database.secret'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'avatar' => $this->image
        ]));

        // process request api
        $result = curl_exec($ch);

        if (!$result) {
            throw new RuntimeException(
                "Firebase sync error:" . curl_error($ch)
            );
        }

        // close connection
        curl_close($ch);
        return $result;
    }

    public static function removeAllFromFirebase() {
        // api request header
        $headers = [
            'Content-Type: application/json'
        ];

        // init api request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('firebase-database.database_uri').'users.json?auth='.config('firebase-database.secret'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        // process request api
        $result = curl_exec($ch);

        if (!$result) {
            throw new RuntimeException(
                "Firebase sync error:" . curl_error($ch)
            );
        }

        // close connection
        curl_close($ch);
        return $result;
    }

    public static function syncAllToFirebase() {

        // get all users
        $firebaseUsers = [];
        $users = self::all();
        foreach ($users as $user) {
            $firebaseUsers[$user->id] = [
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
                'avatar' => $user->image
            ];
        }



        // api request header
        $headers = [
            'Content-Type: application/json'
        ];

        // init api request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('firebase-database.database_uri').'users.json?auth='.config('firebase-database.secret'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($firebaseUsers));

        // process request api
        $result = curl_exec($ch);

        if (!$result) {
            throw new RuntimeException(
                "Firebase sync error:" . curl_error($ch)
            );
        }

        // close connection
        curl_close($ch);
        return count($firebaseUsers);
    }
}