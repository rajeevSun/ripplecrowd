<?php
namespace App;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 12-Dec-16
 * Time: 10:51
 */
interface NewsFeedInterface {
    public function toNewsFeed($userId, $parentId, $action, $actionId);
}