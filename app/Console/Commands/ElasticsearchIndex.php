<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Elastic;
use App\User;
use App\Campaign;
use App\Group;
use App\Meetup;

class ElasticsearchIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:es-index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex Elasticsearch';


    /**
     * @var Elastic
     */
    protected $elastic;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Elastic $elastic)
    {
        $this->elastic = $elastic;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deleteRippleParams = [
            'index' => 'ripple'
        ];
        try {
            $this->elastic->indices()->delete($deleteRippleParams);
        } catch (\Exception $e) {

        }

        $count = 0;
        $users = User::all();
        foreach ($users as $user) {
            // update to elastic search
            $this->elastic->index([
                'index' => 'ripple',
                'type' => 'users',
                'id' => $user->id,
                'body' => [
                    'id' => $user->id,
                    'image' => $user->image,
                    'name' => $user->first_name . ' ' . $user->last_name,
                    'type' => 'user'
                ]
            ]);
            $count++;
        }

        $campaigns = Campaign::all();
        foreach ($campaigns as $campaign) {
            // update to elastic search
            $this->elastic->index([
                'index' => 'ripple',
                'type' => 'campaigns',
                'id' => $campaign->id,
                'body' => [
                    'id' => $campaign->id,
                    'image' => $campaign->image,
                    'name' => $campaign->name,
                    'type' => 'campaign'
                ]
            ]);
            $count++;
        }

        $groups = Group::all();
        foreach ($groups as $group) {
            // update to elastic search
            $this->elastic->index([
                'index' => 'ripple',
                'type' => 'groups',
                'id' => $group->id,
                'body' => [
                    'id' => $group->id,
                    'image' => $group->image,
                    'name' => $group->name,
                    'type' => 'group'
                ]
            ]);
            $count++;
        }

        $meetups = Meetup::all();
        foreach ($meetups as $meetup) {
            // update to elastic search
            $this->elastic->index([
                'index' => 'ripple',
                'type' => 'meetups',
                'id' => $meetup->id,
                'body' => [
                    'id' => $meetup->id,
                    'image' => $meetup->image,
                    'name' => $meetup->title,
                    'type' => 'meetup'
                ]
            ]);
            $count++;
        }
        $this->comment(
            "Elastic index total {$count} record(s)."
        );
    }
}
