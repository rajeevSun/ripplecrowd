<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class FirebaseSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:firebase-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Firebase synchronize';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            User::removeAllFromFirebase();
            $this->comment(
                "Success remove all from Firebase"
            );
        } catch (\Exception $e) {
            $this->comment(
                "An error occurs when remove all from Firebase"
            );
        }

        try {
            User::syncAllToFirebase();
            $this->comment(
                "Success sync to Firebase"
            );
        } catch (\Exception $e) {
            $this->comment(
                "An error occurs when sync to Firebase"
            );
        }

    }
}
