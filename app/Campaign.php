<?php

namespace App;

use App\Owner;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Image;
use App\NewsFeedInterface;
use App\User;
use App\Like;
use App\Share;
use App\Comment;
use App\File;
use App\Followed;
use App\Shared;
use App\Liked;
use App\Milestone;
use App\Entities\NewsfeedMessage;
use App\Entities\AdditionalNewsfeed;

class Campaign extends Model implements NewsFeedInterface
{
    use SoftDeletes, Followed, Liked, Shared, Owner;

    protected $table = 'campaigns';

    // fillable
    protected $fillable = ['name', 'description', 'short_blurb', 'category_id', 'logo', 'video', 'image', 'funding_duration', 'funding_goal', 'funding_strategy', 'funding_kind', 'debt_duration', 'interest_rate_amount', 'interest_payment_period', 'location', 'funding_current', 'backed_count', 'currency_id', 'campaign_verified', 'repayment_amount', 'revenue_share', 'user_id', 'ending_at'];

    // date mutators
    protected $dates = ['deleted_at', 'created_at', 'updated_at', 'ending_at'];

    public $timestamps = TRUE;

    protected $hidden = ['deleted_at', 'verified_at'];

    // project image field accessor.
    public function getImageAttribute($value) {
        return Image::getFullURL($value);
    }

    public function setOwnerFollowedAttribute($value) {
        $this->attributes['owner_followed'] = $value;
    }

    protected $casts = [
        'funding_current' => 'double',
        'funding_goal' => 'double',
        'interest_rate_amount' => 'double',
        'funding_duration' => 'integer'
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function likes() {

        return $this->morphMany(Like::class, 'likes', 'object', 'object_id', 'id');
    }

    public function likedUsers() {
        return $this->morphToMany(User::class, NULL, 'likes', 'object', 'object_id', 'user_id');
    }

    public function shares() {
        return $this->morphMany(Share::class, 'shares', 'object', 'object_id', 'id');
    }

    public function sharedUsers() {
        return $this->morphToMany(User::class, NULL, 'shares', 'object', 'object_id', 'user_id');
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'comments', 'object', 'object_id', 'id');
    }

    public function commentUsers() {
        return $this->morphToMany(User::class, NULL, 'comments', 'object', 'object_id', 'user_id');
    }

    public function activity() {
        return $this->morphOne(Activity::class, 'activities', 'action', 'action_id', 'id');
    }

    public function suggestCompilations() {
        return $this->morphToMany(Compilation::class, 'suggestCompilation', 'suggestions', 'object', 'object_id', 'compilation_id')
            ->wherePivot('deleted_at', NULL);
    }

    public function follows() {
        return $this->morphMany(Follow::class, 'follows', 'object', 'object_id', 'id');
    }

    public function followUsers() {
        return $this->morphToMany(User::class, NULL, 'follows', 'object', 'object_id', 'user_id');
    }

    public function files() {
        return $this->morphMany(File::class, 'files', 'object', 'object_id', 'id');
    }

    public function image() {
        return $this->morphOne(File::class, NULL, 'object', 'object_id', 'image_id');
    }

    public function milestones() {
        return $this->hasMany(Milestone::class, 'campaign_id', 'id');
    }

    public function getDaysRemainingAttribute($value) {
        $today = Carbon::now();
        $remainingSeconds = $today->diffInSeconds($this->ending_at, FALSE);
        if ($remainingSeconds > 0) {
            return $remainingSeconds;
        }
        return 0;
    }

    public function getCategoryNameAttribute($value) {
        if ($value) {
            return $value;
        }
        try {
            return $this->category()->first()->name;
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getPercentFundedAttribute($value) {
        return round(($this->funding_current/$this->funding_goal)*100);
    }

    public function getAvatarAttribute($value) {
        if (!$value) {
            return Image::getFullURL(config('setting.default_avatar'));
        }
        return Image::getFullURL($value);
    }

    public function getPositionAttribute($value) {
        if (!$value) {
            return config('setting.default_position');
        }
        return $value;
    }

    public function getOrganizationAttribute($value) {
        if (!$value) {
            return config('setting.default_organization');
        }
        return $value;
    }

    public function getLogoAttribute($value) {
        return Image::getFullURL($value);
    }

    public function getUserAttribute($value) {
        return $this->user()->first();
    }

    public function toNewsFeed($userId, $parentId, $action, $actionId) {
        $user = $this->user()->first();
        $liked = $this->likes()->where('likes.user_id', $userId)->first();
        $shared = $this->shares()->where('shares.user_id', $userId)->first();
        $followed = $this->follows()->where('follows.user_id', $userId)->first();
        $this->checkIsOwner($userId);
        return new NewsfeedMessage([
            'parent_id' => $parentId,
            'action' => $action,
            'action_id' => $actionId,
            'shared' => $shared ? TRUE : FALSE,
            'liked' => $liked ? TRUE : FALSE,
            'followed' => $followed ? TRUE : FALSE,
            'views_count' => 0,
            'shares_count' => $this->shares_count,
            'likes_count' => $this->likes_count,
            'comments_count' => $this->comments_count,
            'campaign_id' => $this->id,
            'campaign_name' => $this->name,
            'campaign_image' => $this->image,
            'campaign_short_blurb' => $this->short_blurb,
            'campaign_funding_kind' => $this->funding_kind,
            'campaign_days_remaining' => $this->days_remaining,
            'campaign_funding_current' => $this->funding_current,
            'campaign_percent_funded' => $this->percent_funded,
            'campaign_category_name' => $this->category()->first()->name,
            'campaign_date' => $this->created_at->toDateTimeString(),
            'is_campaign_owner' => $this->is_owner,
            'user_id' => $this->user_id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'avatar' => $user->image,
            'organization' => $user->organization,
            'position' => $user->position
        ]);
    }

    public function toAdditionalNewsFeed($userId) {
        $user = $this->user()->first();
        $followed = $this->follows()->where('follows.user_id', $userId)->first();
        $category = $this->category()->first();
        return new AdditionalNewsfeed([
            'user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'followed' => $followed ? TRUE : FALSE,
            'avatar' => $user->image,
            'organization' => $user->organization,
            'position' => $user->position,
            'campaign_id' => $this->id,
            'campaign_name' => $this->name,
            'campaign_image' => $this->image,
            'campaign_short_blurb' => $this->short_blurb,
            'campaign_funding_kind' => $this->funding_kind,
            'campaign_days_remaining' => $this->days_remaining,
            'campaign_funding_current' => $this->funding_current,
            'campaign_percent_funded' => $this->percent_funded,
            'campaign_category_name' => $category->name,
            'campaign_date' => get_class($this->created_at) == Carbon::class ? $this->created_at->toDateTimeString() : $this->created_at
        ]);
    }
}