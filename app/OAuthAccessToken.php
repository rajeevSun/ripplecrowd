<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\User;

class OAuthAccessToken extends Model
{
    protected $table = 'oauth_access_tokens';

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'expires_at'];

    public $timestamps = TRUE;
    // get user belong to this token
    public function user() {
        $current = Carbon::now();
        return $this->belongsTo(User::class, 'id', 'user_id')->where('oauth_access_tokens.expires_at', '>=', $current);
    }
}
