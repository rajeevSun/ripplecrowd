<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\NewsfeedMessage;
use App\Entities\AdditionalNewsfeed;
use App\NewsFeedInterface;
use App\Follow;
use App\User;
use App\GroupMember;
use App\Group;

class Compilation extends Model implements NewsFeedInterface
{

    use SoftDeletes;

    protected $table = 'compilations';

    protected $fillable = ['user_id', 'object'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;

    public function follows() {
        return $this->hasMany(Follow::class, 'compilation_id', 'id');
    }

    public function suggestions() {
        return $this->hasMany(Suggestion::class, 'compilation_id', 'id');
    }

    public function followUsers() {
        return $this->belongsToMany(User::class, 'follows', 'compilation_id', 'object_id')
            ->wherePivot('object', 'user')
            ->wherePivot('deleted_at', NULL);
    }

    public function recommendUsers() {
        return $this->belongsToMany(User::class, 'recommendations', 'compilation_id', 'object_id')->wherePivot('object', 'user');
    }

    public function recommendCampaign() {
        return $this->belongsToMany(Campaign::class, 'recommendations', 'compilation_id', 'object_id')->wherePivot('object', 'campaign');
    }

    public function suggestUsers() {
        return $this->morphedByMany(User::class, 'suggestCompilation', 'suggestions', 'object', 'compilation_id', 'object_id')
            ->wherePivot('deleted_at', NULL);
    }

    public function suggestCampaigns() {
        return $this->morphedByMany(Campaign::class, 'suggestCompilation', 'suggestions', 'object', 'compilation_id', 'object_id')
            ->wherePivot('deleted_at', NULL);
    }

    public function groupMembers() {
        return $this->hasMany(GroupMember::class, 'compilation_id', 'id');
    }

    public function groups() {
        return $this->belongsToMany(Group::class, 'group_members', 'compilation_id', 'group_id');
    }

    public function activity() {
        return $this->morphOne(Activity::class, 'activities', 'action', 'action_id', 'id');
    }

    public function toNewsFeed($userId, $parentId, $action, $actionId) {
        $additionals = [];
        $recommendCount = 0;
        if ($this->object == 'follow') {
            $users = $this->followUsers()->where('follows.object_id', '<>', $userId)->get();
            foreach ($users as $user) {
                $additionals[] = $user->toAdditionalNewsFeed($userId);
            }
        }
        if ($this->object == 'join') {
            $groups = $this->groups()->get();
            foreach ($groups as $group) {
                $additionals[] = $group->toAdditionalNewsFeed($userId);
            }
        }
        if ($this->object == 'suggest') {
            //$suggestions = $this->suggestions()->get();
            $users = $this->suggestUsers()->get();

            $currentUser = User::find($userId);
            $followings = $currentUser->usersFollowing()->pluck('follows.object_id');
            $categories = $currentUser->categories()->pluck('categories.id');
            $recommendUsersCount = User::whereNotIn('users.id', $followings)
                ->join('interests', 'interests.user_id', '=', 'users.id')
                ->whereIn('interests.category_id', $categories)
                ->where('users.id', '<>', $userId)
                ->count();

            $followedCampaignIds = $currentUser->followedCampaigns()->pluck('campaigns.id');
            $recommendCampaignsCount = Campaign::join('interests', 'interests.category_id', '=', 'campaigns.category_id')
                ->where('interests.user_id', '=', $currentUser->id)
                ->where('campaigns.user_id', '<>', $currentUser->id)
                ->where('campaigns.funding_goal', '!=', 0)
                ->where('campaigns.campaign_verified', '=', 2)
                ->whereNotIn('campaigns.id', $followedCampaignIds)
                ->distinct()
                ->count();

            $campaigns = $this->suggestCampaigns()->get();
            $objects = $users->merge($campaigns);
            foreach ($objects as $object) {
                $object->checkFollowed($userId);
                if (!$object->followed) {
                    if (get_class($object) == Campaign::class) {
                        $recommendCount = $recommendCampaignsCount;
                    } elseif (get_class($object) == User::class) {
                        $recommendCount = $recommendUsersCount;
                    }
                    $additionals[] = $object->toAdditionalNewsFeed($userId);
                }
            }
        }
        return new NewsfeedMessage([
            'parent_id' => $parentId,
            'action' => $action,
            'action_id' => $actionId,
            'shared' => FALSE,
            'liked' => FALSE,
            'followed' => FALSE,
            'views_count' => 0,
            'shares_count' => 0,
            'likes_count' => 0,
            'comments_count' => 0,
            'view_more' =>  ($recommendCount>count($additionals)) ? TRUE : FALSE,
            'additionals' => $additionals
        ]);
    }
}
