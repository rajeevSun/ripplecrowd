<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';

    protected $fillable = ['user_id', 'post_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;

    public function user() {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function post() {
        return $this->belongsTo(Post::class, 'id', 'post_id');
    }
}
