<?php
namespace App;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 21-Dec-16
 * Time: 10:37
 */
trait Shared {
    public function checkShared($userId) {
        $liked = $this->shares()->where('shares.user_id', $userId)->first();
        if ($liked) {
            return $this->setSharedAttribute(TRUE);
        } else {
            return $this->setSharedAttribute(FALSE);
        }
    }

    public function getSharedAttribute($value) {
        return $value;
    }

    public function setSharedAttribute($value) {
        $this->attributes['shared'] = $value;
    }
}