<?php

namespace App;

use App\Liked;
use App\Owner;
use App\Shared;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Image;
use App\File;
use App\Like;
use App\User;
use App\Campaign;
use App\Share;
use App\Comment;
use App\Entities\NewsfeedMessage;
use App\NewsFeedInterface;

class Milestone extends Model implements NewsFeedInterface
{
    use SoftDeletes, Liked, Shared, Owner;

    protected $table = 'milestones';

    protected $fillable = ['user_id', 'campaign_id','title', 'description', 'image', 'video'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $attributes = [
        'likes_count' => 0,
        'comments_count' => 0,
        'shares_count' => 0
    ];

    protected $hidden = ['deleted_at'];

    public $timestamps = TRUE;

    public function getImageAttribute($value) {
        return Image::getFullURL($value);
    }

    public function getAvatarAttribute($value) {
        if (!$value) {
            return Image::getFullURL(config('setting.default_avatar'));
        }
        return Image::getFullURL($value);
    }

    public function getPositionAttribute($value) {
        if (!$value) {
            return config('setting.default_position');
        }
        return $value;
    }

    public function getOrganizationAttribute($value) {
        if (!$value) {
            return config('setting.default_organization');
        }
        return $value;
    }

    public function getCampaignImageAttribute($value) {
        return Image::getFullURL($value);
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function campaign() {
        return $this->belongsTo(Campaign::class, 'campaign_id', 'id');
    }

    public function files() {
        return $this->morphMany(File::class, 'files', 'object', 'object_id', 'id');
    }

    public function image() {
        return $this->morphOne(File::class, 'image', 'object', 'object_id', 'image_id');
    }

    public function likes() {
        return $this->morphMany(Like::class, 'likes', 'object', 'object_id', 'id');
    }

    public function likedUsers() {
        return $this->morphToMany(User::class, NULL, 'likes', 'object', 'object_id', 'user_id')->wherePivot('deleted_at', NULL);
    }

    public function shares() {
        return $this->morphMany(Share::class, 'shares', 'object', 'object_id', 'id');
    }

    public function sharedUsers() {
        return $this->morphToMany(User::class, NULL, 'shares', 'object', 'object_id', 'user_id');
    }

    public function activity() {
        return $this->morphOne(Activity::class, 'activities', 'action', 'action_id', 'id');
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'comments', 'object', 'object_id', 'id');
    }

    public function fetchDimension() {
        $image = $this->files()->select([
            'id',
            'link AS url',
            'width',
            'height'
        ])->first();

        if ($this->image_id && $image) {
            $this->attributes['image_object'] = $image;
        }
    }

    public function toNewsFeed($userId, $parentId, $action, $actionId) {
        $user = $this->user()->first();
        // check if post owner is login-ed user
        if ($userId == $this->user_id) {
            $followed = TRUE;
        } else {
            $currentUser = User::find($userId);
            if ($currentUser) {
                $followed = $currentUser->followings()
                    ->where('object_id', $this->user_id)
                    ->first();
            } else {
                $followed = FALSE;
            }
        }
        $likedUser = $this->likedUsers()->where('likes.user_id', $userId)->first();
        $sharedUser = $this->sharedUsers()->where('shares.user_id', $userId)->first();
        $campaign = $this->campaign()->first();
        $campaign->checkIsOwner($userId);
        $imageObject = $this->fetchDimension();
        $result = [
            'parent_id' => $parentId,
            'action' => $action,
            'action_id' => $actionId,
            'shared' => $sharedUser ? TRUE : FALSE,
            'liked' => $likedUser ? TRUE : FALSE,
            'followed' => $followed ? TRUE : FALSE,
            'views_count' => 0,
            'shares_count' => $this->shares_count,
            'likes_count' => $this->likes_count,
            'comments_count' => $this->comments_count,
            'campaign_id' => $campaign->id,
            'campaign_name' => $campaign->name,
            'campaign_image' => $campaign->image,
            'campaign_short_blurb' => $campaign->short_blurb,
            'campaign_funding_kind' => $campaign->funding_kind,
            'campaign_days_remaining' => $campaign->days_remaining,
            'campaign_funding_current' => $campaign->funding_current,
            'campaign_percent_funded' => $campaign->percent_funded,
            'campaign_category_name' => $campaign->category()->first()->name,
            'campaign_date' => $campaign->created_at->toDateTimeString(),
            'is_campaign_owner' => $campaign->is_owner,
            'user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'avatar' => $user->image,
            'organization' => $user->organization,
            'position' => $user->position,
            'milestone_id' => $this->id,
            'milestone_title' => $this->title,
            'milestone_description' => $this->description,
            'milestone_image' => $this->image,
            'milestone_date' => $this->created_at->toDateTimeString()
        ];
        if (isset($this->attributes['image_object'])) {
            $result['image_object'] = $this->attributes['image_object'];
        }
        return new NewsfeedMessage($result);
    }
}

