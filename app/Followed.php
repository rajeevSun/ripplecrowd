<?php
namespace App;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 21-Dec-16
 * Time: 10:37
 */
trait Followed {
    public function checkFollowed($userId) {
        $followed = $this->follows()->where('follows.user_id', $userId)->first();
        if ($followed) {
            return $this->setFollowedAttribute(TRUE);
        } else {
            return $this->setFollowedAttribute(FALSE);
        }
    }

    public function getFollowedAttribute($value) {
        return $value;
    }

    public function setFollowedAttribute($value) {
        $this->attributes['followed'] = $value;
    }
}