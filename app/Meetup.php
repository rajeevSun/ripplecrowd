<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use App\User;
use App\Comment;
use App\File;

class Meetup extends Model
{
    protected $table = 'meetups';

    protected $fillable = ['title','description', 'location', 'attending_date', 'attending_time', 'lat', 'lng', 'image'];

    // date mutators
    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = TRUE;

    public function getImageAttribute($value) {
        return Image::getFullURL($value);
    }

    public function attendees() {
        return $this->belongsToMany(User::class, 'meetup_attendees', 'meetup_id', 'user_id')->withPivot(['role']);
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'comments', 'object', 'object_id', 'id');
    }

    public function commentUsers() {
        return $this->morphToMany(User::class, NULL, 'comments', 'object', 'object_id', 'user_id');
    }

    public function files() {
        return $this->morphMany(File::class, 'files', 'object', 'object_id', 'id');
    }

    public function image() {
        return $this->morphOne(File::class, NULL, 'object', 'object_id', 'image_id');
    }
}