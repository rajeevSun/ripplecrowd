<?php

namespace App\Http\Middleware;

use Closure;

class RequestAccept
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('Accept')!='*/*' && !$request->expectsJson()) {
            return response()->notAcceptable(new \stdClass());
        }

        return $next($request);
    }
}