<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Share;
use App\Activity;
use Validator;

class ShareController extends Controller
{
    public function store(Request $request) {
        $rules = [
            'id' => 'required|integer|min:1',
            'object' => 'required|in:campaign,post,milestone',
            'message' => 'nullable'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        /*$share = Share::where('user_id', $request->user()->id)
            ->where('object', $request->get('object'))
            ->where('object_id', $request->get('id'))
            ->select(['id', 'user_id', 'object', 'object_id', 'wall_id', 'created_at', 'updated_at'])
            ->first();
        if ($share) {
            return response()->success($share);
        }*/

        $share = new Share();
        $share->user_id = $request->user()->id;
        $share->object = $request->get('object');
        $share->object_id = $request->get('id');
        $share->wall_id = $request->user()->wall_id;
        $share->message = $request->get('message');

        if ($share->save()) {

            $user = $request->user();

            $activity = $share->activity()
                ->join('users','users.id', '=', 'activities.user_id')
                ->with('activitable')
                ->select([
                    'activities.id',
                    'activities.type',
                    'activities.action',
                    'activities.action_id',
                    'activities.created_at',
                    'activities.updated_at',
                    'users.first_name',
                    'users.last_name',
                    'users.name',
                    'users.position',
                    'users.organization',
                    'activities.user_id',
                    'users.image AS avatar'
                ])
                ->first();
            $activity->checkIsOwner($user->id);
            $activity->retrieveInfo($user->id);

            return response()->success($activity);
        } else {
            return response()->serverError();
        }
    }

    public function delete(Request $request, $shareId) {
        $share = Share::find($shareId);
        if ($share && $share->user_id == $request->user()->id && $share->delete()) {
            return response()->success();
        } elseif ($share && $share->user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$share) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }

    public function storeSharesByActivity(Request $request, $activityId) {
        $activity = Activity::where('id', $activityId)
            ->with('activitable')
            ->first();

        if ($activity) {
            $object = $activity->getSharableRelateObject();
            if ($object) {
                $share = new Share();
                $share->user_id = $request->user()->id;
                $share->wall_id = $request->user()->wall_id ? $request->user()->wall_id : 0;
                $object->shares()->save($share);

                $activity = $share->activity()
                    ->join('users','users.id', '=', 'activities.user_id')
                    ->with('activitable')
                    ->select([
                        'activities.id',
                        'activities.type',
                        'activities.action',
                        'activities.action_id',
                        'activities.created_at',
                        'activities.updated_at',
                        'users.first_name',
                        'users.last_name',
                        'users.name',
                        'users.position',
                        'users.organization',
                        'activities.user_id',
                        'users.image AS avatar'
                    ])
                    ->first();

                $activity->retrieveInfo($request->user()->id);
                return response()->success($activity);
            } else {
                return response()->serverError();
            }
        } else {
            return response()->notFound();
        }
    }

    public function edit(Request $request, $shareId) {
        $user = $request->user();

        $rules = [
            'message' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $share = Share::find($shareId);
        // check authorization
        if ($share->user_id != $user->id) {
            return response()->notAuthorize();
        }
        // END: check authorization
        $share->message = $request->get('message');
        $share->touch();
        if ($share->save()) {

            $activity = $share->activity()
                ->join('users','users.id', '=', 'activities.user_id')
                ->with('activitable')
                ->select([
                    'activities.id',
                    'activities.type',
                    'activities.action',
                    'activities.action_id',
                    'activities.created_at',
                    'activities.updated_at',
                    'users.first_name',
                    'users.last_name',
                    'users.name',
                    'users.position',
                    'users.organization',
                    'activities.user_id',
                    'users.image AS avatar'
                ])
                ->first();
            $activity->checkIsOwner($user->id);
            $activity->retrieveInfo($user->id);

            return response()->success($activity);
        } else {
            return response()->serverError();
        }
    }
}
