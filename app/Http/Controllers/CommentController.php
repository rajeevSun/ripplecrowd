<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Observers\CommentObserver;
use Illuminate\Http\Request;
use App\Comment;
use Validator;

class CommentController extends Controller
{
    public function getCommentsOfCampaign(Request $request, $campaignId) {
        $perPage = $request->get('per_page', config('setting.campaign.comment.default_per_page'));
        $page = $request->get('page', config('setting.campaign.comment.default_page'));
        $comments = Comment::where('comments.object', 'campaign')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.object_id', $campaignId)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy('comments.created_at', 'desc')
            ->select(['comments.*', 'users.first_name', 'users.last_name', 'users.image AS avatar'])
            ->get();
        return response()->success($comments);
    }

    public function getCommentsOfMeetup(Request $request, $meetupId) {
        $perPage = $request->get('per_page', config('setting.campaign.comment.default_per_page'));
        $page = $request->get('page', config('setting.campaign.comment.default_page'));
        $comments = Comment::where('comments.object', 'meetup')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.object_id', $meetupId)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy('comments.created_at', 'desc')
            ->select(['comments.*', 'users.first_name', 'users.last_name', 'users.image AS avatar'])
            ->get();
        return response()->success($comments);
    }

    public function getCommentsOfMilestone(Request $request, $milestoneId) {
        $perPage = $request->get('per_page', config('setting.campaign.comment.default_per_page'));
        $page = $request->get('page', config('setting.campaign.comment.default_page'));
        $comments = Comment::where('comments.object', 'milestone')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.object_id', $milestoneId)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy('comments.created_at', 'desc')
            ->select(['comments.*', 'users.first_name', 'users.last_name', 'users.image AS avatar'])
            ->get();
        return response()->success($comments);
    }

    public function getCommentsOfPost(Request $request, $postId) {
        $perPage = $request->get('per_page', config('setting.campaign.comment.default_per_page'));
        $page = $request->get('page', config('setting.campaign.comment.default_page'));
        $comments = Comment::where('comments.object', 'post')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.object_id', $postId)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy('comments.created_at', 'desc')
            ->select(['comments.*', 'users.first_name', 'users.last_name', 'users.image AS avatar'])
            ->get();
        return response()->success($comments);
    }

    public function getCommentsOfShare(Request $request, $shareId) {
        $perPage = $request->get('per_page', config('setting.campaign.comment.default_per_page'));
        $page = $request->get('page', config('setting.campaign.comment.default_page'));
        $comments = Comment::where('comments.object', 'share')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.object_id', $shareId)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy('comments.created_at', 'desc')
            ->select(['comments.*', 'users.first_name', 'users.last_name', 'users.image AS avatar'])
            ->get();
        return response()->success($comments);
    }

    public function getCommentsByActivity(Request $request, $activityId) {
        $perPage = $request->get('per_page', config('setting.campaign.comment.default_per_page'));
        $page = $request->get('page', config('setting.campaign.comment.default_page'));
        $activity = Activity::where('id', $activityId)
            ->with('activitable')
            ->first();
        if ($activity && $activity->getCommentableRelateObject()) {
            $comments = $activity->getCommentableRelateObject()
                ->comments()
                ->join('users', 'users.id', '=', 'comments.user_id')
                ->take($perPage)
                ->skip(($page-1)*$perPage)
                ->orderBy('comments.created_at', 'desc')
                ->select(['comments.*', 'users.first_name', 'users.last_name', 'users.image AS avatar'])
                ->get();
            return response()->success($comments);
        } else {
            return response()->notFound();
        }
        return response()->success([]);
    }

    public function store(Request $request) {
        $user = $request->user();
        $rules = [
            'id' => 'required|integer|min:1',
            'object' => 'required|in:comment,campaign,post,activity,meetup,milestone',
            'message' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $comment = new Comment();
        $comment->object = $request->get('object');
        $comment->object_id = $request->get('id');
        $comment->message = $request->get('message');
        $comment->user_id = $request->user()->id;
        if ($comment->save()) {
            $comment->setFirstNameAttribute($user->first_name);
            $comment->setLastNameAttribute($user->last_name);
            $comment->setAvatarAttribute($user->image);
            return response()->success($comment);
        } else {
            return response()->serverError();
        }
    }

    public function delete(Request $request, $commentId) {
        $comment = Comment::where('id', $commentId)
            ->first();

        if ($comment && $comment->user_id == $request->user()->id && $comment->delete()) {
            return response()->success();
        } elseif ($comment && $comment->user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$comment) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }

    public function storeCommentsByActivity(Request $request, $activityId) {
        $activity = Activity::where('id', $activityId)
            ->with('activitable')
            ->first();

        $rules = [
            'message' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        if ($activity) {
            $object = $activity->getCommentableRelateObject();
            if ($object) {
                $comment = new Comment();
                $comment->message = $request->get('message');
                $comment->user_id = $request->user()->id;
                $object->comments()->save($comment);

                $comment = Comment::join('users', 'users.id', '=', 'comments.user_id')
                    ->where('comments.id', $comment->id)
                    ->select([
                        'comments.id',
                        'comments.user_id',
                        'comments.message',
                        'comments.object',
                        'comments.object_id',
                        'comments.likes_count',
                        'comments.created_at',
                        'comments.updated_at',
                        'first_name',
                        'last_name',
                        'users.image AS avatar'
                    ])
                    ->first();

                return response()->success($comment);
            } else {
                return response()->serverError();
            }
        } else {
            return repsonse()->notFound();
        }
    }

    public function edit(Request $request, $commentId) {

        $currentUser = $request->user();

        $rules = [
            'message' => 'required',
            'id' => 'required|exists:comments,id'
        ];
        $validator = Validator::make(array_merge($request->all(), ['id' => $commentId]), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $comment = Comment::find($commentId);
        // check authorization
        if ($comment->user_id != $currentUser->id) {
            return response()->notAuthorize();
        }
        foreach ($rules as $field => $value) {
            if ($request->has($field)) {
                $comment->{$field} = $request->get($field);
            }
        }

        if ($comment->save()) {
            return response()->success($comment);
        } else {
            return response()->serverError();
        }
    }
}
