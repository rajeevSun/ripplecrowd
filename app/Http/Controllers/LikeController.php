<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Observers\LikeObserver;
use App\Share;
use App\Milestone;
use App\Campaign;
use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use App\Like;
use Validator;

class LikeController extends Controller
{
    public function store(Request $request) {
        $rules = [
            'object' => 'required|in:post,comment,activity,campaign,milestone,share',
            'id' => 'required|integer|min:1'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $like = Like::where('object', $request->get('object'))
            ->where('user_id', $request->user()->id)
            ->where('object_id', $request->get('id'))
            ->first();
        if ($like) {
            return response()->conflict($like);
        }

        $objectClass = 'App\\'. ucfirst($request->get('object'));
        $object = $objectClass::find($request->get('id'));
        if (!$object) {
            return response()->notFound();
        }

        $like = new Like();
        $like->object = $request->get('object');
        $like->object_id = $request->get('id');
        $like->user_id = $request->user()->id;
        if ($like->save()) {
            return response()->success($like);
        } else {
            return response()->serverError();
        }
    }

    public function delete(Request $request) {
        $rules = [
            'object' => 'required|in:post,comment,activity,campaign,milestone,share',
            'id' => 'required|integer|min:1'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $objectClass = 'App\\'. ucfirst($request->get('object'));
        $object = $objectClass::find($request->get('id'));
        if (!$object) {
            return response()->notFound();
        }

        $like = Like::where('user_id', $request->user()->id)
            ->where('object', $request->get('object'))
            ->where('object_id', $request->get('id'))
            ->first();

        if ($like && $like->user_id == $request->user()->id && $like->delete()) {
            return response()->success();
        } elseif ($like && $like->user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$like) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }

    public function storeLikesByActivity(Request $request, $activityId) {
        $activity = Activity::where('id', $activityId)
            ->with('activitable')
            ->first();
        if ($activity) {
            $object = $activity->getLikableRelateObject();
            if ($object) {
                $like = new Like();
                $like->user_id = $request->user()->id;
                if (get_class($object) == Share::class) {
                    $liked = Like::where('object', 'share')
                        ->where('object_id', $object->id)
                        ->where('user_id', $request->user()->id)
                        ->first();
                    if (!$liked) {
                        $like->object = 'share';
                        $like->object_id = $object->id;
                        $like->save();
                    }
                } else {

                    if (is_callable([$object, 'likes'], TRUE)) {

                        $liked = $object->likes()
                            ->where('likes.user_id', $request->user()->id)
                            ->where('likes.object_id', $object->id)
                            ->first();
                        if (!$liked) {
                            $object->likes()->save($like);
                        }
                    } else {
                        return response()->paramsError();
                    }
                }

                $activity = Activity::join('users','users.id', '=', 'activities.user_id')
                    ->where('activities.id', $activityId)
                    ->with('activitable')
                    ->select([
                        'activities.id',
                        'activities.type',
                        'activities.action',
                        'activities.action_id',
                        'activities.created_at',
                        'activities.updated_at',
                        'users.first_name',
                        'users.last_name',
                        'users.name',
                        'users.position',
                        'users.organization',
                        'activities.user_id',
                        'users.image AS avatar'
                    ])
                    ->first();
                $activity->retrieveInfo($request->user()->id);
                return response()->success($activity);
            } else {
                return response()->paramsError();
            }
        } else {
            return response()->notFound();
        }
    }

    public function deleteLikesByActivity(Request $request, $activityId) {
        $activity = Activity::where('id', $activityId)
            ->with('activitable')
            ->first();

        if ($activity) {
            $object = $activity->getLikableRelateObject();
            if ($object) {

                if (get_class($object) == Share::class) {
                    $like = Like::where('object', 'share')
                        ->where('object_id', $object->id)
                        ->where('user_id', $request->user()->id)
                        ->first();
                } else {
                    if (is_callable([$object, 'likes'], TRUE)) {
                        $like = $object->likes()
                            ->where('likes.user_id', $request->user()->id)
                            ->where('likes.object_id', $object->id)
                            ->first();
                    } else {
                        return response()->paramsError();
                    }
                }

                if ($like)
                    $like->delete();

                $activity = Activity::join('users','users.id', '=', 'activities.user_id')
                    ->where('activities.id', $activityId)
                    ->with('activitable')
                    ->select([
                        'activities.id',
                        'activities.type',
                        'activities.action',
                        'activities.action_id',
                        'activities.created_at',
                        'activities.updated_at',
                        'users.first_name',
                        'users.last_name',
                        'users.name',
                        'users.position',
                        'users.organization',
                        'activities.user_id',
                        'users.image AS avatar'
                    ])
                    ->first();
                $activity->retrieveInfo($request->user()->id);
                return response()->success($activity);
            } else {
                return response()->paramsError();
            }
        } else {
            return response()->notFound();
        }
    }
}
