<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Milestone;
use Validator;
use App\Activity;
use Image;
use Input;

class MilestoneController extends Controller
{
    public function store(Request $request) {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'campaign_id' => 'required|integer|min:1|exists:campaigns,id',
            'video' => 'sometimes|videolink',
            'image' => 'sometimes|image|mimes:jpg,jpeg,bmp,gif,png|max:5000'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $milestone = new Milestone();
        $milestone->title = $request->get('title');
        $milestone->description = $request->get('description');
        $milestone->campaign_id = $request->get('campaign_id');
        $milestone->video = $request->get('video', '');
        $milestone->user_id = $request->user()->id;
        $milestone->save();

        if ($milestone) {
            $milestone->image = Image::uploadCampaignImage('image', 'milestone', $milestone->id, 'milestone');

            $activity = Activity::join('users', 'users.id', '=', 'activities.user_id')
                ->where('activities.action', 'milestone')
                ->where('activities.action_id', $milestone->id)
                ->where('activities.user_id', $request->user()->id)
                ->select([
                    'activities.id',
                    'activities.type',
                    'activities.action',
                    'activities.action_id',
                    'activities.created_at',
                    'activities.updated_at',
                    'users.first_name',
                    'users.last_name',
                    'users.name',
                    'users.position',
                    'users.organization',
                    'activities.user_id',
                    'users.image AS avatar'
                ])
                ->with('activitable')
                ->first();
            if ($activity) {
                $activity->retrieveInfo($request->user()->id);
            }
            return response()->created($activity);
        } else {
            return response()->serverError();
        }
    }

    public function getMilestonesOfCampaign(Request $request, $campaignId) {
        $perPage = $request->query('per_page', config('setting.milestone.default_per_page'));
        $page = $request->query('page', config('setting.milestone.default_page'));
        $sortBy = $request->query('sort_by', config('setting.milestone.default_sort_by'));
        $order = $request->query('order', config('setting.milestone.default_order'));
        $milestones = Milestone::join('users', 'users.id', '=', 'milestones.user_id')
            ->join('campaigns', 'campaigns.id', '=', 'milestones.campaign_id')
            ->join('users AS owners', 'owners.id', '=', 'campaigns.user_id')
            ->where('milestones.campaign_id', $campaignId)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy($sortBy, $order)
            ->select([
                'milestones.*',
                'users.first_name',
                'users.last_name',
                'users.image AS avatar',
                'campaigns.name AS campaign_name',
                'campaigns.image AS campaign_image',
                'owners.first_name AS owner_first_name',
                'owners.last_name AS owner_last_name',
                'owners.id AS owner_id'
            ])
            ->get();
        $userId = $request->user()->id;
        foreach ($milestones as $milestone) {
            $milestone->checkLiked($userId);
            $milestone->checkShared($userId);
            $milestone->checkIsOwner($userId);
        }
        return response()->success($milestones);
    }

    public function edit(Request $request, $milestoneId) {
        $rules = [
            'title' => 'sometimes',
            'description' => 'sometimes',
            'campaign_id' => 'sometimes|integer|min:1|exists:campaigns,id',
            'video' => 'sometimes|videolink',
            'image' => 'sometimes|image|mimes:jpg,jpeg,bmp,gif,png|max:5000'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $milestone = Milestone::find($milestoneId);
        // check authorization
        if ($milestone->user_id != $request->user()->id) {
            return response()->notAuthorize();
        }
        foreach ($rules as $field => $value) {
            if ($field != 'image' && $request->has($field)) {
                $milestone->{$field} = $request->get($field);
            }
        }

        if (Input::file('image')) {
            $milestone->image = Image::uploadCampaignImage('image', 'milestone', $milestone->id, 'milestone');
        }

        if ($milestone->save()) {
            return response()->success($milestone);
        } else {
            return response()->serverError();
        }
    }

    public function delete(Request $request, $milestoneId) {
        $rules = [
            'id' => 'required|exists:milestones,id'
        ];
        $validator = Validator::make(['id' => $milestoneId], $rules);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $milestone = Milestone::find($milestoneId);
        if ($milestone && $milestone->user_id == $request->user()->id && $milestone->delete()) {
            return response()->success();
        } elseif ($milestone && $milestone->user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$milestone) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }

    public function show(Request $request, $milestoneId) {
        $milestone = Milestone::find($milestoneId);
        if ($milestone) {
            $userId = $request->user()->id;
            $milestone->checkLiked($userId);
            $milestone->checkShared($userId);
            return response()->success($milestone);
        } else {
            return response()->notFound();
        }
    }
}
