<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Backing;
use App\Campaign;
use Validator;

class BackingController extends Controller
{
    public function store(Request $request) {
        $rules = [
            'fund_amount' => 'required|numeric|min:1',
            'campaign_id' => 'required|integer|min:1|exists:campaigns,id'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $campaign = Campaign::find($request->get('campaign_id'));
        $user = $request->user();

        $backing = new Backing();
        $backing->user_id = $user->id;
        $backing->fund_amount = $request->get('fund_amount');
        $backing->campaign_id = $campaign->id;
        $backing->category_id = $campaign->category_id;
        $backing->received_user_id = $campaign->user_id;
        $backing->bank_name = $user->bank_name;
        $backing->bank_account = $user->bank_account;
        $backing->pending = 1;

        if ($backing->save()) {
            return response()->success($backing);
        } else {
            return response()->serverError();
        }
    }

    public function edit(Request $request, $backingId) {
        $rules = [
            'pending' => 'sometimes|boolean'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $backing = Backing::find($backingId);
        // check authorize
        if ($backing->received_user_id != $request->user()->id) {
            return response()->notAuthorize();
        }
        // END: check authorize
        $backing->pending = $request->input('pending');
        if ($backing->save()) {
            return response()->success($backing);
        } else {
            return response()->serverError();
        }
    }

    public function delete(Request $request, $backingId) {
        $rules = [
            'id' => 'required|exists:backings,id'
        ];
        $validator = Validator::make(['id' => $backingId], $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $backing = Backing::find($backingId);
        if ($backing && $backing->received_user_id == $request->user()->id && $backing->delete()) {
            return response()->success();
        } elseif ($backing && $backing->received_user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$backing) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }

    public function getBackingsOfCampaign(Request $request, $campaignId) {
        $backings = Backing::join('users', 'users.id', '=', 'backings.user_id')
            ->where('backings.campaign_id', $campaignId)
            ->select([
                'backings.id',
                'backings.user_id',
                'backings.fund_amount',
                'users.image AS avatar',
                'backings.pending',
                'users.first_name',
                'users.last_name',
                'backings.campaign_id'
            ])
            ->get();
        return response()->success($backings);
    }
}
