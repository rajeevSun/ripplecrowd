<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use Validator;

class InvitationController extends Controller
{
    public function delete(Request $request, $invitationId) {
        $invitation = Invitation::where('id', $invitationId)
            ->first();
        if ($invitation && $invitation->invited_user_id == $request->user()->id && $invitation->delete()) {
            return response()->success();
        } elseif ($invitation && $invitation->invited_user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$invitation) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }

    // create new invitation
    public function store(Request $request) {
        $userId = $request->user()->id;
        // set rules for validator
        $rules = [
            'invited_user_id' => 'required|integer',
            'campaign_id' => 'required|integer'
        ];
        // match validator between request and rules.
        $validator = Validator::make($request->all(), $rules);
        // validate
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $invitation = new Invitation();
        $invitation->user_id = $userId;
        $invitation->invited_user_id = $request->get('invited_user_id');
        $invitation->campaign_id = $request->get('campaign_id');
        $saved = $invitation->save();
        if ($saved) {
            return response()->success($invitation);
        } else {
            return response()->serverError();
        }
    }
}
