<?php

namespace App\Http\Controllers;

use App\Elastic;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function fullTextSearch(Request $request, Elastic $elastic)
    {
        $q = $request->get('q');
        $q = strtolower($q);
        $results = [];

        $page = $request->input('page', config('setting.campaign.default_page'));
        $perPage = $request->input('per_page', config('setting.campaign.default_per_page'));
        $type = $request->input('type');
        if (!in_array($type, ['user', 'campaign', 'meetup', 'group'])) {
            $searchResults = $elastic->msearch([
                'body' => [
                    // users
                    [
                        'index' => 'ripple',
                        'type' => 'users'
                    ],
                    [
                        'query' => [
                            'prefix' => [
                                'name' => $q
                            ],
                        ],
                        'from' => 0,
                        'size' => 3
                    ],
                    // campaigns
                    [
                        'index' => 'ripple',
                        'type' => 'campaigns'
                    ],
                    [
                        'query' => [
                            'prefix' => [
                                'name' => $q,
                            ]
                        ],
                        'from' => 0,
                        'size' => 3
                    ],
                    // meetups
                    [
                        'index' => 'ripple',
                        'type' => 'meetups'
                    ],
                    [
                        'query' => [
                            'prefix' => [
                                'name' => $q,
                            ]
                        ],
                        'from' => 0,
                        'size' => 3
                    ],
                    // groups
                    [
                        'index' => 'ripple',
                        'type' => 'groups'
                    ],
                    [
                        'query' => [
                            'prefix' => [
                                'name' => $q,
                            ]
                        ],
                        'from' => 0,
                        'size' => 3
                    ],
                ]
            ]);
        } else {
            $searchResults = $elastic->search([
                'index' => 'ripple', // index name
                'type' => $type.'s', // type name
                'body' => [ // body of request
                    'query' => [
                        'prefix' => ['name' => $q]
                    ],
                    'from' => ($page-1)*$perPage, // offset of document
                    'size' => $perPage  // numbers of documents
                ]
            ]);
        }

        if (isset($searchResults['responses'])) {
            foreach ($searchResults['responses'] as $searchResult) {
                if ($searchResult['hits']['hits']) {
                    foreach ($searchResult['hits']['hits'] as $object) {
                        $results[] = $object['_source'];
                    }
                }
            }
        } elseif (isset($searchResults['hits']['hits'])) {
            foreach ($searchResults['hits']['hits'] as $object) {
                $results[] = $object['_source'];
            }
        }
        if ($q) {
            return response()->success($results);
        } else {
            return response()->empty([]);
        }
    }
}