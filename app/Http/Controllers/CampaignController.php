<?php

namespace App\Http\Controllers;

use App\Elastic;
use Input;
use App\User;
use App\Activity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Campaign;
use Validator;
use Image;

class CampaignController extends Controller
{
    const DEFAULT_PAGE = 1;
    const DEFAULT_PER_PAGE = 15;
    const DEFAULT_SORT_BY = 'updated_at';
    const DEFAULT_ORDER = 'desc';
    const DEFAULT_CATEGORY = 'all';

    public function getCampaigns(Request $request, $baseCampaigns = NULL) {

        $categoryId = $request->input('category_id', config('setting.campaign.default_category'));
        $page = $request->input('page', config('setting.campaign.default_page'));
        $perPage = $request->input('per_page', config('setting.campaign.default_per_page'));
        $sortBy = $request->input('sort_by', config('setting.campaign.default_sort_by'));
        $order = $request->input('order', config('setting.campaign.default_order'));
        $userId = $request->user()->id;

        if (!$baseCampaigns) {
            $campaigns = Campaign::join('categories', 'categories.id', '=', 'campaigns.category_id')
                ->join('users', 'users.id', '=', 'campaigns.user_id');
        } else {
            $campaigns = $baseCampaigns->join('categories', 'categories.id', '=', 'campaigns.category_id');
        }
        $campaigns = $campaigns
            ->join('currencies', 'campaigns.currency_id', '=', 'currencies.id')
            ->where('campaigns.funding_goal', '!=', 0)
            ->where('campaigns.campaign_verified', '=', 2);
        if ($categoryId != config('setting.campaign.default_category')) {
            $campaigns = $campaigns->where('campaigns.category_id', '=', $categoryId);
        }

        if ($request->input('name')) {
            $campaigns = $campaigns->where('campaigns.name', 'like', '%'. $request->input('name') .'%');
        }

        if ($request->input('q')) {
            $campaigns = $campaigns->where('campaigns.name', 'LIKE', '%'. $request->input('q') .'%');
        }

        $campaigns = $campaigns->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy('campaigns.'.$sortBy, $order)
            ->select([
                'campaigns.id',
                'campaigns.name',
                'campaigns.short_blurb',
                'campaigns.user_id',
                'campaigns.funding_kind',
                'campaigns.category_id',
                'categories.name AS category_name',
                'campaigns.funding_current',
                'campaigns.funding_goal',
                'campaigns.image',
                'campaigns.ending_at',
                'users.first_name',
                'users.last_name',
                'categories.name AS category_name',
                'currencies.currency_symbol',
                'campaigns.created_at',
                'campaigns.updated_at'
            ])
            ->distinct()
            ->get();
        // update days_remaining, percent_funded
        $today = Carbon::now();
        foreach ($campaigns as &$campaign) {
            $campaign->days_remaining = $today->diffInDays($campaign->ending_at, FALSE);
            $campaign->percent_funded = round(($campaign->funding_current/$campaign->funding_goal)*100);
            $campaign->checkIsOwner($userId);
        }
        return $campaigns;
    }

    public function followingCampaigns(Request $request) {
        $baseCampaigns = $request->user()->followingCampaigns()->join('users', 'users.id', '=', 'campaigns.user_id');
        $campaigns = $this->getCampaigns($request, $baseCampaigns);

        if ($campaigns->isEmpty()) {
            return response()->empty($campaigns);
        } else {
            return response()->success($campaigns);
        }
    }

    public function discoverCampaigns(Request $request) {
        $campaigns = $this->getCampaigns($request);

        if ($campaigns->isEmpty()) {
            return response()->empty($campaigns);
        } else {
            return response()->success($campaigns);
        }
    }

    public function recommendCampaigns(Request $request) {
        $user = $request->user();
        $followedCampaignIds = $user->followedCampaigns()->pluck('campaigns.id');
        $campaigns = Campaign::join('interests', 'interests.category_id', '=', 'campaigns.category_id')
            ->join('users', 'users.id', '=', 'campaigns.user_id')
            ->where('interests.user_id', '=', $user->id)
            ->where('campaigns.user_id', '<>', $user->id)
            ->whereNotIn('campaigns.id', $followedCampaignIds);
        $campaigns = $this->getCampaigns($request, $campaigns);
        if ($campaigns->isEmpty()) {
            return response()->empty($campaigns);
        } else {
            return response()->success($campaigns);
        }
    }

    public function trendingCampaigns(Request $request) {
        $campaigns = Campaign::where('campaigns.popular', 1)
            ->join('users', 'users.id', '=', 'campaigns.user_id');
        $campaigns = $this->getCampaigns($request, $campaigns);

        if ($campaigns->isEmpty()) {
            return response()->empty($campaigns);
        } else {
            return response()->success($campaigns);
        }
    }

    // create new campaigns
    public function store(Request $request) {
        $rules = array(
            'name' => 'required|unique:campaigns,name,NULL,id,deleted_at,NULL',
            'description' => 'required',
            'short_blurb' => 'required|max:135',
            'category_id' => 'required|integer|min:1',
            'logo' => 'image|mimes:jpg,jpeg,bmp,gif,png|max:5000',
            'video' => 'videolink|url',
            'image' => 'required|image|mimes:jpg,jpeg,bmp,gif,png|max:8192',
            'funding_duration' => 'required|integer|min:1',
            'funding_goal' => 'required|numeric|not_in:0|min:1',
            'funding_strategy' => 'required|in:allornothing,keepitall',
            'funding_kind' => 'required|in:equity,debt,revenue',
            'debt_duration' => 'required_if:funding_kind,debt|integer|min:1',
            'interest_rate_amount' => 'required_if:funding_kind,debt|numeric',
            'interest_payment_period' => 'required_if:funding_kind,debt|in:annual,semiannual,none',
            'revenue_share' => 'required_if:funding_kind,revenue|numeric',
            'repayment_amount' => 'required_if:funding_kind,revenue|numeric',
            'share_price' => 'required_if:funding_kind,equity|numeric',
            'share_type' => 'required_if:funding_kind,equity|in:common,prefer'
        );
        $messages = array(
            'logo' => 'File format is not supported. Allowed format are jpg,jpeg,bmp,gif,png.',
            'image' => 'File format is not supported. Allowed format are jpg,jpeg,bmp,gif,png.',
            'funding_goal.not_in' => 'Funding goal must be a valid number.',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->setAttributeNames([
            'name' => 'Campaign Name',
            'description' => 'Description',
            'short_blurb' => 'Short Description or Tagline',
            'category_id' => 'Category',
            'logo' => 'Campaign Logo',
            'video' => 'Campaign Video',
            'image' => 'Campaign Image',
            'funding_duration' => 'Campaign Duration',
            'funding_goal' => 'Campaign Goal',
            'funding_strategy' => 'Campaign Strategy',
            'funding_kind' => 'Campaign Type',
            'debt_duration' => 'Debt Duration',
            'interest_rate_amount' => 'Interest Rate Amount',
            'interest_payment_period' => 'Interest Payment Period',
            'revenue_share' => 'Revenue Share',
            'repayment_amount' => 'Repayment Amount',
            'share_price' => 'Share Price',
            'share_type' => 'Share Type'
        ]);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        } else {
            $input = $request->all();
            $input['user_id'] = $request->user()->id;

            // set endingon field
            $currentDate = Carbon::today();
            $currentDate->addDays($input['funding_duration']);
            $input['ending_at'] = $currentDate->toDateString();
            // end set ending field



            // save campaign video
            $video = Input::get('video');
            if ($video && strpos($video, 'www.youtube.com/watch?v=') !== false) {
                $temp = explode('?v=', $video);
                $source = $temp[1];
                $link = "http://www.youtube.com/embed/" . $source;
                $input['video'] = $video;
            }
            // end save campaign video

            // increase total created campaign for current user
            $request->user()->increment('created_count');


            // set default attribute for campaign
            $input['debt_duration'] = $request->get('debt_duration', 0);
            $input['interest_rate_amount'] = $request->get('interest_rate_amount', 0);
            $input['interest_payment_period'] = $request->get('interest_payment_period','none');
            $input['location'] = '182'; //currently, 182 is default value until location plays a part in features
            $input['funding_current'] = 0;
            $input['backings_count'] = 0;
            $input['currency_id'] = 1; //currently, 1 is default value until currency feature plays a part
            $input['campaign_verified'] = 2; //currently, 2 is default value until admin approval is required
            $input['repayment_amount'] = $request->get('repayment_amount', 0);
            $input['revenue_share'] = $request->get('revenue_share', 0);
            // end: set default attribute for campaign

            $campaign = Campaign::create($input);

            if ($campaign) {
                // save campaign image
                $campaign->image = Image::uploadCampaignImage('image', 'campaign', $campaign->id, 'image');
                // end save campaign image

                // save campaign logo
                $campaign->logo = Image::uploadCampaignImage('logo', 'campaign', $campaign->id, 'logo');
                // end save campaign logo


                $activity = Activity::join('users', 'users.id', '=', 'activities.user_id')
                    ->where('activities.action', 'campaign')
                    ->where('activities.action_id', $campaign->id)
                    ->where('activities.user_id', $request->user()->id)
                    ->select([
                        'activities.id',
                        'activities.type',
                        'activities.action',
                        'activities.action_id',
                        'activities.created_at',
                        'activities.updated_at',
                        'users.first_name',
                        'users.last_name',
                        'users.name',
                        'users.position',
                        'users.organization',
                        'activities.user_id',
                        'users.image AS avatar'
                    ])
                    ->with('activitable')
                    ->first();
                if ($activity) {
                    $activity->checkIsOwner($request->user()->id);
                    $activity->retrieveInfo($request->user()->id);
                }

                return response()->created($activity);
            } else {
                return response()->serverError();
            }
        }
    }

    public function show(Request $request, $campaignId) {
        $campaign = Campaign::join('users', 'users.id', '=', 'campaigns.user_id')
            ->join('categories', 'categories.id', '=', 'campaigns.category_id')
            ->where('campaigns.id', $campaignId)
            ->select([
                'campaigns.*',
                'users.first_name',
                'users.last_name',
                'users.position',
                'users.organization',
                'users.image AS avatar',
                'categories.name AS category_name'
            ])
            ->first();
        if ($campaign) {
            $userId = $request->user()->id;
            $campaign->checkFollowed($userId);
            $campaign->checkLiked($userId);
            $campaign->checkShared($userId);
            $campaign->checkIsOwner($userId);
            $campaign->append(['days_remaining', 'percent_funded']);
            // set owner followed attribute
            $ownerFollowed = $request->user()->usersFollowing()->wherePivot('object_id', $campaign->user_id)->first();
            if ($ownerFollowed) {
                $campaign->setOwnerFollowedAttribute(TRUE);
            } else {
                $campaign->setOwnerFollowedAttribute(FALSE);
            }
            return response()->success($campaign);
        } else {
            return response()->notFound($campaign);
        }
    }

    // need repository pattern here???
    public function getRecommendCampaigns(Request $request) {

        $page = $request->input('page', self::DEFAULT_PAGE);
        $perPage = $request->input('per_page', 2);

        Campaign::join('interests', 'interests.category_id', '=', 'campaigns.category_id')
            ->where('users.id', '=', $request->user()->id)
            ->join('users', 'users.id', '=', 'campaigns.user_id')
            ->join('currencies', 'campaigns.currency_id', '=', 'currencies.id')
            ->where('campaigns.funding_goal', '!=', 0)
            ->where('campaigns.campaign_verified', '=', 2)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy('campaigns.'.config('setting.campaign.default_sort_by'), config('setting.campaign.default_order'))
            ->select([
                'campaigns.id',
                'campaigns.name',
                'campaigns.short_blurb',
                'campaigns.user_id',
                'campaigns.funding_kind',
                'campaigns.category_id',
                'campaigns.funding_current',
                'campaigns.funding_goal',
                'campaigns.image',
                'campaigns.ending_at',
                'users.first_name',
                'users.last_name',
                'users.name',
                'categories.name',
                'currencies.currency_symbol'
            ])
            ->get();
    }

    // update a campaign
    public function edit(Request $request, $campaignId) {
        $rules = array(
            'name' => 'sometimes|unique:campaigns,name,'.$campaignId.',id,deleted_at,NULL',
            'description' => 'sometimes',
            'short_blurb' => 'sometimes|max:135',
            'category_id' => 'sometimes|integer|min:1',
            'logo' => 'sometimes|image|mimes:jpg,jpeg,bmp,gif,png|max:8192',
            'video' => 'sometimes|videolink|url',
            'image' => 'sometimes|required|image|mimes:jpg,jpeg,bmp,gif,png|max:5000'
        );
        $messages = array(
            'logo' => 'File format is not supported. Allowed format are jpg,jpeg,bmp,gif,png.',
            'image' => 'File format is not supported. Allowed format are jpg,jpeg,bmp,gif,png.'
        );

        // input data
        $inputData = $request->all();
        $inputData['id'] = $campaignId;

        // validate input data
        $validator = Validator::make($inputData, $rules, $messages);

        $validator->setAttributeNames([
            'id' => 'Campaign ID',
            'name' => 'Campaign Name',
            'description' => 'Description',
            'short_blurb' => 'Short Description or Tagline',
            'category_id' => 'Category',
            'logo' => 'Campaign Logo',
            'video' => 'Campaign Video',
            'image' => 'Campaign Image'
        ]);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        //$campaign = Campaign::find($campaignId);
        $campaign = Campaign::join('users', 'users.id', '=', 'campaigns.user_id')
            ->join('categories', 'categories.id', '=', 'campaigns.category_id')
            ->where('campaigns.id', $campaignId)
            ->select([
                'campaigns.*',
                'users.first_name',
                'users.last_name',
                'users.position',
                'users.organization',
                'users.image AS avatar',
                'categories.name AS category_name'
            ])
            ->first();

        if ($campaign->user_id != $request->user()->id) {
            return response()->notAuthorize();
        }

        foreach ($rules as $field => $rule) {
            if ($request->has($field) && $field == 'video') {
                $campaign->{$field} = $request->get('video');
            } elseif ($request->has($field) && $field!='id' && $field!='image' && $field!='logo') {
                $campaign->{$field} = $request->get($field);
            }
        }

        if ($campaign->save()) {
            // save campaign image
            if (Input::file('image')) {
                $campaign->image = Image::uploadCampaignImage('image', 'campaign', $campaign->id, 'image');
            }
            // end save campaign image

            // save campaign logo
            if (Input::file('logo')) {
                $campaign->logo = Image::uploadCampaignImage('logo', 'campaign', $campaign->id, 'logo');
            }
            // end save campaign logo

            $userId = $request->user()->id;
            $campaign->checkFollowed($userId);
            $campaign->checkLiked($userId);
            $campaign->checkShared($userId);
            $campaign->checkIsOwner($userId);
            $campaign->append(['days_remaining', 'percent_funded']);
            // set owner followed attribute
            $ownerFollowed = $request->user()->usersFollowing()->wherePivot('object_id', $campaign->user_id)->first();
            if ($ownerFollowed) {
                $campaign->setOwnerFollowedAttribute(TRUE);
            } else {
                $campaign->setOwnerFollowedAttribute(FALSE);
            }
            return response()->success($campaign);
        } else {
            return response()->serverError();
        }
    }
}