<?php

namespace App\Http\Controllers;

use App\GroupMember;
use Illuminate\Http\Request;
use App\Group;
use App\Category;
use Image;
use Validator;

class GroupController extends Controller
{
    public function getGroups(Request $request) {
        $page = $request->query('page', config('setting.group.default_page'));
        $perPage = $request->query('per_page', config('setting.group.default_per_page'));
        $categories = Category::with('groupsWithPage')
            ->skip(($page-1)*$perPage)
            ->take($perPage)
            ->select(['id', 'name'])
            ->get();
        return response()->success($categories);
    }

    public function getJoinedGroups(Request $request) {
        $page = $request->query('page', config('setting.group.default_page'));
        $perPage = $request->query('per_page', config('setting.group.default_per_page'));
        $query = $request->query('q');

        $groups = $request->user()->groups()->skip(($page-1)*$perPage)->take($perPage);
        if ($query) {
            $groups->where('name', 'like', '%'.$query.'%');
        }
        $groups = $groups->get();
        return response()->success($groups);
    }
    // create new group
    public function store(Request $request) {
        $userId = $request->user()->id;
        // set rules for validator
        $rules = [
            'name' => 'required',
            'image' => 'image|mimes:jpg,jpeg,bmp,gif,png|max:5000',
            'category_id' => 'required|integer',
            'description' => 'required'
        ];
        // match validator between request and rules.
        $validator = Validator::make($request->all(), $rules);
        // validate
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $group = new Group();
        $group->owner_id = $userId;
        $group->name = $request->get('name');
        $group->category_id = $request->get('category_id');
        $group->description = $request->get('description');
        $saved = $group->save();
        if ($saved) {
            Image::uploadGroupImage('image', 'group', $group->id, 'group');
            return response()->success($group);
        } else {
            return response()->serverError();
        }
    }
    // edit group by id
    public function edit(Request $request, $groupId) {
        // set rules for validator
        $rules = [
            'name' => 'sometimes',
            'image' => 'sometimes|image|mimes:jpg,jpeg,bmp,gif,png|max:5000',
            'description' => 'sometimes'
        ];
        // match validator between request and rules.
        $validator = Validator::make($request->all(), $rules);

        $group = Group::find($groupId);

        // validate
        if ($validator->fails() || !$group) {
            return response()->paramsError($validator->messages());
        }

        // update name if exists.
        if ($request->has('name')) {
            $group->group_name = $request->get('name');
        }

        // update description if exists.
        if ($request->has('description')) {
            $group->description = $request->get('description');
        }
        $saved = $group->save();
        if ($saved) {
            // update image if exists.
            if ($request->hasFile('image')) {
                Image::uploadGroupImage('image', 'group', $group->id, 'group');
            }
            return response()->success($group);
        } else {
            return response()->serverError();
        }
    }
    // get group by id
    public function getGroupById(Request $request, $groupId) {
        $group = Group::find($groupId);
        if ($group) {
            return response()->success($group);
        } else {
            return response()->notFound();
        }
    }

    // get members of group
    public function getMembersById($groupId) {
        $group = Group::find($groupId);
        if ($group) {
            $members = $group->allMembers()
                ->select(['users.id', 'users.image AS avatar', 'users.first_name', 'users.last_name', 'group_members.role'])
                ->get()
                ->groupBy('role');
            return response()->success($members);
        } else {
            return response()->notFound();
        }
    }
    // join group
    public function joinGroup(Request $request, $groupId) {
        $groupMember = GroupMember::where('user_id', $request->user()->id)
            ->where('group_id', $groupId)
            ->first();
        if (!$groupMember) {
            $createGroupMember = GroupMember::create([
                'group_id' => $groupId,
                'user_id' => $request->user()->id,
                'role' => 'member'
            ]);
            if (!$createGroupMember) {
                return response()->serverError();
            }
        }
        return response()->success();
    }
    // leave group
    public function leaveGroup(Request $request, $groupId) {
        $groupMember = GroupMember::where('user_id', $request->user()->id)
            ->where('group_id', $groupId)
            ->delete();
        if ($groupMember) {
            return response()->success();
        } else {
            return response()->serverError();
        }
    }
    // delete group
    public function delete(Request $request, $groupId) {
        $group = Group::where('id', $groupId)
            ->first();
        if ($group && $group->owner_id == $request->user()->id && $group->delete()) {
            return response()->success();
        } elseif ($group && $group->owner_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$group) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }
}