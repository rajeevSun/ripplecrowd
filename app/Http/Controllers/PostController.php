<?php

namespace App\Http\Controllers;

use App\Helpers\Video;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;
use Validator;
use App\Post;
use App\Activity;
use Image;
use Input;

class PostController extends Controller
{
    public static function store(Request $request) {
        $user = $request->user();
        $rules = [
            'message' => 'required',
            'image' => 'nullable|image|mimes:jpg,jpeg,bmp,gif,png|max:5000',
            'video' => 'nullable|mimetypes:video/avi,video/mpeg,video/quicktime,video/x-flv,video/x-ms-wmv,video/mp4'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $post = new Post();
        $post->message = $request->get('message');
        $post->user_id = $request->user()->id;
        $post->wall_id = $request->user()->wall_id;
        if (Input::file('image')) {
            $post->type = 'image_internal';
        } else {
            $post->type = 'text';
        }
        $saved = $post->save();
        if ($saved) {
            $image = Image::uploadUserImage('image', 'post', $post->id, 'post');
            if ($image) {
                $post->image = $image;
            }
            $activity = Activity::join('users', 'users.id', '=', 'activities.user_id')
                ->where('activities.action', 'post')
                ->where('activities.action_id', $post->id)
                ->where('activities.user_id', $request->user()->id)
                ->select([
                    'activities.id',
                    'activities.type',
                    'activities.action',
                    'activities.action_id',
                    'activities.created_at',
                    'activities.updated_at',
                    'users.first_name',
                    'users.last_name',
                    'users.name',
                    'users.position',
                    'users.organization',
                    'activities.user_id',
                    'users.image AS avatar'
                ])
                ->with('activitable')
                ->first();
            if ($activity) {
                $activity->checkIsOwner($user->id);
                $activity->retrieveInfo($user->id);
            }
            return response()->created($activity);
        } else {
            return response()->serverError($saved);
        }
    }

    public function delete(Request $request, $postId) {
        $post = Post::where('id', $postId)
            ->first();
        if ($post && $post->user_id == $request->user()->id && $post->delete()) {
            return response()->success();
        } elseif ($post && $post->user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$post) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }

    public function edit(Request $request, $postId) {
        $user = $request->user();
        // validation rules
        $rules = [
            'message' => 'sometimes',
            'image' => 'nullable|image|mimes:jpg,jpeg,bmp,gif,png|max:5000',
            'video' => 'nullable|mimetypes:video/avi,video/mpeg,video/quicktime,video/x-flv,video/x-ms-wmv,video/mp4'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $post = Post::find($postId);

        // check if post is exist
        if (!$post) {
            return response()->notFound();
        }
        // END: check if post is exist

        // check authorize post
        if ($post->user_id != $user->id) {
            return response()->notAuthorize();
        }
        // END: check authorize post

        $post->message = $request->get('message');
        if (Input::file('image')) {
            $post->type = 'image_internal';
        }

        if (Input::file('image')) {
            $image = Image::uploadUserImage('image', 'post', $post->id, 'post');
            if ($image) {
                $post->image = $image;
                $post->type = 'image_internal';
            }

        } elseif ($request->has('remove_image') && $request->get('remove_image')) {
            $post->image = '';
            $post->image_id = NULL;
            $post->type = 'text';
            $post->video = '';
            $post->description = '';
            $post->title = '';
        }

        $saved = $post->save();
        if ($saved) {
            $activity = Activity::join('users', 'users.id', '=', 'activities.user_id')
                ->where('activities.action', 'post')
                ->where('activities.action_id', $post->id)
                ->where('activities.user_id', $request->user()->id)
                ->select([
                    'activities.id',
                    'activities.type',
                    'activities.action',
                    'activities.action_id',
                    'activities.created_at',
                    'activities.updated_at',
                    'users.first_name',
                    'users.last_name',
                    'users.name',
                    'users.position',
                    'users.organization',
                    'activities.user_id',
                    'users.image AS avatar'
                ])
                ->with('activitable')
                ->first();
            if ($activity) {
                $activity->checkIsOwner($user->id);
                $activity->retrieveInfo($user->id);
            }
            return response()->created($activity);
        } else {
            return response()->serverError($saved);
        }
    }
}
