<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use App\Activity;

class NotificationController extends Controller
{
    public function getNotifications(Request $request) {
        $user = $request->user();
        $perPage = $request->query('per_page', config('setting.notification.default_per_page'));
        $page = $request->query('page', config('setting.notification.default_page'));
        $sortBy = $request->query('sort_by', config('setting.notification.default_sort_by'));
        $order = $request->query('order', config('setting.notification.default_order'));
        $notifications = Notification::where('notifications.notifiable_id', $user->id)
            ->where('notifications.notifiable_type', 'user')
            ->join('users', 'users.id', '=', 'notifications.user_id')
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy($sortBy, $order)
            ->select([
                'notifications.id',
                'notifications.notification_type',
                'notifications.target_object',
                'notifications.target_name',
                'notifications.target_id',
                'notifications.target_activity_id',
                'notifications.created_at',
                'notifications.updated_at',
                'users.id as user_id',
                'users.first_name',
                'users.last_name',
                'users.image as avatar'
            ])
            ->get();
        return response()->success($notifications);
    }
}