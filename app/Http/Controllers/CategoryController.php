<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index() {
        $categories = Category::select(['id', 'name', 'created_at', 'updated_at'])->get();
        if ($categories->isEmpty()) {
            return response()->empty();
        } else {
            return response()->success($categories);
        }
    }
}
