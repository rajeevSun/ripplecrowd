<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Profiling;

class ProfilingController extends Controller
{
    public function all(Request $request, $profilingId = NULL) {
        if ($profilingId) {
            $profilings = $request->user()->profilings()->where('profilings.id', $profilingId)->get();
        } else {
            $profilings = $request->user()->profilings()->get();
        }
        return response()->success($profilings);
    }

    public function add(Request $request) {
        $rules = array(
            'title' => 'required',
            'description' => 'sometimes',
            'type' => 'required|in:campaigns,experiences,certifications,organizations,education',
            'start_date' => 'sometimes|date_format:Y-m-d H:i:s',
            'end_date' => 'sometimes|date_format:Y-m-d H:i:s'
        );

        $validator = Validator::make($request->all(), $rules);

        $validator->setAttributeNames([
            'title' => 'title',
            'description' => 'Description',
            'type' => 'Type',
            'start_date' => 'Start Date',
            'end_date' => 'End Date'
        ]);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        } else {
            $input = $request->all();
            $input['user_id'] = $request->user()->id;

            $profiling = Profiling::create($input);

            if ($profiling) {
                return response()->created($profiling);
            } else {
                return response()->serverError($profiling);
            }
        }
    }

    public function remove(Request $request, $profilingId) {
        $profiling = Profiling::find($profilingId);
        if ($profiling) {
            $profiling->delete();
            return response()->success();
        } else {
            return response()->notFound();
        }
    }

    public function edit(Request $request, $profilingId) {
        $profiling = Profiling::find($profilingId);
        if (!$profiling) {
            return response()->notFound();
        }

        $rules = array(
            'title' => 'required',
            'description' => 'sometimes',
            'type' => 'required|in:campaigns,experiences,certifications,organizations,education',
            'start_date' => 'sometimes|date_format:Y-m-d H:i:s',
            'end_date' => 'sometimes|date_format:Y-m-d H:i:s'
        );

        $validator = Validator::make($request->all(), $rules);

        $validator->setAttributeNames([
            'title' => 'title',
            'description' => 'Description',
            'type' => 'Type',
            'start_date' => 'Start Date',
            'end_date' => 'End Date'
        ]);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        } else {
            $input = $request->all();
            $input['user_id'] = $request->user()->id;

            $profilingSaved = $profiling->update($input);

            if ($profilingSaved) {
                return response()->success($profiling);
            } else {
                return response()->serverError($profilingSaved);
            }
        }
    }
}
