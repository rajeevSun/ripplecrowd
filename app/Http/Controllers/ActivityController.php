<?php

namespace App\Http\Controllers;

use App\Entities\NewsfeedMessage;
use App\Post;
use App\Share;
use App\User;
use App\Wall;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Activity;
use App\Follow;
use App\Campaign;
use App\Comment;
use App\Like;
use Image;

class ActivityController extends Controller
{
    // get activites for newsfeed APIs.
    public function getActivitiesStreamByUser(Request $request, $wallId = NULL) {
        $page = $request->query('page', config('setting.activity.default_page'));
        $perPage = $request->query('per_page', config('setting.activity.default_per_page'));
        $user = $request->user();
        $userId = $request->user()->id;

        // check wall id.
        if ($wallId) {
            $isOwnWall = $this->isOwnWall($wallId, $user);
            $isOwnGroupWall = $this->isOwnGroupWall($wallId, $user);
            // if wallId is not own user nor own group user.
            if (!($isOwnWall xor $isOwnGroupWall)) {
                return response()->notAuthorize();
            } elseif ($isOwnGroupWall) {
                $wallGroupId = $wallId;
            }
        }

        $followingUserIds = $this->getFollowingUserIds($userId);
        $activities = Activity::join('users', 'users.id', '=', 'activities.user_id');

        // why check wall id of group? if wall of current user we will get all activity relate to user.
        // but if group, we only get post and shared of that group.
        // but if other user, we only get post and shared of that user.
        if (isset($wallGroupId) && $isOwnGroupWall) {
            $activities = $activities->where('activities.wall_id', $wallGroupId)
                ->where(function($query) {
                    $query->orWhere('type' , 'share_post')
                        ->orWhere('type', 'share_campaign')
                        ->orWhere('type', 'create_post');
                });
        } else {
            $activities = $activities->where(function($query) use ($followingUserIds, $userId) {
                    foreach($followingUserIds as $followingUser => $date) {
                        $query->orWhere(function($query) use ($followingUser, $date) {
                            return $query->where('activities.user_id', $followingUser)
                                ->where('activities.created_at', '>=', $date);
                        });
                    }
                    $query->orWhere(function ($query) use ($userId) {
                        return $query->where('activities.user_id', $userId)
                            ->where(function ($query) use ($userId) {
                                return $query->orwhere('activities.action', 'post')
                                    ->orWhere('activities.action', 'share')
                                    ->orWhere('activities.action', 'compilation')
                                    ->orWhere('activities.action', 'follow') // allow follow campaign for testing only
                                    ->orWhere('activities.action', 'milestone')
                                    ->orWhere('activities.action', 'succeed')
                                    ->orWhere('activities.action', 'campaign');
                            });
                            //->where('activities.type', '<>', 'follow_campaign');
                    });
                });
        }
        $activities = $activities->where('activities.type', '<>', 'follow_user')
            ->where('activities.action', '<>', 'join')
            ->where('activities.action', '<>', 'comment')
            ->where('activities.action', '<>', 'like')
            ->orderBy('activities.created_at', 'desc')
            ->skip(($page-1)*$perPage)
            ->take($perPage)
            ->select([
                'activities.id',
                'activities.type',
                'activities.action',
                'activities.action_id',
                'activities.created_at',
                'activities.updated_at',
                'users.first_name',
                'users.last_name',
                'users.name',
                'users.position',
                'users.organization',
                'activities.user_id',
                'users.image AS avatar'
            ])
            ->with('activitable')
            ->get();
        $result = [];
        foreach ($activities as &$activity) {
            $activity->checkIsOwner($userId);
            $activity->retrieveInfo($userId);
            $activity->setRelatedInfoAttribute($userId);
            if (isset($activity->info->additionals) && empty($activity->info->additionals) && $activity->action == 'compilation') {
                //
            } else {
                $result[] = $activity;
            }
        }

        return response()->success($result);
    }
    // get following user ids.
    public function getFollowingUserIds($userId) {
        $following = Follow::where('object', 'user')
            ->where('user_id', $userId)
            ->pluck('created_at', 'object_id');
        return $following;
    }
    // check if wall of user.
    public function isOwnWall($wallId, $user) {
        if ($user->wall_id == $wallId) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    // check if wall group joined.
    public function isOwnGroupWall($wallId, $user) {
        $walls = Wall::join('groups', 'walls.id', '=', 'groups.wall_id')
            ->join('group_members', 'group_members.group_id', '=', 'groups.id')
            ->where('walls.id', $wallId)
            ->where('group_members.user_id', $user->id)
            ->get();
        if ($walls->isEmpty()) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function delete(Request $request, $activityId) {
        $activity = Activity::where('id', $activityId)
            ->first();

        $action = $activity->activitable()->first();

        if ($action && $action->user_id == $request->user()->id && $action->delete()) {
            return response()->success();
        } elseif ($action && $action->user_id != $request->user()->id || (get_class($action) != Post::class && get_class($action) != Share::class)) {
            return response()->notAuthorize();
        } elseif (!$action) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }

    public function getActivityById(Request $request, $activityId) {
        $userId = $request->user()->id;

        $activity = Activity::join('users', 'activities.user_id', '=', 'users.id')
            ->where('activities.id', $activityId)
            ->select([
                'activities.id',
                'activities.type',
                'activities.action',
                'activities.action_id',
                'activities.created_at',
                'activities.updated_at',
                'users.first_name',
                'users.last_name',
                'users.name',
                'users.position',
                'users.organization',
                'activities.user_id',
                'users.image AS avatar'
            ])
            ->with('activitable')
            ->first();

        if (!$activity) {
            return response()->notFound();
        }

        $activity->checkIsOwner($userId);
        $activity->retrieveInfo($userId);
        $activity->setRelatedInfoAttribute($userId);

        return response()->success($activity);
    }
}