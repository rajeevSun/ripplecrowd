<?php

namespace App\Http\Controllers;

use App\Achievement;
use DB;
use Illuminate\Http\Request;

class AchievementController extends Controller
{
    public function getUserAchievements(Request $request) {
        $achievements = $request->user()->achievements()->get();
        return response()->success($achievements);
    }

    public function getAllAchievements(Request $request) {
        $userId = $request->user()->id;
        $achievements = Achievement::leftJoin('user_achievements', 'user_achievements.achievement_id', '=', 'achievements.id')
            ->leftJoin('users', function($join) use ($userId) {
                return $join->on('users.id', '=', 'user_achievements.user_id')->where('users.id', $userId);
            })
            ->orderBy('user_achievements.user_id', 'desc')
            ->select(['achievements.*', DB::raw('IF(ISNULL(user_achievements.user_id),0,1) AS achieve')])
            ->get();

        return response()->success($achievements);
    }

    public function getAchievementsSummary(Request $request) {
        $achievements = Achievement::leftJoin('user_achievements', function($join) use ($request){
                return $join->on('user_achievements.achievement_id', '=', 'achievements.id');
            })
            ->where('user_achievements.user_id', $request->user()->id)
            ->groupBy('achievements.type')
            ->select(['achievements.type', DB::raw('COUNT(*) AS `count`')])
            ->get();
        return response()->success($achievements);
    }
}
