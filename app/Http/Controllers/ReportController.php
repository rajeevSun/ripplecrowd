<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Report;

class ReportController extends Controller
{
    public function store(Request $request, $postId) {
        $post = Post::find($postId);
        if ($post) {
            $report = $post->reportUsers()->attach($request->user()->id);
            return response()->success($report);
        } else {
            return response()->notFound();
        }
    }
}
