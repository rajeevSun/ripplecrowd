<?php

namespace App\Http\Controllers;

use App\Helpers\Image;
use Illuminate\Http\Request;
use App\Meetup;
use App\MeetupAttendee;
use Validator;

class MeetupController extends Controller
{
    public function store(Request $request) {
        $userId = $request->user()->id;
        // set rules for validator
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'location' => 'required',
            'attending_date' => 'required|date',
            'attending_time' => 'required|date_format:H:i',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'image' => 'sometimes|image|mines:jpg,jpeg,bmp,gif,png|max:5000'
        ];
        // match validator between request and rules
        $validator = Validator::make($request->all(), $rules);
        // validate
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $meetup = new Meetup();
        $meetup->user_id = $userId;
        $meetup->title = $request->get('title');
        $meetup->description = $request->get('description');
        $meetup->location = $request->get('location');
        $meetup->attending_date = $request->get('attending_date');
        $meetup->attending_time = $request->get('attending_time');
        $meetup->lat = $request->get('lat');
        $meetup->lng = $request->get('lng');
        $saved = $meetup->save();
        if ($saved) {
            Image::uploadMeetupImage('image', 'meetup', $meetup->id, 'meetup');
            return response()->success($meetup);
        } else {
            return response()->serverError();
        }
    }

    public function getMeetups(Request $request) {
        $page = $request->query('page', config('setting.meetup.default_page'));
        $perPage = $request->query('per_page', config('setting.meetup.default_per_page'));
        $sortBy = $request->query('sort_by', config('setting.meetup.default_sort_by'));
        $order = $request->query('order', config('setting.meetup.default_order'));
        $meetups = Meetup::skip(($page-1)*$perPage)
            ->take($perPage)
            ->orderBy($sortBy, $order)
            ->get();
        return response()->success($meetups);
    }

    public function edit(Request $request, $meetupId) {
        // set rules for validator
        $rules = [
            'title' => 'sometimes',
            'description' => 'sometimes',
            'location' => 'sometimes',
            'attending_date' => 'sometimes|date',
            'attending_time' => 'sometimes|date_format:H:i',
            'lat' => 'sometimes|numeric',
            'lng' => 'sometimes|numeric',
            'image' => 'sometimes|image|mines:jpg,jpeg,bmp,gif,png|max:5000'
        ];
        // match validator between request and rules
        $validator = Validator::make($request->all(), $rules);

        $meetup = Meetup::find($meetupId);

        // validate
        if ($validator->fails() || !$meetup) {
            return response()->paramsError($validator->messages());
        }

        // update
        foreach ($rules as $field => $value) {
            if ($request->has($field) && $field!='image') {
                $meetup->{$field} = $request->get($field);
            } elseif ($request->has($field) && $field=='image') {
                $meetup->{$field} = Image::uploadMeetupImage('image', 'meetup', $meetup->id, 'meetup');
            }
        }

        if ($meetup->save()) {
            return response()->success($meetup);
        } else {
            return response()->serverError();
        }
    }

    public function getAttendeesOfMeetup(Request $request, $meetupId) {
        $rules = [
            'id' => 'required|integer|exists:meetups,id'
        ];

        // match validator between request and rule
        $validator = Validator::make(['id' => $meetupId], $rules);
        // validate
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $meetup = Meetup::find($meetupId);
        $attendees = $meetup->attendees()
            ->select(['users.first_name', 'users.last_name', 'users.image AS avatar', 'meetup_attendees.user_id', 'meetup_attendees.role'])
            ->get();
        return response()->success($attendees);
    }

    public function getMeetupById(Request $request, $meetupId) {
        $rules = [
            'id' => 'required|integer|exists:meetups,id'
        ];
        // match validator between request and rules
        $validator = Validator::make(['id' => $meetupId], $rules);
        // validate
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $meetup = Meetup::find($meetupId);
        return response()->success($meetup);
    }

    // join meetup
    public function joinMeetup(Request $request, $meetupId) {
        $attendee = MeetupAttendee::where('user_id', $request->user()->id)
            ->where('meetup_id', $meetupId)
            ->first();
        if (!$attendee) {
            $createAttendee = MeetupAttendee::create([
                'meetup_id' => $meetupId,
                'user_id' => $request->user()->id,
                'role' => 'attendee'
            ]);
            if (!$createAttendee) {
                return response()->serverError();
            }
        }
        return response()->success();
    }
    // leave group
    public function leaveMeetup(Request $request, $meetupId) {
        $attendee = MeetupAttendee::where('user_id', $request->user()->id)
            ->where('meetup_id', $meetupId)
            ->delete();
        if ($attendee) {
            return response()->success();
        } else {
            return response()->serverError();
        }
    }

    // delete meetup
    public function delete(Request $request, $meetupId) {
        $meetup = Meetup::find($meetupId);

        if ($meetup && $meetup->user_id == $request->user()->id && $meetup->delete()) {
            return response()->success();
        } elseif ($meetup && $meetup->user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$meetup) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }
}