<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Follow;
use App\User;
use Validator;

class FollowController extends Controller {
    public function store(Request $request) {
        $rules = [
            'id' => 'required|integer|min:1',
            'object' => 'required|in:campaign,user'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }
        $follow = Follow::where('user_id', $request->user()->id)
            ->where('object', $request->get('object'))
            ->where('object_id', $request->get('id'))
            ->select(['id', 'user_id', 'object', 'object_id', 'created_at', 'updated_at'])
            ->first();
        if ($follow) {
            return response()->success($follow);
        }

        $objectClass = 'App\\'. ucfirst($request->get('object'));
        $object = $objectClass::find($request->get('id'));
        if (!$object) {
            return response()->notFound();
        }

        $follow = new Follow();
        $follow->user_id = $request->user()->id;
        $follow->object = $request->get('object');
        $follow->object_id = $request->get('id');
        if ($follow->save()) {
            return response()->success($follow);
        } else {
            return response()->serverError();
        }
    }

    public function getFollowerUsers(Request $request) {

        $currentUserId = $request->user()->id;

        $page = $request->input('page', config('setting.user.default_page'));
        $perPage = $request->input('per_page', config('setting.user.default_per_page'));
        $sortBy = $request->input('sort_by', config('setting.user.default_sort_by'));
        $order = $request->input('order', config('setting.user.default_order'));

        $users = $request->user()
            ->followers()
            ->select([
                'users.id',
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.followers_count',
                'users.position',
                'users.organization',
                'users.image AS avatar',
                'users.created_at',
                'users.updated_at'
            ])
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy($sortBy, $order)
            ->distinct()
            ->get();

        foreach ($users as $user) {
            $user->checkFollowed($currentUserId);
        }

        if ($users) {
            return response()->success($users);
        } else {
            return response()->serverError();
        }
    }

    public function getFollowingUsers(Request $request) {

        $currentUserId = $request->user()->id;

        $page = $request->input('page', config('setting.user.default_page'));
        $perPage = $request->input('per_page', config('setting.user.default_per_page'));
        $sortBy = $request->input('sort_by', config('setting.user.default_sort_by'));
        $order = $request->input('order', config('setting.user.default_order'));

        $users = $request->user()
            ->usersFollowing()
            ->select([
                'users.id',
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.followers_count',
                'users.position',
                'users.organization',
                'users.image AS avatar',
                'users.created_at',
                'users.updated_at'
            ])
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy($sortBy, $order)
            ->distinct()
            ->get();

        foreach ($users as $user) {
            $user->checkFollowed($currentUserId);
        }

        if ($users) {
            return response()->success($users);
        } else {
            return response()->serverError();
        }
    }

    public function delete(Request $request) {
        $rules = [
            'id' => 'required|integer|min:1',
            'object' => 'required|in:campaign,user'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        $follow = Follow::where('user_id', $request->user()->id)
            ->where('object_id', $request->get('id'))
            ->where('object', $request->get('object'))
            ->first();
        if ($follow && $follow->user_id == $request->user()->id && $follow->delete()) {
            return response()->success();
        } elseif ($follow && $follow->user_id != $request->user()->id) {
            return response()->notAuthorize();
        } elseif (!$follow) {
            return response()->notFound();
        } else {
            return response()->serverError();
        }
    }
}
