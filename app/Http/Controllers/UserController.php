<?php

namespace App\Http\Controllers;

use App\Activity;
use App\OAuthAccessToken;
use App\Wall;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use App\Http\Requests;
use Laravel\Passport\TokenRepository;
use Lcobucci\JWT\Parser;
use League\OAuth2\Server\AuthorizationServer;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Bridge\Client;
use App\User;
use App\Interest;
use Validator;
use Hash;
use App\OAuthClient;
use DB;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\CryptKey;
use Laravel\Passport\Passport;
use App\Mail\ForgotPassword;
use Mail;
use Image;
use App\Follow;
use App\Elastic;
use Auth;
use Illuminate\Events\Dispatcher;

class UserController extends AccessTokenController
{
    use ValidatesRequests;

    const CLIENT_ID = '4';
    const CLIENT_SECRET = 'MYjbf6aC5xsp4hf3i07mVWChKunUeHin7LTRYIdt';
    const GRANT_TYPE = 'password';

    public function getToken(ServerRequestInterface $request) {

        $input = $request->getParsedBody();
        $request = $request->withParsedBody(array_merge($input,
            ['client_id' => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'grant_type' => self::GRANT_TYPE,
            'username' => isset($input['email']) ? $input['email'] : NULL]));
        //echo "<pre>";print_r($request);exit;
        $response = $this->issueToken($request);
        $statusCode = $response->getStatusCode();
        $responseMessage = $response->getBody()->__toString();
        //check if success get token
        if ($statusCode == 200) {
            $responseMessage = json_decode($responseMessage, TRUE);
            $user = $this->getUserByEmail($input['email']);
            $responseMessage['user'] = $user;
            $responseMessage['firebase_token'] = $user->firebase_token;
            $responseMessage = json_encode($responseMessage);
        }
        return response()->auth($statusCode, $responseMessage);
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        // validate data here
        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->name = $user->first_name . ' ' . $user->last_name;
        $user->password = Hash::make($request->get('password'));
        $user->email = $request->get('email');
        if ($user->save()) {
            return response()->created($user);
        } else {
            return response()->serverError($user);
        }
    }

    public function getProviderToken(Request $request, $provider = 'facebook') {

        // validate provider is valid
        if ($provider!= 'facebook' && $provider!='linkedin') {
            return response()->paramsError(['provider' => ['Provider must be facebook or linkedin']]);
        }

        // validate input request before create new user.
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email',
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        // check if user is exist
        $user = User::where('provider', '=', $provider)
            ->where('provider_user_id', '=', $request->get('id'))
            ->first();

        if ($user) {
            $access_token = (string) $this->getNewTokenForUser($user->id)->convertToJWT(new CryptKey('file://'.Passport::keyPath('oauth-private.key')));
            return response()->success([
                'access_token' => $access_token,
                'user' => $user,
                'token_type' => 'Bearer'
            ]);
        } else {

            // validate data here
            $user = new User();
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->name = $user->first_name . ' ' . $user->last_name;
            $user->provider_user_id = $request->get('id');
            $user->provider = $provider;
            $user->email = $request->get('email');
            if ($user->save()) {
                $user = User::find($user->id);
                $access_token = (string) $this->getNewTokenForUser($user->id)->convertToJWT(new CryptKey('file://'.Passport::keyPath('oauth-private.key')));
                return response()->created([
                    'access_token' => $access_token,
                    'user' => $user,
                    'token_type' => 'Bearer',
                    'firebase_token' => $user ? $user->firebase_token : ''
                ]);
            } else {
                return response()->serverError($user);
            }
        }
    }

    public function getNewTokenForUser($userId) {
        // init access token repository
        $accessTokenRepository = new AccessTokenRepository(DB::connection('mysql'), new Dispatcher());
        $oauthClient = OAuthClient::find(self::CLIENT_ID);
        $client = new Client($oauthClient->id, $oauthClient->name, $oauthClient->redirect);
        $accessToken = $accessTokenRepository->getNewToken($client, [], $userId);
        $accessToken->setClient($client);
        $accessToken->setExpiryDateTime(Carbon::now()->addDays(15));

        $maxGenerationAttempts = PasswordGrant::MAX_RANDOM_TOKEN_GENERATION_ATTEMPTS;
        while ($maxGenerationAttempts-- > 0) {
            $accessToken->setIdentifier(self::generateUniqueIdentifier());
            try {
                $accessTokenRepository->persistNewAccessToken($accessToken);

                return $accessToken;
            } catch (UniqueTokenIdentifierConstraintViolationException $e) {
                if (PasswordGrant::MAX_RANDOM_TOKEN_GENERATION_ATTEMPTS === 0) {
                    throw $e;
                }
            }
        }
    }

    public static function generateUniqueIdentifier($length = 40)
    {
        try {
            return bin2hex(random_bytes($length));
            // @codeCoverageIgnoreStart
        } catch (\TypeError $e) {
            throw OAuthServerException::serverError('An unexpected error has occurred');
        } catch (\Error $e) {
            throw OAuthServerException::serverError('An unexpected error has occurred');
        } catch (\Exception $e) {
            // If you get this message, the CSPRNG failed hard.
            throw OAuthServerException::serverError('Could not generate a random string');
        }
        // @codeCoverageIgnoreEnd
    }

    protected function getUserByToken($token) {
        $accessToken = OAuthAccessToken::where('id', $token)->first();
        if ($accessToken) {
            return $accessToken->user()->first;
        } else {
            return FALSE;
        }
    }

    protected function getUserByEmail($email) {
        return User::where('email', $email)->first();
    }

    public function getStatistics(Request $request) {
        $perPage = $request->query('per_page', config('setting.statistic.default_per_page'));
        $page = $request->query('page', config('setting.statistic.default_page'));
        $raisedBackings = $request->user()
            ->raisedBackings()
            ->join('campaigns', 'campaigns.id', '=', 'backings.campaign_id')
            ->skip(($page-1)*$perPage)
            ->take($perPage)
            ->select(['campaigns.id', 'backings.created_at', 'campaigns.name', 'campaigns.funding_kind', 'backings.fund_amount', 'campaigns.status'])
            ->get();
        $investedBackings = $request->user()
            ->investedBackings()
            ->join('campaigns', 'campaigns.id', '=', 'backings.campaign_id')
            ->skip(($page-1)*$perPage)
            ->take($perPage)
            ->select(['campaigns.id', 'backings.created_at', 'campaigns.name', 'campaigns.funding_kind', 'backings.fund_amount', 'campaigns.status'])
            ->get();
        $result = [
            'raised_count' => $this->getRaisedBackingsCount($request),
            'raised_amount' => $this->getRaisedBackingsAmount($request),
            'raised_items' => $raisedBackings,
            'invested_count' => $this->getInvestedBackingsCount($request),
            'invested_amount' => $this->getInvestedBackingsAmount($request),
            'invested_items' => $investedBackings
        ];
        return response()->success($result);
    }

    public function getRaisedStatistics(Request $request) {
        $perPage = $request->query('per_page', config('setting.statistic.default_per_page'));
        $page = $request->query('page', config('setting.statistic.default_page'));
        $backings = $request->user()
            ->raisedBackings()
            ->join('campaigns', 'campaigns.id', '=', 'backings.campaign_id')
            ->skip(($page-1)*$perPage)
            ->take($perPage)
            ->select(['campaigns.id', 'backings.created_at', 'campaigns.name', 'campaigns.funding_kind', 'backings.fund_amount', 'campaigns.status'])
            ->get();
        return response()->success($backings);
    }

    public function getInvestedStatistics(Request $request) {
        $perPage = $request->query('per_page', config('setting.statistic.default_per_page'));
        $page = $request->query('page', config('setting.statistic.default_page'));
        $backings = $request->user()
            ->investedBackings()
            ->join('campaigns', 'campaigns.id', '=', 'backings.campaign_id')
            ->skip(($page-1)*$perPage)
            ->take($perPage)
            ->select(['campaigns.id', 'backings.created_at', 'campaigns.name', 'campaigns.funding_kind', 'backings.fund_amount', 'campaigns.status'])
            ->get();
        return response()->success($backings);
    }

    public function getRaisedBackingsCount(Request $request) {
        return $request->user()
            ->raisedBackings()
            ->join('campaigns', 'campaigns.id', '=', 'backings.campaign_id')
            ->count();
    }

    public function getRaisedBackingsAmount(Request $request) {
        return $request->user()
            ->raisedBackings()
            ->join('campaigns', 'campaigns.id', '=', 'backings.campaign_id')
            ->sum('backings.fund_amount');
    }

    public function getInvestedBackingsCount(Request $request) {
        return $request->user()
            ->investedBackings()
            ->join('campaigns', 'campaigns.id', '=', 'backings.campaign_id')
            ->count();
    }

    public function getInvestedBackingsAmount(Request $request) {
        return $request->user()
            ->investedBackings()
            ->join('campaigns', 'campaigns.id', '=', 'backings.campaign_id')
            ->sum('backings.fund_amount');
    }

    public function edit(Request $request) {
        $user = $request->user();
        $rules = [
            'first_name' => 'sometimes|alpha',
            'last_name' => 'sometimes|alpha',
            'password' => 'sometimes',
            'biography' => 'sometimes',
            'position' => 'sometimes',
            'organization' => 'sometimes',
            'category_id' => 'sometimes',
            'image' => 'sometimes|image|mimes:jpg,jpeg,bmp,gif,png|max:8192',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        foreach ($rules as $field => $rule) {
            if ($request->has($field) && $field == 'password') {
                $user->{$field} = Hash::make($request->get($field));
            } elseif ($request->has($field) && $field == 'category_id') {
                $userId = $request->user()->id;
                foreach ($request->get('category_id') as $categoryId) {
                    Interest::firstOrCreate([
                        'user_id' => $userId,
                        'category_id' => $categoryId
                    ]);
                }
            } elseif($request->has($field) && $field != 'image') {
                $user->{$field} = $request->get($field);
            }
        }

        $saved = $user->save();
        if ($saved) {
            // update image if exists.
            Image::uploadUserImage('image', 'user', $user->id, 'user');
            return response()->success($user);
        } else {
            return response()->serverError();
        }
    }

    public function getInvitedUsersList(Request $request) {
        $user = $request->user();
        $invitings = $user->join('invitations', 'invitations.invited_user_id', '=', 'users.id')
            ->join('users as inviting_users', 'inviting_users.id', '=', 'invitations.user_id')
            ->join('campaigns', 'campaigns.id', '=', 'invitations.campaign_id')
            ->where('invitations.invited_user_id', $user->id)
            ->select(['inviting_users.image as avatar',
                'inviting_users.first_name',
                'inviting_users.last_name',
                'inviting_users.position',
                'inviting_users.organization',
                'invitations.id',
                'invitations.user_id',
                'invitations.campaign_id',
                'campaigns.name'])
            ->get();
        return response()->success($invitings);
    }

    public function getTrending(Request $request) {
        $currentUserId = $request->user()->id;
        $page = $request->query('page', config('setting.user.default_page'));
        $perPage = $request->query('per_page', config('setting.user.default_per_page'));
        $sortBy = $request->query('sort_by', config('setting.user.default_sort_by'));
        $order = $request->query('order', config('setting.user.default_order'));
        //$followings = $request->user()->usersFollowing()->pluck('follows.object_id');
        $users = User::orderBy($sortBy, $order)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->select([
                'users.id',
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.followers_count',
                'users.position',
                'users.organization',
                'users.created_at',
                'users.updated_at',
                'users.image AS avatar'
            ])
            ->get();
        foreach ($users as $user) {
            $user->checkFollowed($currentUserId);
        }
        return response()->success($users);
    }

    public function getActivities(Request $request, $userId = NULL) {
        $page = $request->query('page', config('setting.activity.default_page'));
        $perPage = $request->query('per_page', config('setting.activity.default_per_page'));
        $currentUserId = $request->user()->id;

        // find wall of user.
        $wall = Wall::where('type', 'user')
            ->where('object_id', $userId ? $userId : $currentUserId)
            ->first();
        if (!$wall) {
            return response()->success();
        }

        $followingUserIds = $this->getFollowingUserIds($userId);
        $activities = Activity::join('users', 'users.id', '=', 'activities.user_id')
            ->where('activities.user_id', $userId ? $userId : $currentUserId)
            ->where('activities.action', '<>', 'compilation')
            ->orderBy('activities.created_at', 'desc')
            ->skip(($page-1)*$perPage)
            ->take($perPage)
            ->select([
                'activities.id',
                'activities.type',
                'activities.action',
                'activities.action_id',
                'activities.created_at',
                'activities.updated_at',
                'users.first_name',
                'users.last_name',
                'users.name',
                'users.position',
                'users.organization',
                'activities.user_id',
                'users.image AS avatar'
            ])
            ->get();
        foreach ($activities as $activity) {
            $activity->retrieveInfo($currentUserId);
        }
        return response()->success($activities);
    }

    // get following user ids.
    public function getFollowingUserIds($userId) {
        $following = Follow::where('object', 'user')
            ->where('user_id', $userId)
            ->pluck('object_id', 'created_at');
        return $following;
    }

    public function logout(Request $request) {
        $value = $request->bearerToken();
        $id= (new Parser())->parse($value)->getHeader('jti');

        $token=  DB::table('oauth_access_tokens')
            ->where('id', '=', $id)
            ->update(['revoked' => true]);
        return response()->success();
    }

    public function recommendUsers(Request $request) {
        $page = $request->input('page', config('setting.user.default_page'));
        $perPage = $request->input('per_page', config('setting.user.default_per_page'));
        $sortBy = $request->input('sort_by', config('setting.user.default_sort_by'));
        $order = $request->input('order', config('setting.user.default_order'));
        $followings = $request->user()->usersFollowing()->pluck('follows.object_id');
        $categories = $request->user()->categories()->pluck('categories.id');
        $userId = $request->user()->id;
        $users = User::whereNotIn('users.id', $followings)
            ->join('interests', 'interests.user_id', '=', 'users.id')
            ->whereIn('interests.category_id', $categories)
            ->where('users.id', '<>', $userId)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy($sortBy, $order)
            ->select([
                'users.id',
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.followers_count',
                'users.position',
                'users.organization',
                'users.image AS avatar',
                'users.created_at',
                'users.updated_at'
            ])
            ->distinct()
            ->get();
        foreach ($users as $user) {
            $user->checkFollowed($userId);
        }
        return response()->success($users);
    }

    public function getAllUsers(Request $request) {
        $page = $request->input('page', config('setting.user.default_page'));
        $perPage = $request->input('per_page', config('setting.user.default_per_page'));
        $sortBy = $request->input('sort_by', config('setting.user.default_sort_by'));
        $order = $request->input('order', config('setting.user.default_order'));
        $q = $request->input('q');
        $userId = $request->user()->id;
        $users = User::where('users.id', '<>', $userId)
            ->take($perPage)
            ->skip(($page-1)*$perPage)
            ->orderBy($sortBy, $order)
            ->select([
                'users.id',
                \DB::raw('CONCAT(users.first_name, " ", users.last_name) AS name'),
                'users.first_name',
                'users.last_name',
                'users.position',
                'users.organization',
                'users.image',
                'users.created_at',
                'users.updated_at'
            ])
            ->distinct();
        if ($q) {
            $users = $users->where(function($query) use ($q) {
                return $query->where('first_name', 'LIKE', '%'.$q.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$q.'%');
            });
        }
            $users = $users->get();
        return response()->success($users);
    }

    public function registerDeviceToken(Request $request) {
        $user = $request->user();
        $validator = Validator::make($request->all(), [
            'device_token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->paramsError($validator->messages());
        }

        // validate data here
        $user->device_token = $request->get('device_token');
        if ($user->save()) {
            return response()->success();
        } else {
            return response()->serverError();
        }
    }
}