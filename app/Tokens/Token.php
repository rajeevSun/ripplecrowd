<?php
/**
 * Created by PhpStorm.
 * User: phonghdt
 * Date: 21-Feb-17
 * Time: 13:25
 */

namespace App\Tokens;

use Firebase\JWT\JWT;

class Token
{
    public static function createCustomToken($uid, $isPremiumAccount = FALSE) {
        $serviceAccountEmail = config('chat.service_account_email');
        $privateKey = config('chat.private_key');

        $nowSeconds = time();
        $payload = [
            "iss" => $serviceAccountEmail,
            "sub" => $serviceAccountEmail,
            "aud" => "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit",
            "iat" => $nowSeconds,
            "exp" => $nowSeconds+(60*60),  // Maximum expiration time is one hour
            "uid" => $uid,
            "claims" => [
                "premium_account" => $isPremiumAccount,
            ]
        ];
        return JWT::encode($payload, $privateKey, "RS256");
    }
}