<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Parsers\ParserManager as Parser;

class ParserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Parser::class, function($app) {
            return new Parser($app);
        });
    }
}
