<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Tokens\Token;

class TokenServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Token::class, function($app) {
            return new Token();
        });
    }
}
