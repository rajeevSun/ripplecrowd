<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Channels\FcmChannel;

class FcmServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FcmChannel::class, function($app) {
            return new FcmChannel(config('fcm'));
        });
    }
}
