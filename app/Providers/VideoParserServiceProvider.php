<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Parsers\Video;

class VideoParserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Video::class, function ($app) {
            return new Video();
        });
    }
}
