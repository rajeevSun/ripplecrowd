<?php

namespace App\Providers;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;

class ResponseMacroProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // wrong params
        Response::macro('paramsError', function($value = NULL) {
            return Response::make([
                'meta' => [
                    'success'=> false,
                    'code' => '001',
                    'message' => (get_class($value) == MessageBag::class) ? $value->first() : ''
                ],
                //'data' => $value ? $value : new \stdClass()
                'data' => NULL
            ], 200, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });

        // wrong params
        Response::macro('conflict', function($value = NULL) {
            return Response::make([
                'meta' => [
                    'success'=> false,
                    'code' => '010',
                    'message' => (get_class($value) == MessageBag::class) ? $value->first() : ''
                ],
                //'data' => $value ? $value : new \stdClass()
                'data' => NULL
            ], 409, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });

        // not found
        Response::macro('notFound', function($value = NULL) {
            return Response::make([
                'meta' => [
                    'success'=> false,
                    'code' => '007',
                    'message' => Lang::get('messages.007')
                ],
                'data' => $value ? $value : new \stdClass()
            ], 404, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });

        // success created
        Response::macro('created', function($value) {
            return Response::make([
                'meta' => [
                    'success' => true,
                    'code' => '002',
                    'message' => Lang::get('messages.002')
                ],
                'data' => $value
            ], 201, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });
        // server error
        Response::macro('serverError', function($value = NULL) {
            return Response::make([
                'meta' => [
                    'success' => false,
                    'code' => '003',
                    'message' => Lang::get('messages.003')
                ],
                'data' => $value!==NULL ? $value : new \stdClass()
            ], 500, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });
        // success response
        Response::macro('success', function($value = NULL) {
            return Response::make([
                'meta' => [
                    'success' => true,
                    'code' => '004',
                    'message' => Lang::get('messages.004')
                ],
                'data' => $value!==NULL ? $value : new \stdClass()
            ], 200, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });

        // not authorize
        Response::macro('notAuthorize', function($value = NULL) {
            return Response::make([
                'meta' => [
                    'success' => true,
                    'code' => '009',
                    'message' => Lang::get('messages.009')
                ],
                'data' => $value ? $value : new \stdClass()
            ], 403, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });

        // empty response
        Response::macro('empty', function($value = NULL) {
            return Response::make([
                'meta' => [
                    'success' => true,
                    'code' => '006',
                    'message' => Lang::get('messages.006')
                ],
                'data' => $value!==NULL ? $value : new \stdClass()
            ], 200, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });

        // unauthenticate
        Response::macro('unauthenticate', function($value) {
            return Response::make([
                'meta' => [
                    'success' => false,
                    'code' => '005',
                    'message' => Lang::get('messages.005')
                ],
                'data' => $value
            ], 401, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });

        // not acceptable
        Response::macro('notAcceptable', function($value) {
            return Response::make([
                'meta' => [
                    'success' => false,
                    'code' => '008',
                    'message' => Lang::get('messages.008')
                ],
                'data' => $value
            ], 406, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });

        // auth response
        Response::macro('auth', function($statusCode, $value) {
            // unauthenticate
            if ($statusCode == 401) {
                return Response::make([
                    'meta' => [
                        'success' => false,
                        'code' => '005',
                        'message' => Lang::get('messages.005')
                    ],
                    'data' => json_decode($value)
                ], 401, [
                    'Content-Type' => 'application/json;charset=utf8'
                ]);
            }
            // wrong params
            if ($statusCode == 400) {
                return Response::make([
                    'meta' => [
                        'success' => false,
                        'code' => '001',
                        'message' => (get_class($value) == MessageBag::class) ? $value->first() : ''
                    ],
                    'data' => json_decode($value)
                ], 200, [
                    'Content-Type' => 'application/json;charset=utf8'
                ]);
            }
            // success response
            if ($statusCode == 200) {
                return Response::make([
                    'meta' => [
                        'success' => true,
                        'code' => '004',
                        'message' => Lang::get('messages.004')
                    ],
                    'data' => json_decode($value)
                ], 200, [
                    'Content-Type' => 'application/json;charset=utf8'
                ]);
            }
            // server error
            return Response::make([
                'meta' => [
                    'success' => true,
                    'code' => '003',
                    'message' => json_decode($value)
                ],
                'data' => json_decode($value)
            ], 500, [
                'Content-Type' => 'application/json;charset=utf8'
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
