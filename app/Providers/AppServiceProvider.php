<?php

namespace App\Providers;

use App\Achievement;
use App\Activity;
use App\Compilation;
use App\Campaign;
use App\Comment;
use App\File;
use App\Like;
use App\Meetup;
use App\Milestone;
use App\Observers\CommentObserver;
use App\Observers\CompilationObserver;
use App\Observers\FileObserver;
use App\Observers\GroupMemberObserver;
use App\Observers\LikeObserver;
use App\Observers\MeetupObserver;
use App\Share;
use App\Suggestion;
use App\User;
use App\Backing;
use Illuminate\Support\ServiceProvider;
use Validator;
use App\Validators\VideoLinkValidator;
use App\Observers\PostObserver;
use App\Observers\FollowObserver;
use App\Observers\GroupObserver;
use App\Observers\UserObserver;
use App\Observers\ShareObserver;
use App\Observers\CampaignObserver;
use App\Observers\ActivityObserver;
use App\Observers\BackingObserver;
use App\Observers\MilestoneObserver;
use App\Post;
use App\Follow;
use App\Group;
use App\GroupMember;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('videolink', function($attribute, $value, $parameters) {
            return VideoLinkValidator::checkVideoLink($attribute, $value, $parameters);
        });

        // observe post model
        Post::observe(PostObserver::class);
        // observe follow model
        Follow::observe(FollowObserver::class);
        // observe group model
        Group::observe(GroupObserver::class);
        // observe user model
        User::observe(UserObserver::class);
        // observe share model
        Share::observe(ShareObserver::class);
        // observe like model
        Like::observe(LikeObserver::class);
        // observe comment model
        Comment::observe(CommentObserver::class);
        // observe campaign model
        Campaign::observe(CampaignObserver::class);
        // observe activity model
        Activity::observe(ActivityObserver::class);
        // observe backing model
        Backing::observe(BackingObserver::class);
        // observe compilation model
        Compilation::observe(CompilationObserver::class);
        // observe milestone model
        Milestone::observe(MilestoneObserver::class);
        // observe meetup model
        Meetup::observe(MeetupObserver::class);
        // observe group member model
        GroupMember::observe(GroupMemberObserver::class);

        // morph map
        Relation::morphMap([
            'post' => Post::class,
            'share' => Share::class,
            'like' => Like::class,
            'follow' => Follow::class,
            'comment' => Comment::class,
            'meetup' => Meetup::class,
            'campaign' => Campaign::class,
            'activity' => Activity::class,
            'user' => User::class,
            'join' => GroupMember::class,
            'milestone' => Milestone::class,
            'succeed' => Campaign::class,
            'compilation' => Compilation::class,
            'suggest' => Suggestion::class,
            'campaign' => Campaign::class,
            'group' => Group::class,
            'achievement' => Achievement::class
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
