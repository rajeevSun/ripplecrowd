<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\MediaServices;

class MediaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MediaServices::class, function($app) {
            return new MediaServices(config('media-services.account'));
        });
    }
}
