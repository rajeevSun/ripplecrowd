<?php
namespace App;
/**
 * Created by PhpStorm.
 * User: Thuan Nguyen
 * Date: 21-Dec-16
 * Time: 10:37
 */
trait Owner {
    public function checkIsOwner($userId) {
        return $this->setIsOwnerAttribute($this->user_id == $userId);
    }

    public function getIsOwnerAttribute($value) {
        return $value;
    }

    public function setIsOwnerAttribute($value) {
        $this->attributes['is_owner'] = $value;
    }

    public function getUserAttribute($value) {
        return $value;
    }

    public function setUserAttribute($value) {
        $this->attributes['user'] = $this->user()
            ->select([
                'users.*',
                'users.image AS avatar'
            ])
            ->first();
    }
}