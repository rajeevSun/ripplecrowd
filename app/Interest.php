<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $table = 'interests';

    protected $fillable = ['user_id','category_id'];

    // date mutators
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = TRUE;
}
